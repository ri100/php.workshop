<?php
namespace AtomPieTestAssets\Resource\Mock {

    use AtomPie\Gui\Component;

    /**
     * @property int Property1
     * @property int Property2
     *
     * @package WorkshopTest\Resource\Component
     */
    class MockComponent13 extends Component
    {
    }

}

