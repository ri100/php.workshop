<?php
use AtomPie\Web\Connection\Http\Header\ContentType;
use AtomPieTestAssets\Resource\Mock\DependentClassInterface;
use AtomPieTestAssets\Resource\Mock\MockEndPoint;

return function (\AtomPie\System\EndPoint $oEndPoint) {

    $oEndPoint->setClassName(MockEndPoint::class);
    $oEndPoint->onResponse()
        ->addHeader(ContentType::CONTENT_TYPE, 'application/xml');
    $oEndPoint->onContract(DependentClassInterface::class)
        ->fillBy(
            function () {
                return new \stdClass();
            }
        );
};