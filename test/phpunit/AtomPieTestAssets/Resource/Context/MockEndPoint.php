<?php
use AtomPieTestAssets\Resource\Mock\MockEndPoint;

return function (\AtomPie\System\EndPoint $oEndPoint) {
    $oEndPoint->setClassName(MockEndPoint::class);
};