<?php
use AtomPieTestAssets\Resource\Mock\MockComponent12;

return function (\AtomPie\System\EndPoint $oEndPoint) {
    $oEndPoint->setClassName(MockComponent12::class);
    $oEndPoint->onResponse()
        ->addHeader(\AtomPie\Web\Connection\Http\Header::CONTENT_TYPE, 'application/xml');
};