<?php
use AtomPieTestAssets\Resource\Mock\MockComponent13;

return function (\AtomPie\System\EndPoint $oEndPoint) {
    $oEndPoint->setClassName(MockComponent13::class);
};