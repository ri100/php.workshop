<?php
namespace AtomPieTestAssets\Middleware {

    use AtomPie\Boundary\System\IRunBeforeMiddleware;
    use AtomPie\Web\Boundary\IChangeRequest;
    use AtomPie\Web\Boundary\IChangeResponse;

    class TestBeforeMiddleWare implements IRunBeforeMiddleware
    {

        /**
         * Returns modified Request.
         *
         * @param IChangeRequest $oRequest
         * @param IChangeResponse $oResponse
         * @return IChangeRequest
         */
        public function before(IChangeRequest $oRequest, IChangeResponse $oResponse)
        {
            $oRequest->setParam('CustomParam', 'MockEndPoint.getFile');
            $oResponse->addHeader('Custom', 'MyHeader');
        }
    }

}
