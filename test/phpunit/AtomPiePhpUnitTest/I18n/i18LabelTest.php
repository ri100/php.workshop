<?php
namespace AtomPiePhpUnitTest\I18n;

use \AtomPie\I18n;

class i18LabelTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function shouldLabelAsText()
    {
        $oLabel = I18n\Label('test');
        $this->assertEquals('test', $oLabel);
    }

    /**
     * @test
     */
    public function shouldBehaveAsPrintF()
    {
        $oLabel = I18n\Label('test %s %.02f', '1', 2.345);
        $this->assertEquals("test 1 2.35", $oLabel);
    }
}
