<?php
namespace AtomPiePhpUnitTest\Gui;

use AtomPie\Core\Dispatch\DispatchManifest;
use AtomPie\Core\Dispatch\EndPointImmutable;
use AtomPie\System\Context;
use AtomPiePhpUnitTest\FrameworkTest;
use WorkshopTest\Boot;
use AtomPie\Core\FrameworkConfig;
use AtomPie\File\FileProcessorFactory;
use AtomPie\Gui\Component\ComponentProcessorFactory;
use AtomPie\System\ClassShortcuts;
use AtomPie\System\Namespaces;

class TemplateAnnotationTest extends FrameworkTest
{

    /**
     * MockComponent13 has ContentType text/html
     *
     * @test
     */
    public function shouldThrowExceptionAboutLackingTemplateForRegularContentType()
    {

        $_REQUEST[EndPointImmutable::END_POINT_QUERY] = 'MockComponent13';
        $oResponse = Boot::run($this->getConfig());
        $sContent = $oResponse->getContent()->get();
        $this->assertTrue(false !== strstr($sContent, 'Template path not defined in component'));

    }

    /**
     * MockComponent12 has ContentType application/xml so no template needed.
     *
     * @test
     */
    public function shouldNotThrowExceptionAboutLackingTemplateForSerializedContent()
    {

        $_REQUEST[EndPointImmutable::END_POINT_QUERY] = 'MockComponent12';

        $oResponse = Boot::run($this->getConfig());
        $oResponseXml = $oResponse->getContent()->getAsSimpleXml();

        /** @noinspection PhpUndefinedFieldInspection */
        $this->assertTrue($oResponseXml->Property1 == 1);
        /** @noinspection PhpUndefinedFieldInspection */
        $this->assertTrue($oResponseXml->Property2 == 2);

    }

    public function getConfig()
    {
        $oConfig = new FrameworkConfig(
            __DIR__.'/../../AtomPieTestAssets/Resource/Context',
            'Main'
        );
        $oDispatchManifest = DispatchManifest::factory($oConfig, $_REQUEST);
        $oContext = new Context(
            $oDispatchManifest->getEndPoint(),
            $oConfig
        );
        
        $sViewFolder = __DIR__ . '/../../AtomPieTestAssets/Resource/Theme';

        $oConfig->setContentProcessors([
            new FileProcessorFactory(),
            new ComponentProcessorFactory($sViewFolder, $oContext->getEnvironment(), $oDispatchManifest)
        ]);

        $oConfig->setEventClassShortcuts(
            new ClassShortcuts(
                new Namespaces([
                    "\\AtomPieTestAssets\\Resource\\Mock"
                ])
            )
        );

        return $oConfig;
    }
}
