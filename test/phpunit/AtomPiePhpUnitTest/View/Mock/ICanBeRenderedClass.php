<?php
namespace AtomPiePhpUnitTest\View\Mock {

    use AtomPie\System\IO\File;
    use AtomPie\View\Boundary\ICanBeRendered;

    class ICanBeRenderedClass implements ICanBeRendered
    {

        public function getViewPlaceHolders()
        {
            return array('PlaceHolder1' => 'value1', 'PlaceHolder2' => 'value2');
        }

        /**
         * @param $sFolder
         * @return File
         */
        public function getTemplateFile($sFolder)
        {
            return new File(__DIR__ . '/ICanBeRenderedClass.twig');
        }
    }

}
