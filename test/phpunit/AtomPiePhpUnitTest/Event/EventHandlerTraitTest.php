<?php
namespace AtomPiePhpUnitTest\Event {

    use AtomPie\EventBus\EventHandler;
    use Generi\Boundary\ICollection;

    class Raiser
    {

        use EventHandler;

        const EVENT = 'onTestEvent';

        /**
         * @var \ArrayObject
         */
        public $aCollection;

        public function raise()
        {
            $this->aCollection = new \ArrayObject();
            $this->triggerEvent(self::EVENT, $this->aCollection);
        }
    }

    /**
     * test case.
     */
    class EventHandlerTraitTest extends \PHPUnit_Framework_TestCase
    {

        /**
         * @test
         */
        public function shoudHandleEventRaise()
        {
            $oRaiser = new Raiser();

            // Closure
            /** @noinspection PhpUnusedParameterInspection */
            $oRaiser->handleEvent(Raiser::EVENT, function ($oSender, $aCollection) {
                /* @var $aCollection ICollection */
                $aCollection['closure'] = 'OK';
            });

            $oRaiser->raise();

            $this->assertTrue(isset($oRaiser->aCollection['closure']));
        }

        /**
         * @test
         * @throws \AtomPie\EventBus\Exception
         */
        public function shouldHandleRemoveEvent()
        {
            $oRaiser = new Raiser();
            $oThat = $this;
            $iEventId = $oRaiser->handleEvent(Raiser::EVENT, function () use ($oThat) {
                $oThat->assertTrue(false);
            });
            // Static listener
            $oRaiser->handleEvent(Raiser::EVENT, function () use ($oThat) {
                $oThat->assertTrue(true);
            });
            // Remove callback
            $oRaiser->removeEventHandler('onTestEvent', $iEventId);
            $oRaiser->raise();

        }

    }
}

