<?php
namespace AtomPiePhpUnitTest\Web;

use AtomPie\Web\Connection\Http\Header;
use AtomPiePhpUnitTest\FrameworkTest;

class EnvironmentTest extends FrameworkTest
{

    protected function setUp()
    {
        parent::setUp();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testEnv()
    {
        putenv('test=1');
        $oContext = $this->getContext();
        $this->assertTrue($oContext->getEnvironment()->getEnv()->get('test') == 1);
    }

    public function testAutoRequestContentTypeSetting_XML()
    {

        $_SERVER['HTTP_ACCEPT'] = "*/*;q=0.8,application/json;q=0.6,application/xml;q=0.9";
        $oContext = $this->getContext();
        $oRequest = $oContext->getEnvironment()->getRequest();
        $oAccept = $oRequest->getHeader(Header::ACCEPT);
        $this->assertTrue($oAccept->getValue() == $_SERVER['HTTP_ACCEPT']);
        $this->assertTrue($oContext->getEnvironment()->getResponse()->getContent()->getContentType()->getValue() == Header\ContentType::XML);

    }

    public function testAutoRequestContentTypeSetting_HTML()
    {

        $_SERVER['HTTP_ACCEPT'] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
        $oContext = $this->getContext();
        $oRequest = $oContext->getEnvironment()->getRequest();
        $oAccept = $oRequest->getHeader(Header::ACCEPT);
        $this->assertTrue($oAccept->getValue() == $_SERVER['HTTP_ACCEPT']);
        $this->assertTrue($oContext->getEnvironment()->getResponse()->getContent()->getContentType()->getValue() == Header\ContentType::HTML);

    }

    public function testAutoRequestContentTypeSetting_XHTML()
    {

        $_SERVER['HTTP_ACCEPT'] = "application/xhtml+xml";
        $oContext = $this->getContext();
        $oRequest = $oContext->getEnvironment()->getRequest();
        $oAccept = $oRequest->getHeader(Header::ACCEPT);
        $this->assertTrue($oAccept->getValue() == $_SERVER['HTTP_ACCEPT']);
        $this->assertTrue($oContext->getEnvironment()->getResponse()->hasHeader(Header::CONTENT_TYPE));
        $this->assertTrue($oContext->getEnvironment()->getResponse()->getContent()->getContentType()->getValue() == Header\ContentType::HTML);

    }

    public function testAutoRequestContentTypeSetting_None()
    {

        $_SERVER['HTTP_ACCEPT'] = "*/*;q=0.8";
        $oContext = $this->getContext();
        $oRequest = $oContext->getEnvironment()->getRequest();
        $oAccept = $oRequest->getHeader(Header::ACCEPT);
        $this->assertTrue($oAccept->getValue() == $_SERVER['HTTP_ACCEPT']);
        $this->assertTrue($oContext->getEnvironment()->getResponse()->getContent()->getContentType()->getValue() == Header\ContentType::HTML);

    }

}
