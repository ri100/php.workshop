<?php
namespace AtomPiePhpUnitTest {

    use AtomPie\Core\Dispatch\DispatchManifest;
    use AtomPie\Core\FrameworkConfig;
    use AtomPie\System\Context;
    use AtomPie\System\Kernel;
    use AtomPiePhpUnitTest\Core\Mock\Config;

    class FrameworkTest extends \PHPUnit_Framework_TestCase
    {

        /**
         * @param $oConfig
         * @param null $oContext
         * @return \AtomPie\Web\Connection\Http\Response
         */
        protected function bootKernel($oConfig, $oContext =null)
        {
            $oKernel = new Kernel();
            return $oKernel->boot($oConfig, $oContext);
        }

        /**
         * @param $sDefaultEndPoint
         * @param array $aContractFillers
         * @param array $aMiddleware
         * @param array $aContentProcessors
         * @return FrameworkConfig
         */
        protected function getDefaultConfig(
            $sDefaultEndPoint, 
            array $aContractFillers = [],
            array $aMiddleware = [], 
            array $aContentProcessors = [])
        {
            $oConfig = new FrameworkConfig(
                __DIR__.'/../AtomPieTestAssets/Resource/Context',
                $sDefaultEndPoint
            );

            $oConfig->setContractFillers($aContractFillers);
            $oConfig->setMiddleware($aMiddleware);
            $oConfig->setContentProcessors($aContentProcessors);

            return $oConfig;
        }

        /**
         * @return Context
         */
        protected function getContext()
        {
            $oConfig = Config::get();
            $oDispatchManifest = DispatchManifest::factory($oConfig, $_REQUEST);
            return new Context($oDispatchManifest->getEndPoint(), $oConfig);
        }

        protected function getDispatchManifest($sDefault) {
            return DispatchManifest::factory(self::getDefaultConfig($sDefault), $_REQUEST);
        }
    }

}
