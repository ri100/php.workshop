<?php
namespace AtomPiePhpUnitTest\DependencyInjection;

use AtomPie\Boundary\Core\IAmFrameworkConfig;
use AtomPie\Core\Dispatch\DispatchManifest;
use AtomPie\Core\Dispatch\EndPointImmutable;
use AtomPie\Core\FrameworkConfig;
use AtomPie\DependencyInjection\Dependency;
use AtomPie\DependencyInjection\DependencyInjector;
use AtomPie\DependencyInjection\Exception;
use AtomPie\DependencyInjection\Exception as InjectionException;
use AtomPie\DependencyInjection\Contract;
use AtomPie\DependencyInjection\ContractFillers;
use AtomPie\System\DependencyContainer\EndPointDependencyContainer;
use AtomPie\System\UrlProvider;
use AtomPie\Web\Boundary\IAmEnvironment;
use AtomPiePhpUnitTest\FrameworkTest;

/**
 * This is EndPoint
 *
 * Class B0
 * @package AtomPiePhpUnitTest\DependencyInjection
 */
class B0
{

    /**
     * @param null $id
     * @param B1 $b1
     * @param IAmEnvironment $oEnv
     * @return B1
     */
    public function test(
        /** @noinspection PhpUnusedParameterInspection */
        $id = null,
        B1 $b1,
        IAmEnvironment $oEnv
    ) {
        return $b1;
    }

    public function test1(
        /** @noinspection PhpUnusedParameterInspection */
        $id = null,
        ChildOfB1 $b1,
        IAmEnvironment $oEnv
    ) {
        return $b1->getConfig();
    }

    public function test2(
        /** @noinspection PhpUnusedParameterInspection */
        $id = null,
        B1 $b1,
        IAmEnvironment $oEnv
    ) {
        return $b1->getConfig();
    }
}

/**
 * This is EndPoint
 *
 * Class B1
 * @package AtomPiePhpUnitTest\DependencyInjection
 */
class B1
{
    /**
     * @var IAmFrameworkConfig
     */
    private $oConfig;

    public function __construct(IAmFrameworkConfig $oConfig)
    {
        $this->oConfig = $oConfig;
    }

    /**
     * @return IAmFrameworkConfig
     */
    public function getConfig()
    {
        return $this->oConfig;
    }
}

/**
 * This is EndPoint
 *
 * Class ChildOfB1
 * @package AtomPiePhpUnitTest\DependencyInjection
 */
class ChildOfB1 extends B1
{
    /**
     * @var IAmEnvironment
     */
    private $oEnv;

    public function __construct(IAmFrameworkConfig $oConfig, IAmEnvironment $oEnv)
    {
        parent::__construct($oConfig);
        $this->oEnv = $oEnv;
    }

    /**
     * @return IAmEnvironment
     */
    public function getEnv()
    {
        return $this->oEnv;
    }
}

class ChildOfB0 extends B0
{

}

class C0
{

    static function __build()
    {
        return new C0;
    }

    static function __constrainBuild()
    {
        return [
            AllowedEndPointForBuild::class,
            EndPointWithConstructorWithCustomParameter::class
        ];
    }

}

class EndPointWithConstructor
{

    public $injected;
    public function __construct(IAmFrameworkConfig $injected)
    {
        $this->injected = $injected;
    }
}

class EndPointWithConstructorWithWrongParameter
{

    public $injected;
    public function __construct(EndPointWithConstructor $injected)
    {
        $this->injected = $injected;
    }
}

class EndPointWithConstructorWithCustomParameter
{

    public $injected;
    public function __construct(C0 $injected)
    {
        $this->injected = $injected;
    }
}

class AllowedEndPointForBuild
{
    public function inject(C0 $c)
    {

    }
}

class NotAllowedEndPointForBuild
{
    public function inject(C0 $c)
    {

    }
}

class DependencyInjectorTest extends FrameworkTest
{

    /**
     * @test
     */
    public function shouldInjectionIntoConstructor()
    {
        $oInjector = new DependencyInjector($this->getContainer());
        $oObject = $oInjector->newInstance(EndPointWithConstructor::class);
        $this->assertInstanceOf(IAmFrameworkConfig::class, $oObject->injected);
    }

    /**
     * @test
     */
    public function shouldInjectionIntoConstructorWithCustomInjection()
    {
        $oInjector = new DependencyInjector($this->getContainer());
        $oObject = $oInjector->newInstance(EndPointWithConstructorWithCustomParameter::class);
        $this->assertInstanceOf(C0::class, $oObject->injected);
    }

    /**
     * @test
     */
    public function shouldNotInjectionIntoConstructorWhenNoInjectionDefined()
    {
        $oInjector = new DependencyInjector($this->getContainer());
        $this->expectException(Exception::class);
        $oInjector->newInstance(EndPointWithConstructorWithWrongParameter::class);
    }


    /**
     * @test
     */
    public function shouldThrowExceptionWhenInjectionOutsideConstrainedClass()
    {
        $oInjector = new DependencyInjector($this->getContainer());
        $this->expectException(InjectionException::class);
        $oInjector->invokeMethod(new NotAllowedEndPointForBuild, 'inject');
    }

    /**
     * @test
     */
    public function shouldInjectionBuilderWhenInsideConstrainedClass()
    {
        $oInjector = new DependencyInjector($this->getContainer());
        $oInjector->invokeMethod(new AllowedEndPointForBuild, 'inject');
    }

    /**
     * @test
     */
    public function shouldThrowExceptionWhenInjectionDoesNotMatchParamType()
    {

        $oContainer = $this->getContainer();

        $oDependency = new Dependency();
        $oDependency->setDependency(
            [
                // Here is the ERROR
                // Here we define ChildOdB1 as injection type and return B1 instead
                // Param type is set to be B1 in test1 method and B1 is not ChildOfB1
                ChildOfB1::class => function (IAmFrameworkConfig $oConfig, IAmEnvironment $oEnv) {
                    return new ChildOfB1($oConfig, $oEnv);
                }
            ]
        );

        $oContainer->addDependency(
            B0::class, // Class to allow dependency
            'test2', // Method to allow dependency
            $oDependency
        );

        $oInjector = new DependencyInjector($oContainer);
        $this->expectException(Exception::class);
        $oInjector->invokeMethod(new B0(), 'test2');

    }

    /**
     * @test
     */
    public function shouldThrowExceptionWhenChildClassIsInInjectionDefinition()
    {

        $oContainer = $this->getContainer();

        $oDependency = new Dependency();
        $oDependency->setDependency(
            [
                // Here is the ERROR
                // Here we define ChildOdB1 as injection type
                // But we return B1 instead
                // Param type is set to be ChildOdB1 in test1 method
                ChildOfB1::class => function (IAmFrameworkConfig $oConfig) {
                    return new B1($oConfig);
                }
            ]
        );

        $oContainer->addDependency(
            ChildOfB0::class, // Class to allow dependency
            'test1', // Method to allow dependency
            $oDependency
        );


        $oInjector = new DependencyInjector($oContainer);
        if (class_exists(\TypeError::class)) {
            $this->expectException(\TypeError::class);
        } else {
            $this->expectException(\PHPUnit_Framework_Error::class);
        }
        $oInjector->invokeMethod(new ChildOfB0(), 'test1');
        $this->assertTrue(false);

    }

    /**
     * @test
     */
    public function shouldInjectDependencyInDependencyInChildClass()
    {

        $oContainer = $this->getContainer();

        $oDependency = new Dependency();
        $oDependency->setDependency(
            [
                B1::class => function (IAmFrameworkConfig $oConfig, IAmEnvironment $oEnv) {
                    return new ChildOfB1($oConfig, $oEnv);
                }
            ]
        );

        $oContainer->addDependency(
            ChildOfB0::class, // Class to allow dependency
            'test', // Method to allow dependency
            $oDependency
        );

        $oInjector = new DependencyInjector($oContainer);
        /**
         * @var $oResult ChildOfB1
         */
        $oResult = $oInjector->invokeMethod(new ChildOfB0(), 'test');
        $this->assertEquals('none', $oResult->getConfig()->getDefaultEndPoint());
        $this->assertInstanceOf(IAmEnvironment::class, $oResult->getEnv());

    }

    /**
     * @test
     */
    public function shouldInjectDependencyInDependency()
    {

        $oContainer = $this->getContainer();

        $oDependency = new Dependency();
        $oDependency->setDependency(
            [
                B1::class => function (IAmFrameworkConfig $oConfig) {
                    return new B1($oConfig);
                }
            ]
        );

        $oContainer->addDependency(
            B0::class,
            'test',
            $oDependency
        );

        $oInjector = new DependencyInjector($oContainer);
        $oResult = $oInjector->invokeMethod(new B0(), 'test');
        $this->assertEquals('none', $oResult->getConfig()->getDefaultEndPoint());

    }

    /**
     * @test
     */
    public function shouldInjectedDependencyWithContractFiller()
    {

        $oContainer = $this->getContainer();
        $oContractFillers = new ContractFillers([
                (new Contract(B1::class))
                    ->fillBy(
                        function () {
                            return new B1(new FrameworkConfig(__DIR__, 'default'));
                        }
                    )
            ]
        );

        $oInjector = new DependencyInjector($oContainer, $oContractFillers);
        $oResult = $oInjector->invokeMethod(new B0(), 'test');
        $this->assertEquals('default', $oResult->getConfig()->getDefaultEndPoint());

    }

    /**
     * @test
     */
    public function shouldNotInjectedDependencyWithContractFillerIfNotDefined()
    {

        $oContainer = $this->getContainer();
        $oContractFillers = new ContractFillers([
            ]
        );

        $oInjector = new DependencyInjector($oContainer, $oContractFillers);
        $this->expectException(InjectionException::class);
        $oInjector->invokeMethod(new B0(), 'test');

    }

    /**
     * @return EndPointDependencyContainer
     */
    private function getContainer()
    {
        $oConfig = new FrameworkConfig(
            __DIR__
            , 'none'
        );
        /** @noinspection PhpInternalEntityUsedInspection */
        $oManifest = new DispatchManifest($oConfig, new EndPointImmutable('none.none'));

        /** @noinspection PhpInternalEntityUsedInspection */
        $oContainer = new EndPointDependencyContainer(
            $this->getContext()->getEnvironment(),
            $oConfig,
            $oManifest,
            new UrlProvider($oManifest, [])
        );
        return $oContainer;
    }
}
