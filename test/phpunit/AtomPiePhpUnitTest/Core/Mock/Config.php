<?php
namespace AtomPiePhpUnitTest\Core\Mock {

    use AtomPie\Core\FrameworkConfig;
    use AtomPie\System\ClassShortcuts;
    use AtomPie\System\Namespaces;

    class Config
    {

        public static function get()
        {
            $oConfig = new FrameworkConfig(
                __DIR__.'/../../../AtomPieTestAssets/Resource/Context',
                'Main'
            );

            $oConfig->setEventClassShortcuts(new ClassShortcuts(
                new Namespaces([
                    "\\WorkshopTest\\Resource\\Component",
                    "\\WorkshopTest\\Resource",
                    "\\WorkshopTest\\Resource\\EndPoint",
                    '\\AtomPiePhpUnitTest\\Core\\Mock'
                ])
            ));

            return $oConfig;
        }

    }

}
