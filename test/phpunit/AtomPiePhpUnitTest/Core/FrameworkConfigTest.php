<?php
namespace AtomPiePhpUnitTest\Core;

use AtomPie\Core\Exception;
use AtomPie\Core\FrameworkConfig;

class FrameworkConfigTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function shouldLockConfig()
    {
        $oConfig = new FrameworkConfig(
            __DIR__,
            'default'
        );
        $oConfig->lock();
        $this->expectException(Exception::class);
        $oConfig->setMiddleware([]);
    }

    /**
     * @test
     */
    public function shouldAllowConfigurationBeforeLock()
    {
        $oConfig = new FrameworkConfig(
            __DIR__,
            'default'
        );
        $oConfig->setMiddleware([]);
        $this->assertTrue(is_array($oConfig->getMiddleware()));
    }
}
