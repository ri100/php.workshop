<?php
namespace AtomPiePhpUnitTest\System;

use AtomPie\System\Application;
use AtomPie\System\Context;
use AtomPie\System\Init;
use AtomPie\Core\Dispatch\DispatchManifest;
use WorkshopTest\Resource\Config\Config;

class InitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function shouldReturnApplication()
    {
        $oConfig = Config::get();
        $oDispatchManifest = DispatchManifest::factory($oConfig, $_REQUEST);
        $Context = new Context($oDispatchManifest->getEndPoint(), $oConfig);

        $oInit = new Init();
        $oApplication = $oInit->initApplication($Context, $oConfig, $oDispatchManifest, []);
        $this->assertTrue($oApplication instanceof Application);
    }
}
