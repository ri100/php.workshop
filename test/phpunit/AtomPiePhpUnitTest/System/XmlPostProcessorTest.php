<?php
namespace phpunit\AtomPiePhpUnitTest\System;

use AtomPie\System\ContentProcessor\XmlPostProcessor;

class XmlPostProcessorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @throws \AtomPie\System\Exception
     */
    public function shouldConvertArrayWithKeysToXml() {
        $aArray = [
            'test' => true,
            'embedded' => [
                'test' => 2
            ]
        ];

        $oProcessor = new XmlPostProcessor();
        $sXml = $oProcessor->convertToXml($aArray);
        $this->assertEquals(/** @lang xml */"<?xml version=\"1.0\"?>\n<Root><test>1</test><embedded><test>2</test></embedded></Root>",trim($sXml));
    }

    /**
     * @test
     * @throws \AtomPie\System\Exception
     */
    public function shouldConvertArrayToXml() {

        $oObject = new \stdClass();
        $oObject->test = true;
        $oObject->embedded = new \stdClass();
        $oObject->embedded->test = 2;

        $aArray = [
            true,
            'string',
            $oObject
        ];

        $oProcessor = new XmlPostProcessor();
        $sXml = $oProcessor->convertToXml($aArray);
        $this->assertEquals(/** @lang xml */"<?xml version=\"1.0\"?>\n<Root><_x0030_>1</_x0030_><_x0031_>string</_x0031_><_x0032_><test>1</test><embedded><test>2</test></embedded></_x0032_></Root>",trim($sXml));


    }

    /**
     * @test
     * @throws \AtomPie\System\Exception
     */
    public function shouldConvertStringToXml() {
        $oProcessor = new XmlPostProcessor();
        $sXml = $oProcessor->convertToXml('string');
        $this->assertEquals(/** @lang xml */
            "<Root><![CDATA[string]]></Root>",trim($sXml));
    }

    /**
     * @test
     * @throws \AtomPie\System\Exception
     */
    public function shouldConvertBoolToXml() {
        $oProcessor = new XmlPostProcessor();
        $sXml = $oProcessor->convertToXml(true);
        $this->assertEquals(/** @lang xml */"<Root>1</Root>",trim($sXml));
    }

    /**
     * @test
     * @throws \AtomPie\System\Exception
     */
    public function shouldConvertObjectToXml() {
        $oObject = new \stdClass();
        $oObject->test = true;
        $oObject->embedded = new \stdClass();
        $oObject->embedded->test = 2;

        $oProcessor = new XmlPostProcessor();
        $sXml = $oProcessor->convertToXml($oObject);
        $this->assertEquals(/** @lang xml */"<?xml version=\"1.0\"?>\n<Root><test>1</test><embedded><test>2</test></embedded></Root>",trim($sXml));
    }
}
