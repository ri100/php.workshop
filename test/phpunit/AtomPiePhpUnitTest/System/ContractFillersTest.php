<?php
namespace AtomPiePhpUnitTest\System;

use AtomPie\DependencyInjection\Contract;
use AtomPie\DependencyInjection\ContractFillers AS Fillers;

interface ContractFillerInterface {

}

class ContractFillersTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function shouldDeliverContractFillerAsClosure()
    {

        // No fall back to class

        $oContractFillers = new Fillers(
            [
                (new Contract(ContractFillerInterface::class))
                    ->fillBy(function() {})
            ]
        );

        $oContractFiller = $oContractFillers->getContractFillerFor(
            ContractFillerInterface::class
            , self::class
            , 'test'
        );

        $this->assertInstanceOf(\Closure::class, $oContractFiller);
        
    }

    /**
     * @test
     */
    public function shouldDeliverContractFillerWithInheritance() {

        // No fall back to class

        $oContractFillers = new Fillers(
            [
                (new \AtomPie\DependencyInjection\Contract(ContractFillerInterface::class))
                    ->fillBy('global')

                , (new \AtomPie\DependencyInjection\Contract(ContractFillerInterface::class))
                    ->forClass(self::class)
                    ->fillBy('class')

                , (new \AtomPie\DependencyInjection\Contract(ContractFillerInterface::class))
                    ->forClassAndMethod(self::class, 'test')
                    ->fillBy('classAndMethod')
            ]
        );

        $oContractFiller = $oContractFillers->getContractFillerFor(
            ContractFillerInterface::class
            , self::class
            , 'test'
        );

        $this->assertEquals('classAndMethod', $oContractFiller);

        // Fall back to class

        $oContractFillers = new Fillers(
            [
                (new Contract(ContractFillerInterface::class))
                    ->fillBy('global')

                , (new \AtomPie\DependencyInjection\Contract(ContractFillerInterface::class))
                ->forClass(self::class)
                ->fillBy('class')

            ]
        );

        $oContractFiller = $oContractFillers->getContractFillerFor(
            ContractFillerInterface::class
            , self::class
            , 'test'
        );

        $this->assertEquals('class', $oContractFiller);

        // Fall back to global

        $oContractFillers = new Fillers(
            [
                (new Contract(ContractFillerInterface::class))
                    ->fillBy('global')

            ]
        );

        $oContractFiller = $oContractFillers->getContractFillerFor(
            ContractFillerInterface::class
            , self::class
            , 'test'
        );

        $this->assertEquals('global', $oContractFiller);
    }

    /**
     * @test
     */
    public function shouldDeliverContractFillerForFirstClassAndMethod()
    {

        // Order matters

        $oContractFillers = new Fillers(
            [
                (new Contract(ContractFillerInterface::class))
                    ->forClassAndMethod(self::class, 'test')
                    ->fillBy('classAndMethod')
                ,
                (new \AtomPie\DependencyInjection\Contract(ContractFillerInterface::class))
                    ->forClassAndMethod(self::class, 'test')
                    ->fillBy('classAndMethod1') // Duplicated contract filler - will not reach this definition
                ,
                (new Contract(ContractFillerInterface::class))
                    ->fillBy('global')
                ,
                (new Contract(ContractFillerInterface::class))
                    ->forClass(self::class)
                    ->fillBy('class')

            ]
        );

        $oContractFiller = $oContractFillers->getContractFillerFor(
            ContractFillerInterface::class
            , self::class
            , 'test'
        );

        $this->assertEquals('classAndMethod', $oContractFiller);
    }
}
