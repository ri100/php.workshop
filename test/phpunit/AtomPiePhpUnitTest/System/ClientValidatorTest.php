<?php
namespace AtomPiePhpUnitTest\System;


use AtomPie\System\Dispatch\ClientConstrain;
use AtomPie\System\Dispatch\ClientValidator;
use AtomPie\System\Dispatch\DispatchException;
use AtomPie\Web\Connection\Http\Content;
use AtomPie\Web\Connection\Http\Header\ContentType;
use AtomPie\Web\Connection\Http\Request;

class ClientValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function shouldValidateMethod() {

        $oClient = new ClientConstrain();
        $oClient->hasMethod('get');

        $oRequest = new Request();
        $oRequest->setMethod('get');

        $oValidator = new ClientValidator($oClient);
        $this->assertTrue($oValidator->validate($oRequest));
        $this->assertNull($oValidator->getError());

        $oClient = new ClientConstrain();
        $oClient->hasMethod('post');

        $oValidator = new ClientValidator($oClient);
        $this->assertFalse($oValidator->validate($oRequest));
        $this->assertInstanceOf(DispatchException::class, $oValidator->getError());

    }

    /**
     * @test
     */
    public function shouldValidateContentType() {

        $oClient = new ClientConstrain();
        $oClient->hasContentType('application/json');

        $oRequest = new Request();
        $oRequest->setMethod('post');
        $oRequest->setContent(new Content('',new ContentType('application/json')));

        $oValidator = new ClientValidator($oClient);
        $this->assertTrue($oValidator->validate($oRequest));
        $this->assertNull($oValidator->getError());

        $oClient = new ClientConstrain();
        $oClient->hasMethod('text/html');

        $oValidator = new ClientValidator($oClient);
        $this->assertFalse($oValidator->validate($oRequest));
        $this->assertInstanceOf(DispatchException::class, $oValidator->getError());

    }

    /**
     * @test
     */
    public function shouldValidateAccept() {

        $oClient = new ClientConstrain();
        $oClient->accept('application/json');

        $sOld = @$_SERVER['HTTP_ACCEPT'];
        $_SERVER['HTTP_ACCEPT'] = 'application/json';
        $oRequest = new Request();
        $oRequest->load();
        $_SERVER['HTTP_ACCEPT'] = $sOld;

        $oValidator = new ClientValidator($oClient);
        $this->assertTrue($oValidator->validate($oRequest));
        $this->assertNull($oValidator->getError());

        $oClient = new ClientConstrain();
        $oClient->accept('text/html');

        $oValidator = new ClientValidator($oClient);
        $this->assertFalse($oValidator->validate($oRequest));
        $this->assertInstanceOf(DispatchException::class, $oValidator->getError());

    }

    /**
     * @test
     */
    public function shouldValidateAcceptWithPriority() {

        $oClient = new ClientConstrain();
        $oClient->accept('application/json');

        $sOld = @$_SERVER['HTTP_ACCEPT'];
        $_SERVER['HTTP_ACCEPT'] = 'application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
        $oRequest = new Request();
        $oRequest->load();


        $oValidator = new ClientValidator($oClient);
        $this->assertTrue($oValidator->validate($oRequest));
        $this->assertNull($oValidator->getError());

        $_SERVER['HTTP_ACCEPT'] = $sOld;

    }
}
