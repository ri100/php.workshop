<?php
namespace AtomPiePhpUnitTest\System;

use AtomPie\Core\Dispatch\EndPointImmutable;
use AtomPie\System\Context;
use AtomPie\System\Dispatch\DispatchException;
use AtomPie\System\Dispatch\EndPointClass;
use AtomPie\Web\Connection\Http\Response;
use AtomPiePhpUnitTest\FrameworkTest;
use AtomPieTestAssets\Resource\Mock\DependentClassInterface;

class ContextTest extends FrameworkTest
{

    /**
     * @test
     */
    public function shouldNotLoadEndPointIfNoClassNameDefined() {
        $this->expectException(DispatchException::class);
        $this->_getContext('EmptyMockEndPointContext.method');
    }

    private function _getContext($sEndPoint) {
        return new Context(
            new EndPointImmutable($sEndPoint)
            , self::getDefaultConfig('default')
        );
    }

    /**
     * @test
     */
    public function shouldLoadEndPointContext() {

        $oContext = $this->_getContext('NewMockEndPointContext.method');

        // Response

        $oResponse = $oContext->getEnvironment()->getResponse();
        $this->assertInstanceOf(Response::class, $oResponse);
        $oResponse->setContentType('application/json');
        $this->assertEquals('application/json', $oContext->getEnvironment()->getResponse()->getContent()->getContentType()->getValue());

        // ContentFillers

        $oContractFillers = $oContext->getContractFillers();
        $pClosure = $oContractFillers->getContractFillerFor(DependentClassInterface::class, 'class', 'method');
        if($pClosure instanceof \Closure) {
            $mResult = $pClosure();
            $this->assertInstanceOf(\stdClass::class, $mResult);
        }

        // Route
        
        $oRoute = $oContext->getEndPointClass();
        $this->assertInstanceOf(EndPointClass::class, $oRoute);
    }
}
