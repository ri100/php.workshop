<?php
namespace WorkshopTest {

    use AtomPie\Core\Dispatch\EndPointImmutable;
    use AtomPie\Core\Dispatch\EventSpecImmutable;
    use AtomPie\System\Context;
    use AtomPie\Web\Connection\Http\Request;
    use WorkshopTest\Resource\Config\Config;

    class RequestFactory
    {

        /**
         * @param string $sDefaultEndPointSpecString
         * @param null $sDefaultEventSpecString
         * @param array $aParams
         * @return Request
         */
        public static function produce(
            $sDefaultEndPointSpecString = 'Default.index',
            $sDefaultEventSpecString = null,
            array $aParams = null
        ) {
            $_REQUEST = [];
            $_REQUEST[EndPointImmutable::END_POINT_QUERY] = $sDefaultEndPointSpecString;
            if (isset($sDefaultEventSpecString)) {
                $_REQUEST[EventSpecImmutable::EVENT_QUERY] = $sDefaultEventSpecString;
            }

            if (is_array($aParams)) {
                foreach ($aParams as $sKey => $mValue) {
                    $_REQUEST[$sKey] = $mValue;
                }
            }

            $oContext = new Context(new EndPointImmutable('Main'), Config::get());

            $oRequest = $oContext->getEnvironment()->getRequest();

            return $oRequest;

        }
    }

}
