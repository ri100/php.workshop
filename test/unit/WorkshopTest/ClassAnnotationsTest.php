<?php
namespace WorkshopTest;

require_once __DIR__ . '/../Config.php';

use AtomPie\Annotation\AnnotationParser;
use AtomPie\AnnotationTag\Authorize;
use AtomPie\AnnotationTag\SaveState;

/**
 * Class Annotated
 * @package WorkshopTest
 * @Authorize(ResourceIndex="WorkshopTest\Annotated", AuthType="Basic",AuthToken="risto:risto")
 */
class Annotated
{
    public function annotated()
    {

    }
}

class ClassAnnotationsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function shouldParseClassAnnotationFromPhpDoc()
    {

        // Set default set of Annotations
        $aDefaultAnnotationMapping = array(
            'SaveState' => SaveState::class,
            'Authorize' => Authorize::class,
        );

        $oParser = new AnnotationParser();
        $oAnnotations = $oParser->getAnnotationsFromObjectOrMethod(
            $aDefaultAnnotationMapping,
            new Annotated()
        );

        $oAuthorize = $oAnnotations->getFirstAnnotationByType(Authorize::class);
        $this->assertTrue($oAuthorize->AuthType == 'Basic');
    }

    /**
     * @test
     */
    public function shouldParseClassAnnotationFromPhpDocWhenCalledDouble()
    {

        // Set default set of Annotations
        $aDefaultAnnotationMapping = array(
            'Authorize' => Authorize::class
        );

        $oParser = new AnnotationParser();
        $oAnnotations = $oParser->getAnnotationsFromObjectOrMethod(
            $aDefaultAnnotationMapping,
            new Annotated()
        );

        $this->assertTrue($oAnnotations[Authorize::class][0] instanceof Authorize);

        $oAnnotations = $oParser->getAnnotationsFromObjectOrMethod(
            $aDefaultAnnotationMapping,
            new Annotated()
        );

        $this->assertTrue($oAnnotations[Authorize::class][0] instanceof Authorize);
    }
}
