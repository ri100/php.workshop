<?php
namespace WorkshopTest\Resource\Config {

    use AtomPie\Core\Dispatch\DispatchManifest;
    use AtomPie\Core\FrameworkConfig;
    use AtomPie\File\FileProcessorFactory;
    use AtomPie\Gui\Component\ComponentProcessorFactory;
    use AtomPie\System\Context;
    use AtomPie\System\ClassShortcuts;
    use AtomPie\System\Namespaces;

    class Config
    {

        public static function get()
        {
            $oConfig = new FrameworkConfig(
                __DIR__.'/../Context'
                , 'Main'
            );

            $oConfig->setEventClassShortcuts(new ClassShortcuts(
                new Namespaces([
                    "\\WorkshopTest\\Resource\\Component",
                    "\\WorkshopTest\\Resource",
                    "\\WorkshopTest\\Resource\\EndPoint",
                ])
            ));
            $oDispatchManifest = DispatchManifest::factory($oConfig, $_REQUEST);
            $oContext = new Context(
                $oDispatchManifest->getEndPoint(),
                $oConfig
            );

            $sViewFolder = __DIR__ . '/../../../WorkshopTest/Resource/Theme';

            $oConfig->setContentProcessors([
                new FileProcessorFactory(),
                new ComponentProcessorFactory($sViewFolder, $oContext->getEnvironment(), $oDispatchManifest)
            ]);

            return $oConfig;
        }

    }

}
