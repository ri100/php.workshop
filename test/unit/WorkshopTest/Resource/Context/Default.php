<?php
use WorkshopTest\Resource\EndPoint\DefaultController;

return function(\AtomPie\System\EndPoint $oEndPoint) {
    $oEndPoint->setClassName(DefaultController::class);
};