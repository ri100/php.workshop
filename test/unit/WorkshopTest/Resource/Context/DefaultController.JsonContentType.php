<?php
return function(\AtomPie\System\EndPoint $oEndPoint) {
    $oEndPoint->onResponse()
        ->addHeader(\AtomPie\Web\Connection\Http\Header::CONTENT_TYPE,'application/json');
};