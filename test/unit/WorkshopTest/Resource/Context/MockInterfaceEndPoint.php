<?php
use WorkshopTest\Resource\EndPoint\MockInterfaceEndPoint;

return function(\AtomPie\System\EndPoint $oEndPoint) {
    $oEndPoint->setClassName(MockInterfaceEndPoint::class);
};