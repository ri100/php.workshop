<?php
use AtomPie\System\EndPoint;
use AtomPie\Web\Connection\Http\Header;

return function(EndPoint $oEndPoint) {
    $oEndPoint->onResponse()
        ->addHeader(Header::CONTENT_TYPE,'application/xml');
};