<?php

use AtomPie\System\EndPoint;
use AtomPie\Web\Connection\Http\Header;
use WorkshopTest\Resource\UseCase\UseCase;

return function(EndPoint $oEndPoint) {
    
    $oEndPoint->setClassName(UseCase::class);
    $oEndPoint->onResponse()
        ->addHeader(Header::CONTENT_TYPE,'application/json');

};
