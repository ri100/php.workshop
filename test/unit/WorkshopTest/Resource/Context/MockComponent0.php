<?php
use WorkshopTest\Resource\Component\MockComponent0;

return function(\AtomPie\System\EndPoint $oEndPoint) {
    $oEndPoint->setClassName(MockComponent0::class);
};