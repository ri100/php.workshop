<?php
namespace WorkshopTest\Resource\EndPoint {

    require_once __DIR__ . '/../Page/DefaultPage.php';
    require_once __DIR__ . '/../Page/NotPage.php';

    use AtomPie\AnnotationTag\SaveState;
    use Generi\Object;
    use AtomPie\Boundary\Core\IAmService;
    use AtomPie\AnnotationTag\Documentation;
    use AtomPie\AnnotationTag\EndPointParam;
    use AtomPie\AnnotationTag\Template;
    use AtomPie\Web\Connection\Http\Url\Param;
    use WorkshopTest\Resource\Component\Yes;
    use WorkshopTest\Resource\Page\DefaultPage;
    use WorkshopTest\Resource\Page\NotPage;

    /**
     * Class DefaultController
     * Holds all endpoints
     * @Template(File="DefaultController.mustache")
     */
    class DefaultController extends Object
    {

        /**
         * @return DefaultPage
         */
        public function index()
        {
            $oPage = new DefaultPage();
            $oPage->Inner->handleEvent('OutsideEvent', function () use ($oPage) {
                $oPage->Inner->EventFlag = 'OutsideYes';
            });

            return $oPage;
        }

        /**
         * @SaveState(Param="Name")
         * @param $Name
         * @return mixed
         */
        public function simpleParam($Name)
        {
            return $Name;
        }

        /**
         * @return string
         */
        public function yes()
        {
            return new Yes('Test');
        }

        /**
         * @return DefaultPage
         */
        public function annotatedMethod()
        {
            return new DefaultPage();
        }

        /**
         * Return object that is not Page type.
         *
         * @return NotPage
         */
        public function NotPage()
        {
            return new NotPage();
        }

        /**
         * @return DefaultPage
         */
        public function JsonContentType()
        {
            return new DefaultPage();
        }

        /**
         * @return null
         */
        public function noPage()
        {
            return null;
        }

        /**
         *
         * This is documented
         *      endpoint
         *
         * {{ File }}
         *
         * sfsd
         *
         * @EndPointParam(Name="Id",Description="IdDesc")
         * @EndPointParam(Name="Integer",Description="IntDesc")
         * @EndPointParam(Name="oService",Description="ServiceDesc")
         * @Documentation(Name="File",File="WorkshopTest/Resource/EndPoint/DefaultController.php")
         *
         * @param Param $Id
         * @param Param\Number $Integer
         * @param IAmService $oService
         */
        public function notEndPoint(Param $Id = null, Param\Number $Integer, IAmService $oService = null)
        {
            
        }
    }

}
