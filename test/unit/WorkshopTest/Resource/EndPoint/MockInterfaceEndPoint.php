<?php
namespace WorkshopTest\Resource\EndPoint {

    require_once __DIR__ . '/../Page/DefaultPage.php';
    require_once __DIR__ . '/../Page/NotPage.php';

    use Generi\Object;
    use AtomPie\AnnotationTag\Template;
    use AtomPie\Web\Connection\Http\Url\Param;
    use WorkshopTest\Resource\Page\MockPage;

    /**
     * @Template(File="Default/MockInterfaceEndPoint.mustache")
     */
    class MockInterfaceEndPoint extends Object
    {

        public static $staticCall = false;
        public static $staticCallParam;

        /**
         * @return MockPage
         */
        public function index()
        {
            $oPage = new MockPage();
            return $oPage;
        }

        /**
         * @param Param $param
         * @return bool
         */
        public static function staticCall(Param $param = null)
        {
            self::$staticCall = true;
            if (!$param->isNull()) {
                self::$staticCallParam = $param->getValue();
            }
            return true;
        }

        /**
         * @return bool
         */
        public static function staticJsonCall()
        {
            return true;
        }

    }

}
