<?php
namespace WorkshopTest\Resource\Component {

    use AtomPie\AnnotationTag\Template;
    use AtomPie\Gui\Component;

    /**
     * Class MultiLevel1
     * @package WorkshopTest\Resource\Component
     * @Template(File="Default/MultiLevel1.mustache")
     */
    class MultiLevel1 extends Component
    {
        public function __create()
        {
            $this->Property1 = 'Wlasciwosc1';
            $this->Property2 = new \stdClass();
            $this->Property2->Property1 = 'std11';
        }
    }

}
