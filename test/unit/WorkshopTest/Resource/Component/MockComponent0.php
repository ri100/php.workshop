<?php
namespace WorkshopTest\Resource\Component;

use AtomPie\Gui\Component;
use AtomPie\Web\Boundary\IAmEnvironment;
use AtomPie\AnnotationTag\Template;
use AtomPie\Html\Tag\Head;

/**
 * @property string EventFlag
 * @property bool $IsFactoryInvoked
 * @property bool IsProcessInvoked
 * @package WorkshopTest\Resource\Component
 * @Template(File="Default/MockComponent0.mustache")
 */
class MockComponent0 extends Component
{

    public $oEnv;
    public $oHead;

    public function __create()
    {
        $this->EventFlag = 'Empty';
    }

    public function __factory(IAmEnvironment $oEnv)
    {
        $this->IsFactoryInvoked = true;
        $this->oEnv = $oEnv;
    }

    public function __process(Head $oHead)
    {
        $this->IsProcessInvoked = true;
        $this->oHead = $oHead;
    }

    public function clickEvent()
    {
        $this->EventFlag = 'Yes';
    }

    /**
     * @return MockComponent0
     */
    public static function EndPoint()
    {
        return new self();
    }

}