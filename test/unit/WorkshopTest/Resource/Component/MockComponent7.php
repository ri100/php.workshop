<?php
namespace WorkshopTest\Resource\Component {

    use AtomPie\System\Application;
    use AtomPie\Core\Dispatch\DispatchManifest;
    use AtomPie\Web\Boundary\IAmEnvironment;
    use WorkshopTest\Resource\Service;
    use AtomPie\Gui\Component;
    use AtomPie\Html\ScriptsCollection;
    use AtomPie\Html\Tag\Head;
    use AtomPie\Web\Connection\Http\Url\Param;
    use AtomPie\AnnotationTag\Template;

    /**
     * @Template(File="MockComponent7.mustache")
     * Class MockComponent7
     * @package WorkshopTest\Resource\Component
     */
    class MockComponent7 extends Component
    {

        public $ComponentParam;
        public $Param;
        public $Head;
        public $Context;

        public function __create(
            /** @noinspection PhpUnusedParameterInspection */
            Component\ComponentParam $ComponentParam,
            Param $Param,
            Head $Head,
            ScriptsCollection $ScriptsCollection,
            IAmEnvironment $context,
            Application $Application,
            DispatchManifest $DispatchManifest,
            Service $Service
        ) {

        }

        public function __factory(
            /** @noinspection PhpUnusedParameterInspection */
            Component $Component,
            Component\ComponentParam $ComponentParam,
            Param $Param,
            Head $Head,
            ScriptsCollection $ScriptsCollection,
            IAmEnvironment $context,
            Application $Application,
            DispatchManifest $DispatchManifest,
            Service $Service
        ) {
            $this->ComponentParam = $ComponentParam;
            $this->Param = $Param;
            $this->Head = $Head;
            $this->Context = $context;
        }

        public function __process(
            Component $Component,
            Component\ComponentParam $ComponentParam,
            Param $Param,
            Head $Head,
            ScriptsCollection $ScriptsCollection,
            IAmEnvironment $context,
            Application $Application,
            DispatchManifest $DispatchManifest,
            Service $Service
        ) {

        }

        public function eventEvent()
        {

        }

        /**
         * @return MockComponent7
         */
        public static function EndPoint()
        {
            return new self();
        }
    }

}

