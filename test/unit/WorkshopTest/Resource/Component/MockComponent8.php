<?php
namespace WorkshopTest\Resource\Component {

    use AtomPie\AnnotationTag\Template;

    /**
     * Class MockComponent1
     * @package WorkshopTest\Resource\Component
     * @Template(File="WorkshopTest/Resource/Theme/Default/MockComponent1.mustache")
     */
    class MockComponent8
    {

        /**
         * @param $test
         * @return string
         */
        public function requireJsonParam($test)
        {
            return 'requireJsonParam';
        }

        /**
         * @param $test
         * @return string
         */
        public function requireXmlParam($test)
        {
            return 'requireXmlParam';
        }

    }
}
