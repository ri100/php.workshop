<?php
namespace WorkshopTest\Resource\Operation {

    use AtomPie\Web\Boundary\IAmEnvironment;
    use Generi\Object;
    use WorkshopTest\Resource\Repo\DataRepository;

    class UseCaseApi extends Object
    {

        /**
         * @return bool
         */
        public function getJson()
        {
            return true;
        }

        /**
         * @return bool
         */
        public function twoContentTypes()
        {
            return true;
        }

        /**
         * @return array
         */
        public function getArrayAsXml()
        {
            return array('tag' => 'value', 'nested' => array('node' => 1));
        }

        /**
         * @return object
         */
        public function getObjectAsXml()
        {
            $oObject = new \stdClass();
            $oObject->tag = 'value';
            $oObject->nested = new \stdClass();
            $oObject->nested->node = 1;

            return $oObject;
        }

        /**
         * @param DataRepository $oRepo
         * @return array
         */
        public function getDataFromRepo(DataRepository $oRepo)
        {
            return $oRepo->loadData();
        }

        /**
         * @param IAmEnvironment $oEnv
         * @return string
         */
        public function getDataFromSession(IAmEnvironment $oEnv)
        {
            $sSessionValue = $oEnv->getSession()->get('MySession1');
            return $sSessionValue;
        }

    }

}
