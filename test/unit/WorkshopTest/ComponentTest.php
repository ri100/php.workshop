<?php
namespace WorkshopTest;

use AtomPie\Core\Dispatch\DispatchManifest;
use AtomPie\Core\Dispatch\EndPointImmutable;
use AtomPie\Core\FrameworkConfig;
use AtomPie\Gui\Component\ComponentProcessorFactory;
use AtomPie\System\Application;
use AtomPie\System\ContentProcessor;
use AtomPie\System\Context;
use AtomPie\System\Dispatch\DispatchException;
use AtomPie\System\Environment;
use AtomPie\Web\Boundary\IAmEnvironment;
use AtomPie\Gui\Component\ComponentParam;
use AtomPie\Html\Tag\Head;
use AtomPie\Web\Connection\Http\Header\ContentType;
use AtomPie\Web\Connection\Http\Url\Param;
use WorkshopTest\Resource\Component\MockComponent0;
use WorkshopTest\Resource\Component\MockComponent7;
use WorkshopTest\Resource\Component\MultiLevel;
use WorkshopTest\Resource\Config\Config;

class ComponentTest extends ScaffoldTest
{

    /**
     * @param Application $oApplication
     * @param $oConfig
     * @return mixed
     * @throws DispatchException
     * @throws \AtomPie\System\Exception
     */
    private function runApp(Application $oApplication, $oConfig)
    {
        $oEndPointSpec = $oApplication->getDispatcher()->getDispatchManifest()->getEndPoint();
        $oContext = new Context($oEndPointSpec, $oConfig);
        return $oApplication->run($oConfig, $oContext);
    }

    public function testEcho()
    {
        $sEndPoint = MockComponent7::Type()->getName() . '.EndPoint';
        RequestFactory::produce(
            $sEndPoint,
            null,
            array(
                'Param' => 'Param',
                'ComponentParam' => array('MockComponent7' => 'MockComponent7')
            ));

        $oConfig = Config::get();
        $oApplication = Boot::upRequest($oConfig);

        $this->assertTrue(false !== strstr($this->runApp($oApplication, $oConfig)->__toString(), 'MockComponent7'));
    }

    public function testComponentFactory_InsideConstructor()
    {
        $oComponent = new MockComponent0();
        // Component is factored outside the __constructor so
        // IsFactored should not be true
        $this->assertTrue(!isset($oComponent->IsFactoryInvoked));
    }

    public function testComponentFactory_FullProcess_AllDiParams()
    {
        $sEndPoint = MockComponent7::Type()->getName() . '.EndPoint';
       RequestFactory::produce(
            $sEndPoint,
            null,
            array(
                'Param' => 'Param',
                'ComponentParam' => array('MockComponent7' => 'MockComponent7')
            ));

        $oConfig = Config::get();
        $oApplication = Boot::upRequest($oConfig);

        $oPhpUnit = $this;
        /** @noinspection PhpUnusedParameterInspection */
        $oApplication->handleEvent(Application::EVENT_BEFORE_RENDERING,
            function ($oSender, MockComponent7 $oContent) use ($oPhpUnit) {
                $this->assertTrue($oContent->Context instanceof IAmEnvironment);
                $this->assertTrue($oContent->ComponentParam instanceof ComponentParam);
                $this->assertTrue($oContent->Head instanceof Head);
                $this->assertTrue($oContent->Param instanceof Param);
            }
        );
        $oConfig = Config::get();
        $this->runApp($oApplication, $oConfig);
    }

    public function testComponentFactory_FullProcess()
    {
        $sEndPoint = MockComponent0::Type()->getName() . '.EndPoint';
        $oConfig = Config::get();
        $oApplication = $this->getApp($sEndPoint, $oConfig);
        $oPhpUnit = $this;
        /** @noinspection PhpUnusedParameterInspection */
        $oApplication->handleEvent(Application::EVENT_BEFORE_RENDERING,
            function ($oSender, MockComponent0 $oContent) use ($oPhpUnit) {
                $oPhpUnit->assertTrue($oContent->IsFactoryInvoked === true);
                $oPhpUnit->assertInstanceOf(IAmEnvironment::class,$oContent->oEnv);
            });
        $oConfig = Config::get();
        $this->runApp($oApplication, $oConfig);
    }

    public function testComponentProcess_FullProcess()
    {
        $sEndPoint = MockComponent0::Type()->getName() . '.EndPoint';

        $oConfig = Config::get();
        $oApplication = $this->getApp($sEndPoint, $oConfig);

        $oPhpUnit = $this;
        /** @noinspection PhpUnusedParameterInspection */
        $oApplication->handleEvent(Application::EVENT_BEFORE_RENDERING,
            function ($oSender, MockComponent0 $oContent) use ($oPhpUnit) {
                $oPhpUnit->assertTrue($oContent->IsProcessInvoked === true);
                $oPhpUnit->assertTrue($oContent->oHead instanceof Head);
            }
        );
        $oConfig = Config::get();
        $this->runApp($oApplication, $oConfig);
    }

    /**
     * @test
     * @throws \AtomPie\EventBus\Exception
     */
    public function shouldProcessComponent()
    {
        $sViewFolder = __DIR__ . '/Resource/Theme';
        $oConfig = new FrameworkConfig(__DIR__.'/Resource/Context', 'Main');
        $oEnvironment = Environment::getInstance();

        $oComponent = new MultiLevel();
        $oProcessor = new ContentProcessor();
        $oDispatchManifest = new DispatchManifest(
            $oConfig,
            new EndPointImmutable('Main'),
            null
         );

        $oContentProcessor = new ComponentProcessorFactory($sViewFolder, $oEnvironment, $oDispatchManifest);
        $oContentProcessor->configureProcessor($oProcessor);
        echo $oProcessor->finish($oComponent, new ContentType('text/html'));
    }

    /**
     * @param $sEndPoint
     * @param $oConfig
     * @return Application
     */
    private function getApp($sEndPoint, $oConfig)
    {
        return Boot::up($oConfig, $sEndPoint);
    }
}
