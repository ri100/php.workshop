<?php
namespace WorkshopTest\EndPoint {

    use AtomPie\Core\Dispatch\EndPointException;
    use AtomPie\Core\Dispatch\EndPointImmutable;
    use AtomPie\Core\Dispatch\EventSpecImmutable;
    use AtomPie\Core\Dispatch\QueryString;
    use AtomPie\System\Application;
    use AtomPie\System\Context;
    use AtomPie\System\Dispatch\DispatchException;
    use AtomPie\Web\Connection\Http\Response;
    use WorkshopTest\Boot;
    use WorkshopTest\Resource\Component\MockComponent0;
    use WorkshopTest\Resource\Config\Config;
    use WorkshopTest\Resource\EndPoint\DefaultController;
    use WorkshopTest\Resource\EndPoint\MockInterfaceEndPoint;
    use WorkshopTest\Resource\Operation\UseCaseApi;
    use WorkshopTest\Resource\Page\DefaultPage;

    require_once __DIR__ . '/../../Config.php';

    /**
     * Class DispatcherTest
     * @package WorkshopTest\EndPoint
     */
    class DispatcherTest extends \PHPUnit_Framework_TestCase
    {
        /**
         * @param Application $oApplication
         * @param $oConfig
         * @return mixed
         * @throws DispatchException
         */
        private static function runApp(Application $oApplication, $oConfig)
        {
            $oEndPointSpec = $oApplication->getDispatcher()->getDispatchManifest()->getEndPoint();
            $oContext = new Context($oEndPointSpec, $oConfig);
            return $oApplication->run($oConfig, $oContext);
        }

        protected function setUp()
        {
            $_REQUEST = array();
            parent::setUp();
        }

        protected function tearDown()
        {
            $_REQUEST = array();
            parent::tearDown();
        }

        public function testDispatcher_EndPoint()
        {
            $oConfig = Config::get();
            $oApplication = $this->getApp(DefaultController::Type()->getName() . '.index', $oConfig);

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue($oResponse->getContent()->getContentType()->isHtml());
        }

        public function testDispatcher_EndPoint_SimpleParam()
        {
            $oConfig = Config::get();
            $oApplication = $this->getApp(
                DefaultController::Type()->getName() . '.simpleParam',
                $oConfig,
                null,
                null,
                ['Name'=>"Ian"]
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue($oResponse->getContent()->get() == 'Ian');
            $this->assertTrue($oResponse->getContent()->getContentType()->isHtml());
        }

        public function testDispatcher_EndPoint_AnnotatedMethod()
        {

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                DefaultController::Type()->getName() . '.annotatedMethod',
                $oConfig
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue($oResponse->getContent()->getContentType()->isHtml());
        }

        public function testDispatcher_EndPoint_JsonContentType()
        {

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                DefaultController::Type()->getName() . '.JsonContentType',
                $oConfig
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue($oResponse->getContent()->getContentType()->isJson());
        }

        public function testDispatcher_EndPoint_NoEndPoint()
        {

            $this->expectException(DispatchException::class);
            $oConfig = Config::get();
            $this->getApp(
                'NO_END_POINT.NO_METHOD',
                $oConfig
            );
        }

        public function testDispatcher_EndPoint_NoActionMethod()
        {

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                'DefaultController.NO_METHOD',
                $oConfig
            );

            $this->expectException(DispatchException::class);
            self::runApp($oApplication, $oConfig);

        }

        public function testDispatcher_EndPoint_ReturnsNotPageType()
        {

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                DefaultController::Type()->getName() . '.NotPage',
                $oConfig
            );

            $this->expectException(DispatchException::class);
            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getContent()->getContentLength() == 0);
            $this->assertTrue($oResponse->getContent()->get() == null);
        }

        public function testDispatcher_EndPoint_EmptyAction()
        {
            $this->expectException(EndPointException::class);

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                DefaultController::Type()->getName() . '.',
                $oConfig
            );

            self::runApp($oApplication, $oConfig);
        }

        // UseCaseApi

        public function testDispatcher_Operation_Json()
        {

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                UseCaseApi::Type()->getName() . '.getJson',
                $oConfig,
                'WorkshopTest\Resource\Operation'
            );

            $oResponse = self::runApp($oApplication, $oConfig);

            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue($oResponse->getContent()->getContentType()->isJson());

            $this->assertTrue(json_decode($oResponse->getContent()->get()) == true);
        }

        public function testDispatcher_Operation_XmlFromArray()
        {

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                UseCaseApi::Type()->getName() . '.getArrayAsXml',
                $oConfig,
                'WorkshopTest\Resource\Operation'
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue($oResponse->getContent()->getContentType()->isXml());
            $this->assertTrue(false !== strstr($oResponse->getContent()->get(), '<tag>value</tag>'));
        }

        public function testDispatcher_Operation_XmlFromObject()
        {

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                UseCaseApi::Type()->getName() . '.getObjectAsXml',
                $oConfig,
                'WorkshopTest\Resource\Operation'
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue($oResponse->getContent()->getContentType()->isXml());
            $this->assertTrue(false !== strstr($oResponse->getContent()->get(), '<tag>value</tag>'));
        }

        public function testDispatcher_Operation_TwoContentTypes()
        {

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                UseCaseApi::Type()->getName() . '.twoContentTypes',
                $oConfig,
                'WorkshopTest\Resource\Operation'
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue($oResponse->getContent()->getContentType()->isJson());
        }

        // Event

        public function testDispatcher_EndPoint_CatchEvent()
        {

            $sEndPoint = DefaultController::Type()->getName() . '.index';
            $sEventSpecString = 'MockComponent0.Inner.event';

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig,
                'WorkshopTest\Resource\EndPoint',
                $sEventSpecString
            );

            $oThat = $this;

            /** @noinspection PhpUnusedParameterInspection */
            $oApplication->handleEvent(Application::EVENT_AFTER_END_POINT_INVOKE,
                function ($oApp, DefaultPage $oEndPointContent) use ($oThat) {
                    $oEndPointContent->Inner->handleEvent('event', function () use ($oThat) {
                        $oThat->assertTrue(true);
                    });
                });

            self::runApp($oApplication, $oConfig);

        }

        public function testDispatcher_EndPoint_Default()
        {

            $sEndPoint = MockComponent0::Type()->getName();

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig
            );

            /**
             * @var Response $oResponse
             */
            $oResponse = self::runApp($oApplication, $oConfig);
            echo $oResponse;
        }


        public function testDispatcher_EndPoint_Event()
        {

            $sEndPoint = DefaultController::Type()->getName() . '.index';
            $sEventSpecString = QueryString::urlEscape('WorkshopTest\\Resource\\Page\\DefaultPage.Top.event');

            $_REQUEST = [];
            $_REQUEST[EndPointImmutable::END_POINT_QUERY] = $sEndPoint;
            $_REQUEST[EventSpecImmutable::EVENT_QUERY] = $sEventSpecString;

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig,
                'WorkshopTest\Resource\EndPoint',
                $sEventSpecString
            );

            $oThat = $this;

            /** @noinspection PhpUnusedParameterInspection */
            $oApplication->handleEvent(Application::EVENT_AFTER_END_POINT_INVOKE,
                function ($oApp, DefaultPage $oEndPointContent) use ($oThat) {
                    $oEndPointContent->handleEvent('event', function () use ($oThat) {
                        $oThat->assertTrue(true);
                    });
                });

            $oResponse = self::runApp($oApplication, $oConfig);

            $this->assertTrue($oApplication->getDispatcher()->getDispatchManifest()->hasEventSpec());
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue(strstr($oResponse->getContent()->get(), 'Yes') !== false);

        }

        public function testDispatcher_EndPoint_OutsideComponentEvent()
        {

            $sEndPoint = DefaultController::Type()->getName() . '.index';
            $sEventSpecString = QueryString::urlEscape('WorkshopTest\\Resource\\Component\\MockComponent0.Inner.OutsideEvent');

            $_REQUEST = [];
            $_REQUEST[EndPointImmutable::END_POINT_QUERY] = $sEndPoint;
            $_REQUEST[EventSpecImmutable::EVENT_QUERY] = $sEventSpecString;

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig,
                'WorkshopTest\Resource\EndPoint',
                $sEventSpecString
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oApplication->getDispatcher()->getDispatchManifest()->hasEventSpec());
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue(strstr($oResponse->getContent()->get(), 'OutsideYes') !== false);

        }

        public function testDispatcher_EndPoint_NoEvent()
        {

            $sEndPoint = DefaultController::Type()->getName() . '.index';

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue(strstr($oResponse->getContent()->get(), 'No') !== false);

        }


        public function testDispatcher_UseCaseEndPointWithInjectedRepository_Access()
        {
            $sEndPoint = UseCaseApi::Type()->getName() . '.getDataFromRepo';

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig,
                'WorkshopTest\Resource\Operation'
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $oJson = $oResponse->getContent()->decodeAsJson();
            $this->assertTrue(in_array('data', $oJson));
        }

        public function testDispatcher_UseCaseEndPointWithInjectedSession_Access()
        {
            $_SESSION['MySession1'] = 'session-data1';
            $sEndPoint = UseCaseApi::Type()->getName() . '.getDataFromSession';

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig,
                'WorkshopTest\Resource\Operation'
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $oJson = $oResponse->getContent()->decodeAsJson();
            $this->assertTrue($oJson == 'session-data1');
        }

        public function testDispatcher_EndPointWithInjectedHead_In_Component()
        {
            $sEndPoint = MockInterfaceEndPoint::Type()->getName() . '.index';

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig
            );
            $oResponse = self::runApp($oApplication, $oConfig);
            $sHtml = $oResponse->getContent()->get();
            $this->assertTrue(strstr($sHtml, 'test.js') !== false);
        }

        public function testDispatcher_EndPointWithComponent_WhichDoesNotExits()
        {
            $_SESSION['MySession1'] = 'session-data1';
            $sEndPoint = MockInterfaceEndPoint::Type()->getName() . '.NO_METHOD';

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig
            );

            $this->expectException(DispatchException::class);
            self::runApp($oApplication, $oConfig);
        }

        public function testDispatcher_StaticEndPoint()
        {
            $this->assertFalse(MockInterfaceEndPoint::$staticCall);
            $sEndPoint = MockInterfaceEndPoint::Type()->getName() . '.staticCall';

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue(MockInterfaceEndPoint::$staticCall);
        }

        public function testDispatcher_StaticEndPoint_WithParam()
        {
            $sEndPoint = MockInterfaceEndPoint::Type()->getName() . '.staticCall';

            $oConfig = Config::get();
            $oApplication = $this->getApp(
                $sEndPoint,
                $oConfig,
                'WorkshopTest\Resource\EndPoint',
                null,
                ['param' => 'abc']
            );

            $oResponse = self::runApp($oApplication, $oConfig);
            $this->assertTrue($oResponse->getStatus()->isOk());
            $this->assertTrue(MockInterfaceEndPoint::$staticCallParam == 'abc');
        }

        /**
         * @param $sEndPoint
         * @param $oConfig
         * @param string $sNamespace
         * @param null $sEventSpec
         * @param null $aParams
         * @return \AtomPie\System\Application
         */
        private function getApp(
            $sEndPoint,
            $oConfig,
            /** @noinspection PhpUnusedParameterInspection */
            $sNamespace = null,
            $sEventSpec = null,
            $aParams = null
        ) {
            return Boot::up(
                $oConfig,
                $sEndPoint,
                $sEventSpec,
                $aParams
            );
        }

    }

}
