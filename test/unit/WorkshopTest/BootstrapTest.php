<?php
namespace WorkshopTest;

use AtomPie\Core\Dispatch\EndPointImmutable;
use AtomPie\Core\Dispatch\EventSpecImmutable;
use AtomPie\Web\Connection\Http\Header\Status;
use WorkshopTest\Resource\Config\TestConfig;

require_once __DIR__ . '/../../../vendor/autoload.php';

class BootstrapTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * Cleans up the environment runAfter running a test.
     */
    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testBootstrap_EndPoint()
    {

        RequestFactory::produce(
            'DefaultController.index'
        );
        $oResponse = Boot::run(TestConfig::get());

        $this->assertTrue(
            strstr($oResponse->getContent()->get(),'No') !== false &&
            strstr($oResponse->getContent()->get(), 'Empty') !== false
        );
    }

    public function testBootstrap_UseCase()
    {

        RequestFactory::produce(
            'UseCase.index'
        );
        $oResponse = Boot::run(TestConfig::get());
        $this->assertTrue($oResponse->getContent()->get() == "[\"data\"]");
    }

    public function testBootstrap_Event()
    {

        $_REQUEST[EndPointImmutable::END_POINT_QUERY] ='DefaultController.index';
        $_REQUEST[EventSpecImmutable::EVENT_QUERY] = 'MockComponent0.Inner.Click';

        $oResponse = Boot::run(TestConfig::get());
        $this->assertTrue(
            strstr($oResponse->getContent()->get(),'No') !== false and
            strstr($oResponse->getContent()->get(), 'Yes') !== false);

    }

    public function testBootstrap_Exception()
    {

        RequestFactory::produce(
            'ErrorController.index'
        );
        $oResponse = Boot::run(TestConfig::get());

        $this->assertTrue($oResponse->getStatus()->is(Status::NOT_FOUND));

    }

}
