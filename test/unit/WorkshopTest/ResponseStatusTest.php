<?php
namespace unit\WorkshopTest;

use AtomPie\Core\Dispatch\EndPointImmutable;
use AtomPie\System\Context;
use AtomPie\Web\Connection\Http\Content;
use AtomPie\Web\Connection\Http\Header\ContentType;
use AtomPie\Web\Connection\Http\Header\Status;
use AtomPie\Web\Connection\Http\Request;
use WorkshopTest\Boot;
use WorkshopTest\Resource\Config\Config;

class ResponseStatusTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $_REQUEST = array();
        parent::setUp();
    }

    protected function tearDown()
    {
        $_REQUEST = array();
        parent::tearDown();
    }

    public function testMissingEndPointParamError()
    {
        $_REQUEST[EndPointImmutable::END_POINT_QUERY] = 'MockComponent7.EndPoint';
        $oResponse = Boot::run(Config::get());
        $this->assertTrue($oResponse->getStatus()->is(Status::BAD_REQUEST));

    }

    public function testIncorrectRequestContentTypeError_JSON()
    {
        $_REQUEST[EndPointImmutable::END_POINT_QUERY] = 'MockComponent8.requireJsonParam';

        $oResponse = Boot::run(Config::get(),
            function (Context $oContext) {
                /**
                 * @var $oRequest Request
                 */
                $oRequest = $oContext->getEnvironment()->getRequest();
                // Sets incompatible content to content-type, one is url-encoded string
                // and content-type is json
                $oRequest->setContent(new Content('test=1', new ContentType('application/json')));
            }
        );
        // BUT EndPoint requires that client explicitly accepts application\/json media-type.
        $this->assertTrue($oResponse->getStatus()->is(Status::UNPROCESSABLE_ENTITY));

    }

    public function testIncorrectRequestContentTypeError_XML()
    {
        $_REQUEST[EndPointImmutable::END_POINT_QUERY] = 'MockComponent8.requireXmlParam';

        // Sets compatible content and content-type, both are json
        $oResponse = Boot::run(Config::get(),
            function (Context $oContext) {
                /**
                 * @var $oRequest Request
                 */
                $oRequest = $oContext->getEnvironment()->getRequest();
                $oRequest->setContent(new Content('NOT-XML', new ContentType('application/xml')));
            }
        );
        // BUT EndPoint requires that client explicitly accepts application\/json media-type.
        $this->assertTrue($oResponse->getStatus()->is(Status::UNPROCESSABLE_ENTITY));

    }

    public function testCorrectRequestContentType_Json()
    {
        $_REQUEST[EndPointImmutable::END_POINT_QUERY] = 'MockComponent8.requireJsonParam';

        // Sets compatible content and content-type, both are json

        $oResponse = Boot::run(Config::get(),
            function (Context $oContext) {
                /**
                 * @var $oRequest Request
                 */
                $oRequest = $oContext->getEnvironment()->getRequest();
                $oRequest->setContent(new Content('{"test":1}', new ContentType('application/json')));
            });
        $this->assertTrue($oResponse->getStatus()->is(Status::OK));
    }

    public function testCorrectRequestContentType_Xml()
    {
        $_REQUEST[EndPointImmutable::END_POINT_QUERY] = 'MockComponent8.requireXmlParam';

        $oResponse = Boot::run(Config::get(),
            function (Context $oContext) {
                /**
                 * @var $oRequest Request
                 */
                $oRequest = $oContext->getEnvironment()->getRequest();
                // Sets compatible content and content-type, both are json
                $oRequest->setContent(new Content(/** @lang XML */
                    '<data><test><b>1</b><c>2</c></test><a>2</a></data>', new ContentType('application/xml')));

            }
        );
        // BUT EndPoint requires that client explicitly accepts application\/json media-type.
        $this->assertTrue($oResponse->getStatus()->is(Status::OK));
    }

}
