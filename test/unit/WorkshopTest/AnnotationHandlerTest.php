<?php
namespace WorkshopTest;

require_once __DIR__ . '/../Config.php';

use AtomPie\Core\Service\AuthorizeAnnotationService;
use AtomPie\AnnotationTag\Authorize;
use Generi\Exception;

/**
 * @Authorize(ResourceIndex="WWWAnnotated",AuthType="Basic",AuthToken="class:class")
 */
class WWWAnnotated
{
    /**
     * @Authorize(ResourceIndex="WWWAnnotated.annotated",AuthType="Basic",AuthToken="method:method")
     */
    public function annotated()
    {

    }

    /**
     * @Authorize(ResourceIndex="WWWAnnotated.PrivateAccess",AuthType="Static",AuthToken="private")
     */
    public function privateAccess()
    {

    }

    /**
     * @Authorize(ResourceIndex="WWWAnnotated.PublicAccess",AuthType="Static",AuthToken="public")
     */
    public function publicAccess()
    {

    }

    /**
     * @Authorize(ResourceIndex="WWWAnnotated.AccessTrue",AuthType="Method",AuthToken="this.LogicTrue")
     */
    public function accessTrue()
    {

    }

    /**
     * @Authorize(ResourceIndex="WWWAnnotated.AccessFalse",AuthType="Method",AuthToken="this.LogicFalse")
     */
    public function accessFalse()
    {

    }

    /**
     * @Authorize(ResourceIndex="WWWAnnotated.AccessNull",AuthType="Method",AuthToken="this.LogicNull")
     */
    public function accessNull()
    {

    }

    /**
     * @Authorize(ResourceIndex="WWWAnnotated.AccessStatic",AuthType="Method",AuthToken="\WorkshopTest\WWWAnnotated.LogicStaticTrue")
     */
    public function accessStatic()
    {

    }

    public function webMethodNotAnnotated()
    {

    }


    public static function LogicStaticTrue()
    {
        return true;
    }

    public function LogicTrue()
    {
        return true;
    }

    public function LogicFalse()
    {
        return false;
    }

    public function LogicNull()
    {
        return null;
    }
}

class AnnotationHandlerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {

        $oConfig = Resource\Config\Config::get();
        Boot::up($oConfig);
        parent::setUp();
        unset($_SERVER['PHP_AUTH_USER']);
        unset($_SERVER['PHP_AUTH_PW']);
    }

    /**
     * Cleans up the environment runAfter running a test.
     */
    protected function tearDown()
    {
        parent::tearDown();
        unset($_SERVER['PHP_AUTH_USER']);
        unset($_SERVER['PHP_AUTH_PW']);
    }

    public function testAnnotationHandler_PhpDocFromClass()
    {

        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(new WWWAnnotated());
        $this->assertFalse($bResult);
        $_SERVER['PHP_AUTH_USER'] = 'class';
        $_SERVER['PHP_AUTH_PW'] = 'class';
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(new WWWAnnotated());
        $this->assertTrue($bResult);
        $_SERVER['PHP_AUTH_USER'] = 'method';
        $_SERVER['PHP_AUTH_PW'] = 'method';
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(new WWWAnnotated());
        $this->assertFalse($bResult);
        $_SERVER['PHP_AUTH_USER'] = 'method';
        $_SERVER['PHP_AUTH_PW'] = 'method';
    }

    public function testAnnotationHandler_PhpDocFromMethod()
    {
        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'privateAccess');
        $this->assertFalse($bResult);
        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'publicAccess');
        $this->assertTrue($bResult);
        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'accessTrue');
        $this->assertTrue($bResult);
        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'accessFalse');
        $this->assertFalse($bResult);
    }

    public function testAnnotationHandler_PhpDocFromMethod_WithStrategy()
    {

        $aStrategy = array(
            'basic' => function () {
                return 'basic';
            },
            'method' => function () {
                return 'method';
            },
            'static' => function () {
                return 'static';
            }
        );

        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'privateAccess', $aStrategy);
        $this->assertTrue($bResult == 'static');
        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'accessFalse', $aStrategy);
        $this->assertTrue($bResult == 'method');
    }

    public function testAnnotationHandler_PhpDocFromMethod_WithStrategy_WithException()
    {

        $aStrategy = array(
            'basic' => function () {
                return 'basic';
            },
            'method' => function () {
                return 'method';
            },
            'static' => function () {
                return 'static';
            }
        );

        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'publicAccess', $aStrategy);
        $this->assertTrue($bResult);
    }

    public function testAnnotationHandler_PhpDocFromMethod_Exception_NotBool()
    {
        $this->expectException(Exception::class);
        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'accessNull');
        $this->assertFalse($bResult);
    }

    public function testAnnotationHandler_PhpDocFromMethod_Exception_NoMethod()
    {
        $this->expectException(\ReflectionException::class);
        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'accessNONE');
        $this->assertFalse($bResult);
    }

    public function testAnnotationHandler_PhpDocFromMethod_Static_Method()
    {
        $oHandler = new AuthorizeAnnotationService();
        /** @noinspection PhpUnusedLocalVariableInspection */
        list($bResult, $sMessage) = $oHandler->invokeAuthorizeAnnotation(
            new WWWAnnotated(), 'accessStatic');
        $this->assertTrue($bResult);
    }

}
