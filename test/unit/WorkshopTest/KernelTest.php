<?php
namespace WorkshopTest;

require_once __DIR__ . '/../Config.php';

use AtomPie\Boundary\Core\IAmFrameworkConfig;
use AtomPie\Core\Dispatch\QueryString;
use AtomPie\Core\Dispatch\DispatchManifest;
use AtomPie\Core\Dispatch\EndPointImmutable;
use AtomPie\Core\Dispatch\EventSpecImmutable;
use WorkshopTest\Resource\Config\Config;

class KernelTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        $_REQUEST = array();
        $oConfig = Config::get();
        Boot::up($oConfig);
        parent::setUp();
    }

    /**
     * Cleans up the environment runAfter running a test.
     */
    protected function tearDown()
    {
        $_REQUEST = array();
        parent::tearDown();
    }

    public function testKernel_DispatchManifestFactory()
    {

        RequestFactory::produce(
            QueryString::urlEscape('DefaultController.Method'),
            QueryString::urlEscape('WorkshopTest\\Resource\\Component\\MockComponent0.Name.event')
        );

        $oConfig = Config::get();

        $oDispatchManifest = $this->getDispatchManifest($oConfig);
        $this->assertTrue($oDispatchManifest->getEndPoint() instanceof EndPointImmutable);
        $this->assertTrue($oDispatchManifest->getEventSpec() instanceof EventSpecImmutable);

    }

    public function testKernel_DispatchManifestFactory_OnlyEndPoint()
    {

        $_REQUEST[EndPointImmutable::END_POINT_QUERY]
            = QueryString::urlEscape('DefaultController.Method');

        $oConfig = Config::get();

        $oDispatchManifest = $this->getDispatchManifest($oConfig);

        $this->assertTrue($oDispatchManifest->getEndPoint() instanceof EndPointImmutable);
        $this->assertTrue($oDispatchManifest->getEventSpec() === null);

    }

    public function testKernel_DispatchManifestFactory_OnlyEndPointAndComponent()
    {

        $_REQUEST[EndPointImmutable::END_POINT_QUERY]
            = QueryString::urlEscape('DefaultController.Method');

        $oConfig = Config::get();

        $oDispatchManifest = $this->getDispatchManifest($oConfig);

        $this->assertTrue($oDispatchManifest->getEndPoint() instanceof EndPointImmutable);
        $this->assertTrue($oDispatchManifest->getEventSpec() === null);

    }

    public function testKernel_DispatchManifestFactory_DefaultValues()
    {

        $oConfig = Config::get();

        $oDispatchManifest = $this->getDispatchManifest($oConfig);

        $this->assertTrue($oDispatchManifest->getEndPoint() instanceof EndPointImmutable);
        $this->assertTrue($oDispatchManifest->getEndPoint()->__toString() == 'Default.index');
        $this->assertTrue($oDispatchManifest->getEventSpec() === null);
        $this->assertFalse($oDispatchManifest->hasEventSpec());

    }

    /**
     * @param IAmFrameworkConfig $oConfig
     * @return DispatchManifest
     */
    private function getDispatchManifest($oConfig)
    {
        $oDispatchManifest = DispatchManifest::factory($oConfig, $_REQUEST);
        return $oDispatchManifest;
    }
}
