<?php
namespace WorkshopTest {

    use AtomPie\Gui\Component\ComponentDependencyContainer;
    use AtomPie\System\Context;
    use AtomPie\System\UrlProvider;
    use AtomPie\System\Init;
    use AtomPie\Boundary\Core\IAmFrameworkConfig;
    use AtomPie\System\DependencyContainer\EndPointDependencyContainer;
    use AtomPie\Core\FrameworkConfig;
    use AtomPie\Core\Dispatch\DispatchManifest;
    use AtomPie\Web\Session\ParamStatePersister;
    use AtomPie\System\Kernel;
    use AtomPie\Web;

    class Boot
    {
        
        /**
         * @param \Closure $closure
         * @return EndPointDependencyContainer
         */
        public static function getEndPointDi(\Closure $closure = null)
        {
            $oConfig = self::getFrameworkConfig();
            /**
             * @var $oContext Context
             */
            list($oDispatchManifest, $oContext) = self::getContext($oConfig);

            if(null !== $closure) {
                $closure($oContext);
            }

            $oThisUrlProvider = new UrlProvider(
                $oDispatchManifest,
                $oContext->getEnvironment()->getRequest()->getAllParams()
            );

            /** @noinspection PhpInternalEntityUsedInspection */
            return new EndPointDependencyContainer($oContext->getEnvironment(), $oConfig, $oDispatchManifest, $oThisUrlProvider);
        }

        public static function upRequest(IAmFrameworkConfig $oConfig, \Closure $closure = null)
        {

            /**
             * @var $oDispatchManifest DispatchManifest
             * @var $oContext Context
             */
            list($oDispatchManifest, $oContext) = self::getContext($oConfig);

            if(null !== $closure) {
                $closure($oContext);
            }
            
            $oBoot = new Init();

            $oApplication = $oBoot->initApplication(
                $oContext,
                $oConfig,
                $oDispatchManifest,
                $oConfig->getContentProcessors()
            );

            return $oApplication;

        }

        public static function up(
            IAmFrameworkConfig $oConfig,
            $sEndPoint = 'Default.index',
            $sEventSpecString = null,
            array $aParams = null
        ) {

            RequestFactory::produce(
                $sEndPoint,
                $sEventSpecString,
                $aParams
            );

            return self::upRequest($oConfig);
        }

        /**
         * @param IAmFrameworkConfig $oConfig
         * @param \Closure $closure
         * @return Web\Boundary\IChangeResponse
         */
        public static function run(
            IAmFrameworkConfig $oConfig,
            \Closure $closure = null
        ) {
            if(null !== $closure) {
                $oDispatchManifest = DispatchManifest::factory($oConfig, $_REQUEST);
                $oContext = new Context($oDispatchManifest->getEndPoint(), $oConfig);
                $closure($oContext);
                $oKernel = new Kernel();
                return $oKernel->boot($oConfig, $oContext);
            } else {
                $oDispatchManifest = DispatchManifest::factory($oConfig, $_REQUEST);
                $oContext = new Context($oDispatchManifest->getEndPoint(), $oConfig);
                $oKernel = new Kernel();
                return $oKernel->boot($oConfig, $oContext);
            }
        }

        /**
         * @param null $oDispatchManifest
         * @param \Closure $closure
         * @return ComponentDependencyContainer
         */
        public static function getComponentDi($oDispatchManifest = null, \Closure $closure = null)
        {

            $oConfig = self::getFrameworkConfig();
            if ($oDispatchManifest === null) {
                $oDispatchManifest = self::getDispatchManifest($oConfig);
            }
            
            $oContext = new Context($oDispatchManifest->getEndPoint(), $oConfig);

            if(null !== $closure) {
                $closure($oContext);
            }
            
            $oStatePersister = new ParamStatePersister(
                $oContext->getEnvironment()->getSession(),
                $oDispatchManifest->getEndPoint()->__toString()
            );

            return new ComponentDependencyContainer(
                $oContext->getEnvironment(),
                $oDispatchManifest,
                $oStatePersister
            );
        }

        /**
         * @return FrameworkConfig
         */
        private static function getFrameworkConfig()
        {
            return new FrameworkConfig(
                __DIR__.'/Resource/Context'
                , 'Main'
            );
        }

        /**
         * @param IAmFrameworkConfig $oConfig
         * @return DispatchManifest
         * @internal param Environment $oEnvironment
         */
        private static function getDispatchManifest($oConfig)
        {
            return DispatchManifest::factory($oConfig, $_REQUEST);
        }

        /**
         * @param IAmFrameworkConfig $oConfig
         * @return array | [DispatchManifest,Context]
         */
        private static function getContext(IAmFrameworkConfig $oConfig)
        {
            $oDispatchManifest = self::getDispatchManifest($oConfig);
            $oContext = new Context($oDispatchManifest->getEndPoint(), $oConfig);
            return array($oDispatchManifest, $oContext);
        }
    }

}
