<?php
namespace WorkshopTest;

use AtomPie\DependencyInjection\DependencyInjector;
use AtomPie\Gui\Component\Part;
use AtomPie\Gui\Component\RecursiveInvoker\FactoryTreeInvoker;
use AtomPie\System\Context;
use AtomPie\Web\Connection\Http\Request;
use AtomPie\Web\Connection\Http\Url\Param;
use AtomPie\Web\Connection\Http\Content;
use AtomPie\Web\Connection\Http\Header\ContentType;
use WorkshopTest\Resource\Component\MockComponent1;
use WorkshopTest\Resource\Component\MockComponent2;
use WorkshopTest\Resource\Config\Config;
use WorkshopTest\Resource\Param\MyParam1;
use WorkshopTest\Resource\Param\MyParam2;

require_once __DIR__ . '/../Config.php';
require_once __DIR__ . '/Resource/Component/MockComponent1.php';
require_once __DIR__ . '/Resource/Component/MockComponent2.php';

class ComponentMethodParamTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $oConfig = Config::get();
        Boot::up($oConfig);
        parent::setUp();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testParam_NotInRequest()
    {
        $this->expectException(Param\ParamException::class);
        $oComponent = new MockComponent1(Part::TOP_COMPONENT_NAME);
        $oInvoker = new FactoryTreeInvoker();
        $oInvoker->invokeFactoryForComponent(
            Boot::getComponentDi(),
            $oComponent
        );
    }

    public function testParam_NotInRequest_ButOptional()
    {
        $oComponent = new MockComponent2(Part::TOP_COMPONENT_NAME);
        $oInvoker = new FactoryTreeInvoker();
        $oInvoker->invokeFactoryForComponent(
            Boot::getComponentDi(),
            $oComponent
        );
    }

    public function testParam_InRequest()
    {
        $_REQUEST = [];
        $_REQUEST['MyParam1'] = 1;

        $oComponent = new MockComponent1(Part::TOP_COMPONENT_NAME);
        $oInvoker = new FactoryTreeInvoker();
        $oInvoker->invokeFactoryForComponent(
            Boot::getComponentDi(),
            $oComponent
        );

        $this->assertTrue($oComponent->oMyParam1->getValue() == '1');
        $this->assertTrue($oComponent->oMyParam1->getName() == MyParam1::Type()->getName());
        $this->assertTrue($oComponent->oMyParam2->isNull());
    }

    public function testArrayParam_InRequest()
    {

        $_REQUEST = [];
        $_REQUEST['MyParam1'] = array('a' => 1, 'b' => 2);
        $_REQUEST['MyParam2'] = 1;

        $oComponent = new MockComponent1(Part::TOP_COMPONENT_NAME);
        $oInvoker = new FactoryTreeInvoker();
        $oInvoker->invokeFactoryForComponent(Boot::getComponentDi(), $oComponent);

        $this->assertTrue($oComponent->oMyParam1->isArray());
        $aArray = $oComponent->oMyParam1->getValue();
        $this->assertTrue($aArray['a'] == '1');
        $this->assertTrue($oComponent->oMyParam2->getValue() == 1);

    }

    public function testNotAllowedParam_EmptyRequest()
    {

        $_REQUEST = [];
        $_REQUEST['MyParam1'] = array('a' => 1, 'b' => 2);
        $_REQUEST['MyParam2'] = 1;

        $oComponent = new MockComponent1(Part::TOP_COMPONENT_NAME);
        $oInvoker = new FactoryTreeInvoker();
        $oInvoker->invokeFactoryForComponent(
            Boot::getComponentDi(),
            $oComponent
        );

        $this->expectException(Param\ParamException::class);
        $oDependencyInjector = new DependencyInjector(Boot::getEndPointDi());
        $oDependencyInjector->invokeMethod($oComponent, 'test1');

    }

    public function testJsonParam()
    {
        $oConfig = Config::get();
        Boot::upRequest($oConfig);

        $oComponent = new MockComponent1(Part::TOP_COMPONENT_NAME);
        $oInvoker = new FactoryTreeInvoker();
        $oInvoker->invokeFactoryForComponent(
            Boot::getComponentDi(null,
                function(Context $oContext) {
                    /**
                     * @var $oRequest Request
                     */
                    $oRequest = $oContext->getEnvironment()->getRequest();
                    $oRequest->removeParam(MyParam1::Type()->getName());
                    $oRequest->removeParam(MyParam2::Type()->getName());
                    $oRequest->setContent(
                        new Content(
                            json_encode(
                                array(
                                    MyParam1::Type()->getName() => 1,
                                )
                            ),
                            new ContentType(ContentType::JSON)
                        )
                    );
                    $oRequest->load();
                }),
            $oComponent
        );

        $this->assertTrue($oComponent->oMyParam1->getValue() == 1);
        $this->assertTrue($oComponent->oMyParam2->isNull());
    }

    public function testJsonParam_ArrayParam()
    {
        $oComponent = new MockComponent1(Part::TOP_COMPONENT_NAME);
        $oInvoker = new FactoryTreeInvoker();
        $oInvoker->invokeFactoryForComponent(
            Boot::getComponentDi(null,
                function(Context $oContext) {
                    /**
                     * @var $oRequest Request
                     */
                    $oRequest = $oContext->getEnvironment()->getRequest();
                    $oRequest->removeParam('MyParam1');
                    $oRequest->removeParam('MyParam2');
                    $oRequest->setContent(
                        new Content(
                            json_encode(
                                array(
                                    'MyParam1' => array('a' => 1, 'b' => '2'),
                                    'MyParam2' => 2,
                                )
                            ),
                            new ContentType(ContentType::JSON)
                        )
                    );
                }),
            $oComponent
        );

        $aArray = $oComponent->oMyParam1->getValue();

        $this->assertTrue($aArray['a'] == 1);
        $this->assertTrue($aArray['b'] == 2);
        $this->assertTrue($oComponent->oMyParam2->getValue() == 2);
    }

}
