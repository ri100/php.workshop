<?php
namespace AtomPie\Core\Dispatch {

    use Generi\Boundary\ICanBeIdentified;
    use AtomPie\Boundary\Core\Dispatch\IAmEndPointValue;
    use AtomPie\Boundary\Core\Dispatch\IAmEventSpecImmutable;
    use AtomPie\Boundary\Core\Dispatch\IAmDispatchManifest;
    use AtomPie\Boundary\Core\IAmFrameworkConfig;

    class DispatchManifest implements IAmDispatchManifest
    {

        /**
         * @var EndPointImmutable
         */
        private $oEndPointQuery;

        /**
         * @var IAmEventSpecImmutable
         */
        private $oEventSpec = null;

        /**
         * @var IAmFrameworkConfig
         */
        private $oConfig;

        /**
         * DispatchManifest constructor.
         * @param IAmFrameworkConfig $oConfig
         * @param IAmEndPointValue $oEndPointSpec
         * @param IAmEventSpecImmutable|null $oEventSpec
         * @internal 
         */
        public function __construct(
            IAmFrameworkConfig $oConfig,
            IAmEndPointValue $oEndPointSpec,
            IAmEventSpecImmutable $oEventSpec = null
        ) {

            $this->oConfig = $oConfig;
            $this->oEndPointQuery = $oEndPointSpec;

            if ($oEventSpec !== null) {
                $this->oEventSpec = $oEventSpec;
            }

        }

        /**
         * @return \AtomPie\Core\Dispatch\EndPointImmutable
         */
        public function getEndPoint()
        {
            return $this->oEndPointQuery;
        }

        /**
         * @return bool
         */
        public function hasEventSpec()
        {
            return $this->oEventSpec !== null;
        }

        public function __toString()
        {
            $sComponentSpecString = '';
            $sEventSpec = '';
            if ($this->hasEventSpec()) {
                $sEventSpec = $this->getEventSpec()->__toUrlString();
            }

            return $this->getEndPoint()->cloneEndPointUrl()->getUrl() . $sComponentSpecString . $sEventSpec;
        }

        /**
         * @return IAmEventSpecImmutable
         */
        public function getEventSpec()
        {
            return $this->oEventSpec;
        }

        ////////////////////////////////

        /**
         * @param \AtomPie\Boundary\Core\IAmFrameworkConfig $oConfig
         * @param $aRawRequest
         * @return DispatchManifest
         */
        public static function factory(
            IAmFrameworkConfig $oConfig,
            $aRawRequest
        ) {

            $oEndPointSpec = EndPointImmutable::factoryEndPointSpec(
                $aRawRequest,
                $oConfig->getDefaultEndPoint());

            $oEventSpec = EventSpecImmutable::factoryEventSpec(
                $aRawRequest
                , $oConfig->getEventNamespaces()
                , $oConfig->getEventClasses());

            $oDispatchManifest = new DispatchManifest(
                $oConfig,
                $oEndPointSpec,
                $oEventSpec
            );

            return $oDispatchManifest;

        }

        /**
         * @param $sComponentType
         * @param $sComponentName
         * @param $sEvent
         * @return \AtomPie\Core\\AtomPie\Core\Dispatch\EventSpecImmutable
         */
        public function newEventSpec(
            $sComponentType,
            $sComponentName,
            $sEvent
        ) {

            return EventSpecImmutable::factory($sComponentType,
                $sComponentName,
                $sEvent,
                $this->oConfig->getEventNamespaces(),
                $this->oConfig->getEventClasses()
            );
        }

        /**
         * Returns immutable clone of DispatchManifest with new event.
         *
         * @param ICanBeIdentified $oComponent
         * @param $sEvent
         * @return DispatchManifest
         */
        public function cloneWithEvent(ICanBeIdentified $oComponent, $sEvent)
        {
            return new DispatchManifest(
                $this->oConfig,
                $this->getEndPoint(),
                EventSpecImmutable::factory(
                    $oComponent->getType()->getFullName(),
                    $oComponent->getName(),
                    $sEvent,
                    $this->oConfig->getEventNamespaces(),
                    $this->oConfig->getEventClasses()
                )
            );
        }

    }

}