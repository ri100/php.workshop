<?php
namespace AtomPie\Core\Dispatch {

    use AtomPie\Boundary\Core\Dispatch\IAmEndPointValue;
    use AtomPie\I18n;
    use AtomPie\Web\Connection\Http\ImmutableUrl;

    class EndPointImmutable extends QueryString implements IAmEndPointValue
    {

        const DEFAULT_METHOD = '__default';
        const DEFAULT_SEPARATOR = '.';
        const END_POINT_QUERY = '__end_point';

        private $sEndPointClass;
        private $sEndPointMethod;

        public function __construct($sEndPointUrl)
        {

            if (false !== strstr($sEndPointUrl, self::DEFAULT_SEPARATOR)) {
                list($sClass, $sMethod) = explode(self::DEFAULT_SEPARATOR, $sEndPointUrl);
                if (!is_null($sMethod)) {
                    $this->validateMethod($sMethod);
                } else {
                    throw new EndPointException('Invalid EndPoint query structure.');
                }
            } else {
                if (strlen($sEndPointUrl) > 0) {
                    $sClass = $sEndPointUrl;
                    $sMethod = self::DEFAULT_METHOD; // This is default EndPoint method
                } else {
                    throw new EndPointException('Invalid EndPoint query structure.');
                }
            }

            $this->sEndPointMethod = $sMethod;
            $this->sEndPointClass = self::escape($sClass);
            $this->validateClassType($this->sEndPointClass);

        }

        /**
         * @param $aRawRequest
         * @param string $sDefaultQueryString
         * @return EndPointImmutable
         */
        public static function factoryEndPointSpec($aRawRequest, $sDefaultQueryString)
        {
            if (isset($aRawRequest[self::END_POINT_QUERY])) {
                $sEndPoint = $aRawRequest[self::END_POINT_QUERY];
                $oEndPointQuery = new EndPointImmutable($sEndPoint);
            } else {
                $oEndPointQuery = new EndPointImmutable($sDefaultQueryString);
            }
            return $oEndPointQuery;
        }

        /**
         * @return string
         */
        public function getEndPointString()
        {
            return $this->sEndPointClass;
        }

        /**
         * @return string
         */
        public function getMethodString()
        {
            return $this->sEndPointMethod;
        }

        /**
         * @return bool
         */
        public function isDefaultMethod()
        {
            return $this->sEndPointMethod == self::DEFAULT_METHOD;
        }

        /**
         * @return bool
         */
        public function hasMethod()
        {
            return !empty($this->sEndPointMethod);
        }

        /**
         * @return string
         */
        public function __toString()
        {
            return $this->getEndPointString() . self::DEFAULT_SEPARATOR . $this->getMethodString();
        }

        /**
         * @return string
         */
        public function __toUrlString()
        {
            return self::urlEscape($this->getEndPointString()) . self::DEFAULT_SEPARATOR . $this->getMethodString();
        }

        /**
         * Returns instance of EndPointUrl filled with EndPoint string.
         *
         * @return ImmutableUrl
         */
        public function cloneEndPointUrl()
        {
            return new ImmutableUrl($this->__toUrlString());
        }

        private function validateClassType($sClassType)
        {

            // Class

            if (!is_string($sClassType)) {
                throw new EndPointException("Dispatch command must be string.");
            }
            if (!preg_match('/^[a-zA-Z0-9|_|\\\]+$/', $sClassType) > 0) {
                throw new EndPointException(
                    I18n\Label(
                        'Class type: [%s] contains not allowed chars! Only a-Z, digits, [_\\] allowed.',$sClassType
                    )
                );
            }

        }

        private function validateMethod($sMethod)
        {

            // Event/Action

            if (!is_string($sMethod)) {
                throw new EndPointException('Dispatch action/event must be string.');
            }

            if (!preg_match('/^[a-zA-Z0-9|_|\]\[]+$/', $sMethod) > 0) {
                throw new EndPointException(
                    I18n\Label(
                        'Method: [%s] contains not allowed chars! Only a-Z, digits, [_] allowed.',
                        $sMethod)
                );
            }

        }

    }

}