<?php
namespace AtomPie\Core {

    use AtomPie\Boundary\Core\IAmFrameworkConfig;
    use AtomPie\Boundary\Core\ISetUpContentProcessor;
    use AtomPie\Boundary\System\IAmRouter;
    use AtomPie\Boundary\System\IHandleException;
    use AtomPie\Boundary\System\IRunAfterMiddleware;
    use AtomPie\Boundary\System\IRunBeforeMiddleware;
    use AtomPie\DependencyInjection\ContractFillers;
    use AtomPie\System\ClassShortcuts;

    final class FrameworkConfig implements IAmFrameworkConfig
    {

        /**
         * @var string
         */
        private $sDefaultEndPoint;

        /**
         * @var IAmRouter
         */
        private $oRouter;

        /**
         * @var \AtomPie\DependencyInjection\ContractFillers
         */
        private $oContractFillers;
        
        /**
         * @var IRunBeforeMiddleware[] | IRunAfterMiddleware[]
         */
        private $aMiddleware;
        
        /**
         * @var ClassShortcuts
         */
        private $oEventClassShortCuts;
        /**
         * @var ISetUpContentProcessor[]
         */
        private $aContentProcessors;
        
        /**
         * @var IHandleException
         */
        private $oErrorRenderer;

        /**
         * @var bool
         */
        private $isLocked = false;

        /**
         * @var string
         */
        private $sSrcDir;

        public function __construct(
            $sEndPointDir,
            $sDefaultEndPoint
        ) {

            $this->sDefaultEndPoint = $sDefaultEndPoint;

            $this->oContractFillers = new ContractFillers([]);
            $this->oRouter = null;
            $this->aMiddleware = [];
            $this->oEventClassShortCuts = new ClassShortcuts();
            $this->aContentProcessors = [];
            $this->oErrorRenderer = null;
            $this->sSrcDir = $sEndPointDir;
        }

        /**
         * Locks config for changes.
         */
        public function lock() {
            $this->isLocked = true;
        }

        /**
         * @return array
         */
        public function getEventNamespaces()
        {
            return $this->oEventClassShortCuts->getNamespaces()->__toArray();
        }

        /**
         * @return array
         */
        public function getEventClasses()
        {
            return $this->oEventClassShortCuts->getClasses()->__toArray();
        }

        /**
         * @return string
         */
        public function getDefaultEndPoint()
        {
            return $this->sDefaultEndPoint;
        }

        /**
         * @return IAmRouter
         */
        public function getRouter()
        {
            return $this->oRouter;
        }

        /**
         * @return \AtomPie\DependencyInjection\ContractFillers
         */
        public function getContractsFillers()
        {
            return $this->oContractFillers;
        }

        /**
         * @return IRunAfterMiddleware[]|IRunBeforeMiddleware[]
         */
        public function getMiddleware()
        {
            return $this->aMiddleware;
        }

        /**
         * @return ISetUpContentProcessor[]
         */
        public function getContentProcessors()
        {
            return $this->aContentProcessors;
        }

        /**
         * @return IHandleException
         */
        public function getErrorHandler()
        {
            return $this->oErrorRenderer;
        }

        /**
         * @param array $aContractFillers
         * @throws Exception
         */
        public function setContractFillers(array $aContractFillers)
        {
            if($this->isLocked) {
                throw new Exception('Framework config is locked. Do not change its state.');
            }
            $this->oContractFillers = new ContractFillers($aContractFillers);
        }

        /**
         * @param IRunAfterMiddleware[]|IRunBeforeMiddleware[] $aMiddleware
         * @throws Exception
         */
        public function setMiddleware(array $aMiddleware)
        {
            if($this->isLocked) {
                throw new Exception('Framework config is locked. Do not change its state.');
            }
            $this->aMiddleware = $aMiddleware;
        }

        /**
         * @param IAmRouter $oRouter
         * @throws Exception
         */
        public function setRouter(IAmRouter $oRouter)
        {
            if($this->isLocked) {
                throw new Exception('Framework config is locked. Do not change its state.');
            }
            $this->oRouter = $oRouter;
        }

        /**
         * @param IHandleException $oErrorRenderer
         * @throws Exception
         */
        public function setErrorRenderer(IHandleException $oErrorRenderer)
        {
            if($this->isLocked) {
                throw new Exception('Framework config is locked. Do not change its state.');
            }
            $this->oErrorRenderer = $oErrorRenderer;
        }

        /**
         * @param ClassShortcuts $oEventConfig
         * @throws Exception
         */
        public function setEventClassShortcuts(ClassShortcuts $oEventConfig)
        {
            if($this->isLocked) {
                throw new Exception('Framework config is locked. Do not change its state.');
            }
            $this->oEventClassShortCuts = $oEventConfig;
        }

        /**
         * @param ISetUpContentProcessor[] $aContentProcessors
         * @throws Exception
         */
        public function setContentProcessors(array $aContentProcessors)
        {
            if($this->isLocked) {
                throw new Exception('Framework config is locked. Do not change its state.');
            }
            $this->aContentProcessors = $aContentProcessors;
        }

        /**
         * @return string
         */
        public function getEndPointDir() {
            return $this->sSrcDir;
        }
    }

}
