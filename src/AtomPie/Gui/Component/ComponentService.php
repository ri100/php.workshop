<?php
namespace AtomPie\Gui\Component {

    use AtomPie\Boundary\Core\Dispatch\IAmEventSpecImmutable;
    use AtomPie\DependencyInjection\Boundary\IConstructInjection;
    use AtomPie\Boundary\Gui\Component\IAmComponent;
    use AtomPie\Boundary\Gui\Component\IBasicAuthorize;
    use AtomPie\Core\Service\AuthorizeAnnotationService;
    use AtomPie\Gui\Component\RecursiveInvoker\EventTreeInvoker;
    use AtomPie\Gui\Component\RecursiveInvoker\BeforeEventInvokeParams;

    class ComponentService
    {

        public static function buildPageTree(
            IAmComponent $oComponent,
            IConstructInjection $oComponentDependencyContainer,
            IAmEventSpecImmutable $oEventSpec = null
        ) {

            $oAnnotationHandler = new AuthorizeAnnotationService();

            $oComponentService = new ComponentService();

            $oTopComponent = $oComponentService->buildTreeOfComponents(
                $oComponentDependencyContainer,
                $oComponent,
                $oAnnotationHandler,
                $oEventSpec
            );

            return $oTopComponent;
        }

        public static function buildComponentTree(
            IConstructInjection $oComponentDependencyContainer,
            IAmComponent $oComponent,
            IAmEventSpecImmutable $oEventSpec = null
        ) {

            $oAnnotationHandler = new AuthorizeAnnotationService();

            $oComponentService = new ComponentService();

            $oTopComponent = $oComponentService->buildTreeOfComponents(
                $oComponentDependencyContainer,
                $oComponent,
                $oAnnotationHandler,
                $oEventSpec
            );

            return $oTopComponent;
        }

        /**
         * @param IConstructInjection $oComponentDependencyContainer
         * @param IAmComponent $oComponent
         * @param IBasicAuthorize $oAnnotationHandler
         * @param IAmEventSpecImmutable $oEventSpec
         * @return mixed
         */
        private function buildTreeOfComponents(
            IConstructInjection $oComponentDependencyContainer,
            IAmComponent $oComponent,
            IBasicAuthorize $oAnnotationHandler,
            IAmEventSpecImmutable $oEventSpec = null
        ) {

            $oEventInvoker = new EventTreeInvoker();

            /** @noinspection PhpUnusedParameterInspection */
            $oEventInvoker->handleEvent(EventTreeInvoker::EVENT_BEFORE_INVOKE,
                function ($oSender, BeforeEventInvokeParams $oParams) use ($oAnnotationHandler) {
                    $oAnnotationHandler->checkAuthorizeAnnotation(
                        $oParams->getComponent(),
                        $oParams->getEvent());
                }
            );

            //////////////////////////////////////
            // Invoke __process tree

            $oFactoryInvoker = new RecursiveInvoker\FactoryTreeInvoker();
            $oProcessInvoker = new RecursiveInvoker\ProcessTreeInvoker();

            ///////////////////////////////////////////
            // Iterates processing.
            // Each invocation of __process may change
            // the component tree so the whole process
            // of tree building must be repeated.
            $this->iterateTreeProcessing(
                $oComponentDependencyContainer,
                $oComponent,
                $oProcessInvoker,
                $oFactoryInvoker,
                $oEventInvoker,
                $oEventSpec
            );

            return $oComponent;

        }

        /**
         * @param IConstructInjection $oComponentDependencyContainer
         * @param IAmComponent $oComponent
         * @param \AtomPie\Gui\Component\RecursiveInvoker\ProcessTreeInvoker $oProcessInvoker
         * @param \AtomPie\Gui\Component\RecursiveInvoker\FactoryTreeInvoker $oFactoryInvoker
         * @param \AtomPie\Gui\Component\RecursiveInvoker\EventTreeInvoker $oEventInvoker
         * @param IAmEventSpecImmutable $oEventSpec
         */
        private function iterateTreeProcessing(
            $oComponentDependencyContainer,
            $oComponent,
            $oProcessInvoker,
            $oFactoryInvoker,
            $oEventInvoker,
            $oEventSpec = null
        ) {

            ///////////////////////
            // Factory
            $oFactoryInvoker->invokeFactoryTree($oComponentDependencyContainer, $oComponent);

            ///////////////////////
            // Events

            $oEventInvoker->invokeEventTree(
                $oComponentDependencyContainer,
                $oComponent,
                $oEventSpec  // Event tree
            );

            //////////////////////////////////////
            // Mark tree of components clean

            $oComponent->markTreeClean();

            ///////////////////////////////////////
            // Invoke __process tree

            $oProcessInvoker->invokeProcessTree($oComponentDependencyContainer, $oComponent);

            ///////////////////////////////////////
            // Iterate if not finished

            if ($oComponent->isTreeDirty()) {
                $this->iterateTreeProcessing(
                    $oComponentDependencyContainer,
                    $oComponent,
                    $oProcessInvoker,
                    $oFactoryInvoker,
                    $oEventInvoker,
                    $oEventSpec
                );
            };

        }
    }

}
