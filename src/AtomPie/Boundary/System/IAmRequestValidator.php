<?php
namespace AtomPie\Boundary\System;

use AtomPie\I18n;
use AtomPie\System\Dispatch\DispatchException;

interface IAmRequestValidator
{
    /**
     * @param $sContentType
     * @return $this
     * @throws DispatchException
     * @throws I18n\Exception
     */
    public function isContentType($sContentType);

    /**
     * @return $this
     */
    public function isAjax();

    /**
     * @param $sMethod
     * @return $this
     * @throws DispatchException
     * @throws I18n\Exception
     */
    public function isMethod($sMethod);

    /**
     * @param $sAccept
     * @return $this
     * @throws DispatchException
     * @throws I18n\Exception
     */
    public function accepts($sAccept);
}