<?php
namespace AtomPie\Boundary\System;

interface IAmEndPointClass
{
    /**
     * @return string
     */
    public function getClassName();
}