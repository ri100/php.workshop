<?php
namespace AtomPie\Boundary\System;

interface IRoute
{
    /**
     * @param $aServerVariable $_SERVER
     * @return $_REQUEST
     */
    public function rewrite($aServerVariable);
}