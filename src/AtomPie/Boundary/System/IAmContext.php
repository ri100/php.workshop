<?php
namespace AtomPie\Boundary\System {

    use AtomPie\DependencyInjection\Boundary\IFillContracts;
    use AtomPie\Web\Boundary\IAmEnvironment;

    interface IAmContext
    {
        /**
         * @return IFillContracts
         */
        public function getContractFillers();

        /**
         * @return IAmEndPointClass
         */
        public function getEndPointClass();


        /**
         * @return IAmEnvironment
         */
        public function getEnvironment();

        /**
         * @return \Closure
         */
        public function getOnReturn();
    }

}
