<?php
namespace AtomPie\Boundary\System;

use AtomPie\Boundary\Core\Dispatch\IAmDispatchManifest;
use AtomPie\Boundary\Core\Dispatch\IAmEndPointValue;
use AtomPie\Boundary\Core\IProcessContent;
use AtomPie\DependencyInjection\Boundary\IConstructInjection;
use AtomPie\DependencyInjection\Boundary\IFillContracts;
use AtomPie\Web\Boundary\IChangeRequest;

interface IAmDispatcher
{

    public function dispatch(
        IFillContracts $oContractFillers,
        IChangeRequest $oRequest,
        $oEndPointObject,
        IProcessContent $oContentProcessor,
        IConstructInjection $oGlobalDependencyContainer
    );

    public function getEndPointObject(
        $sEndPointFullClassName,
        IFillContracts $oContractFillers,
        IAmEndPointValue $oEndPointSpec,
        IConstructInjection $oGlobalDependencyContainer
    );

    /**
     * @return IAmDispatchManifest
     */
    public function getDispatchManifest();

}