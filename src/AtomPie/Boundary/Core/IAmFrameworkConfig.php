<?php
namespace AtomPie\Boundary\Core;

use AtomPie\Boundary\System\IAmRouter;
use AtomPie\Boundary\System\IHandleException;
use AtomPie\Boundary\System\IRunAfterMiddleware;
use AtomPie\Boundary\System\IRunBeforeMiddleware;
use AtomPie\DependencyInjection\Boundary\IFillContracts;

interface IAmFrameworkConfig
{

    /**
     * @return void
     */
    public function lock();
    /**
     * @return ISetUpContentProcessor[]
     */
    public function getContentProcessors();

    /**
     * @return IHandleException
     */
    public function getErrorHandler();
    
    /**
     * @return IRunAfterMiddleware[]|IRunBeforeMiddleware[]
     */
    public function getMiddleware();
    
    /**
     * @return IFillContracts
     */
    public function getContractsFillers();

    /**
     * @return array
     */
    public function getEventNamespaces();

    /**
     * @return array
     */
    public function getEventClasses();

    /**
     * @return string
     */
    public function getDefaultEndPoint();

    /**
     * @return IAmRouter
     */
    public function getRouter();

    /**
     * @return string
     */
    public function getEndPointDir();
}