<?php
namespace AtomPie\Boundary\Core\Dispatch;

use Generi\Boundary\IType;

interface IHaveComponentName
{

    /**
     * @return string
     */
    public function getComponentName();

    /**
     * @return IType
     */
    public function getComponentType();

}

interface IAmEventSpecImmutable extends IHaveComponentName
{

    public function hasEvent();

    public function getEvent();

    public function getEventMethod();

    public function __toString();

    public function __toUrlString();

}