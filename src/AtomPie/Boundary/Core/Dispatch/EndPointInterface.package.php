<?php
namespace AtomPie\Boundary\Core\Dispatch;

interface IHaveAccessToEndPoint
{

    /**
     * Returns instance of EndPointUrl filled with EndPoint string.
     *
     * @return IAmEndPointUrl
     */
    public function cloneEndPointUrl();

}

interface IAmEndPointValue extends IHaveAccessToEndPoint
{
    /**
     * @return string
     */
    public function getEndPointString();

    /**
     * @return string
     */
    public function getMethodString();

    /**
     * @return bool
     */
    public function isDefaultMethod();

    /**
     * @return bool
     */
    public function hasMethod();

    /**
     * @return string
     */
    public function __toString();

    public function __toUrlString();

}