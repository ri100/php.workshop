<?php
namespace AtomPie\Boundary\Core\Dispatch;

interface IHaveEndPointSpec
{
    /**
     * @return IAmEndPointValue
     */
    public function getEndPoint();
}

interface IHaveEventSpec
{
    /**
     * @return bool
     */
    public function hasEventSpec();

    /**
     * @return IAmEventSpecImmutable
     */
    public function getEventSpec();
}

interface IAmDispatchManifest extends IHaveEndPointSpec, IHaveEventSpec
{

    /**
     * @param $sComponentType
     * @param $sComponentName
     * @param $sEvent
     * @return IAmEventSpecImmutable
     */
    public function newEventSpec(
        $sComponentType,
        $sComponentName,
        $sEvent
    );

    /**
     * @return string
     */
    public function __toString();
}