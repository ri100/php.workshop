<?php
namespace AtomPie\Boundary\Core;

interface ISetUpContentProcessor
{
    public function configureProcessor(IRegisterContentProcessors $oContentProcessor);
}