<?php
namespace AtomPie\Boundary\Gui\Component;

use Generi\Boundary\IHaveName;

interface IHaveContext extends IHaveName, IHaveNamespace
{

}