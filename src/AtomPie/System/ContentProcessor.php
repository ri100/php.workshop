<?php
namespace AtomPie\System {

    use AtomPie\Boundary\Core\IRegisterContentProcessors;
    use AtomPie\Boundary\Core\IProcessContent;
    use AtomPie\Boundary\Core\ISerializeModel;
    use AtomPie\System\ContentProcessor\XmlPostProcessor;
    use AtomPie\System\Dispatch\DispatchException;
    use AtomPie\Web\Boundary\IRecognizeMediaType;
    use Generi\Boundary\IStringable;

    class ContentProcessor implements IProcessContent, IRegisterContentProcessors
    {

        private $aAfterProcessThatConvertEndpointContentToHtml = [];
        private $aBeforeProcessEndpointInit = [];
        private $aFinallyProcessEndpointInit = [];

        public function __construct()
        {
        }

        public function registerAfter($sReturnType, $pClosure)
        {
            $this->aAfterProcessThatConvertEndpointContentToHtml[$sReturnType] = $pClosure;
        }

        public function registerBefore($pClosure)
        {
            $this->aBeforeProcessEndpointInit[] = $pClosure;
        }

        public function registerFinally($sReturnType, $pClosure)
        {
            $this->aFinallyProcessEndpointInit[$sReturnType] = $pClosure;
        }

        /**
         * @param $sClassName
         * @param $aCollection
         * @return bool|callable
         */
        private function getClosureByType($sClassName, &$aCollection)
        {
            if (isset($aCollection[$sClassName])) {
                $pClosure = $aCollection[$sClassName];
                if (is_callable($pClosure)) {
                    return $pClosure;
                }
            }

            return false;
        }

        /**
         * @param $mContent
         * @param $sType
         * @param $aCollection
         * @return bool|callable
         */
        private function getClosureBySubTypeOfContent($mContent, $sType, &$aCollection)
        {
            if ($mContent instanceof $sType) {
                $pClosure = $aCollection[$sType];
                if (is_callable($pClosure)) {
                    return $pClosure;
                }
            }

            return false;
        }

        /**
         * Run full stack of content processing
         *
         * @param $mContent
         * @param IRecognizeMediaType $oContentType
         * @return array|mixed|string
         * @throws DispatchException
         */
        public function finish($mContent, IRecognizeMediaType $oContentType) {
            $this->processBeforeEndPoint();
            $mContent = $this->processAfterEndPoint($mContent);
            return $this->processFinally($mContent, $oContentType);
        }
        
        /**
         * Invokes closures that init dependencies of
         * the processed classes. Most of the time it is the
         * dependency injection container.
         */
        public function processBeforeEndPoint()
        {
            if(!empty($this->aBeforeProcessEndpointInit)) {
                foreach ($this->aBeforeProcessEndpointInit as $pClosure) {
                    $pClosure();
                }
            }
        }

        /**
         * Finds and invokes closure from closure repository.
         * Closures are indexed by content class type.
         * If $mContent is not an object it will not be
         * processed aby any closure.
         *
         * @param $mContent
         * @return string
         * @throws DispatchException
         */
        public function processAfterEndPoint($mContent)
        {
            /*
             * $mContent can be any type of content.
             * Here we are expecting Page or Component.
             */
            return $this->runByClosure($mContent, $this->aAfterProcessThatConvertEndpointContentToHtml);
        }

        /**
         * @param $mContent
         * @param IRecognizeMediaType $oContentType
         * @return array|mixed|string
         * @throws DispatchException
         */
        public function processFinally($mContent, IRecognizeMediaType $oContentType)
        {

            if ($oContentType->isJson()) {

                if ($mContent instanceof ISerializeModel) {
                    $mContent = $this->placeHoldersToArray($mContent->__toModel());
                }

                $mContent = json_encode($mContent);

                if ($mContent === false) {
                    throw new DispatchException('EndPoint of Content-Type: application/json returned data that could not be encoded to JSON string!');
                }

            } else if ($oContentType->isXml()) {

                if ($mContent instanceof ISerializeModel) {
                    $mContent = $this->placeHoldersToArray($mContent->__toModel());
                }

                $oXMLProcessor = new XmlPostProcessor();
                $mContent = $oXMLProcessor->convertToXml($mContent);

                if ($mContent === null) {
                    throw new DispatchException('EndPoint of Content-Type: application/xml returned data that could not be serialized to XML');
                }

            } else {
                $mContent = $this->runFinallyByClosure($mContent);
            }

            return $mContent;
        }

        private function placeHoldersToArray($aPlaceHolders)
        {

            $aData = array();
            foreach ($aPlaceHolders as $sKey => $mPlaceHolder) {
                if (is_array($mPlaceHolder) || $mPlaceHolder instanceof \Iterator) {
                    $aData[$sKey] = $this->placeHoldersToArray($mPlaceHolder);
                } else {
                    if ($mPlaceHolder instanceof ISerializeModel) {
                        $aData[$sKey] = $this->placeHoldersToArray($mPlaceHolder->__toModel());
                    } else {
                        if ($mPlaceHolder instanceof \stdClass) {
                            $aData[$sKey] = (array)$mPlaceHolder;
                        } else {
                            if ($mPlaceHolder instanceof IStringable) {
                                $aData[$sKey] = $mPlaceHolder->__toString();
                            } else {
                                $aData[$sKey] = $mPlaceHolder;
                            }
                        }
                    }
                }
            }

            return $aData;

        }

        private function canBeString($sValue)
        {
            if (is_object($sValue) && method_exists($sValue, '__toString')) {
                return true;
            }
            if (is_null($sValue)) {
                return true;
            }
            return is_scalar($sValue);
        }

        /**
         * @param $mContent
         * @return string
         * @throws DispatchException
         */
        private function runFinallyByClosure($mContent)
        {
            $mContent = $this->runByClosure($mContent, $this->aFinallyProcessEndpointInit);

            // All other text/html, application/xhtml+xml, etc.
            if (!$this->canBeString($mContent)) {
                throw new DispatchException('EndPoint of Content-Type: text/html returned object that can not be casted to string! Could not convert array to string. Please return string.');
            }

            return (string)$mContent;
        }

        private function runByClosure($mContent, $aClosureDefinition)
        {

            if (is_object($mContent)) {
                $pClosure = $this->getClosureByType(get_class($mContent), $aClosureDefinition);
                if ($pClosure !== false) {
                    // Hard binding to class type
                    return $pClosure($mContent);
                } else {
                    // Soft binding. It can be a subclass or interface
                    foreach ($aClosureDefinition as $sType => $pClosure) {
                        $pClosure = $this->getClosureBySubTypeOfContent($mContent, $sType, $aClosureDefinition);
                        if (false !== $pClosure) {
                            return $pClosure($mContent);
                        }
                    }
                }
            }
            return $mContent;
        }

    }

}
