<?php
namespace AtomPie\System;

use AtomPie\Boundary\System\IAddRoutes;
use AtomPie\Boundary\System\IRoute;
use AtomPie\Core\Dispatch\EndPointImmutable;

class TreeRouterFacade extends \TreeRoute\Router implements IAddRoutes { }

class Router implements IRoute
{

    /**
     * @var Router
     */
    private $oRouter;

    /**
     * @var RouteManifest
     */
    private $oRouteManifest;

    public function __construct()
    {
        $this->oRouter = new TreeRouterFacade();
    }

    /**
     * @param $aServerVariable
     * @return array
     * @throws Exception
     */
    public function rewrite($aServerVariable)
    {
        $sMethod = $aServerVariable['REQUEST_METHOD'];
        $sRequestUri = $aServerVariable['REQUEST_URI'];

        $aResult = $this->oRouter->dispatch($sMethod, $sRequestUri);

        if (isset($aResult['error'])) {

            switch ($aResult['error']['code']) {
                case 405 :
                    // Method not allowed handler here
                    $allowedMethods = $aResult['allowed'];
                    throw new Exception(\AtomPie\I18n\Label('Method not allowed. Allowed methods %s', implode(',', $allowedMethods)), 405);
            }

            return null;

        }

        $sEndPoint = $aResult['handler'];
        $aRequestParams = $aResult['params'];
        $aRequestParams[EndPointImmutable::END_POINT_QUERY] = $sEndPoint;

        return $aRequestParams;

    }

    /**
     * @param $sUrl
     * @return RouteManifest
     */
    public function post($sUrl)
    {
        $this->oRouteManifest = new RouteManifest(
            $this->oRouter,
            ['POST'], $sUrl);


        return $this->oRouteManifest;
    }

    /**
     * @param $sUrl
     * @return RouteManifest
     */
    public function get($sUrl)
    {
        $this->oRouteManifest = new RouteManifest(
            $this->oRouter,
            ['GET'], $sUrl);
        return $this->oRouteManifest;
    }

    /**
     * @param $sUrl
     * @return RouteManifest
     */
    public function put($sUrl)
    {
        $this->oRouteManifest = new RouteManifest(
            $this->oRouter,
            ['PUT'], $sUrl);
        return $this->oRouteManifest;
    }

    /**
     * @param $sUrl
     * @return RouteManifest
     */
    public function delete($sUrl)
    {
        $this->oRouteManifest = new RouteManifest(
            $this->oRouter,
            ['DELETE'], $sUrl);
        return $this->oRouteManifest;
    }

}