<?php
namespace AtomPie\System {

    use AtomPie\Boundary\Core\Dispatch\IAmDispatchManifest;
    use AtomPie\Boundary\Core\IProcessContent;
    use AtomPie\Boundary\Core\ISetUpContentProcessor;
    use AtomPie\Boundary\Core\ISetUpDependencyContainer;
    use AtomPie\Boundary\Core\IAmFrameworkConfig;
    use AtomPie\Boundary\System\IAmContext;
    use AtomPie\DependencyInjection\Dependency;
    use AtomPie\System\DependencyContainer\EndPointDependencyContainer;

    class Init
    {

        /**
         * @var \AtomPie\Core\Dispatch\DispatchManifest
         */
        private $oDispatchManifest;

        /**
         * @var IAmFrameworkConfig
         */
        private $oConfig;

        /**
         * @var ISetUpContentProcessor[]
         */
        private $aContentProcessors;

        public function __construct()
        {
        }

        /**
         * @param IAmContext $oContext
         * @param IAmFrameworkConfig $oConfig
         * @param IAmDispatchManifest $oDispatchManifest
         * @param array $aContentProcessors
         * @return Application
         */
        public function initApplication(
            IAmContext $oContext,
            IAmFrameworkConfig $oConfig,
            IAmDispatchManifest $oDispatchManifest,
            array $aContentProcessors
        ) {

            $this->oConfig = $oConfig;
            $this->oDispatchManifest = $oDispatchManifest;
            $oDefaultContentProcessor = new ContentProcessor();
            $this->aContentProcessors = $aContentProcessors;

            $oApplication = $this->newApplication(
                $oContext,
                $this->oDispatchManifest,
                $oDefaultContentProcessor
            );

            // Merge dependencies for EndPoint -----------------

            // Add dependency injection for application

            $aDependencySet = [
                Application::class => function () use ($oApplication) {
                    return $oApplication;
                },
                IAmDispatchManifest::class => function () use ($oDispatchManifest) {
                    return $oDispatchManifest;
                },
            ];

            $oEndPointDependencyContainer = $this->factoryEndPointDependencyContainer(
                $oContext,
                $aDependencySet
            );

            $oApplication->injectDependency($oEndPointDependencyContainer);

            // Merge dependencies for content processors -----------------

            foreach ($this->aContentProcessors as $oContentProcessorFactory) {
                if ($oContentProcessorFactory instanceof ISetUpContentProcessor) {
                    if ($oContentProcessorFactory instanceof ISetUpDependencyContainer) {
                        $oContentProcessorFactory->mergeGlobalDependencyContainer($aDependencySet);
                    }
                    $oContentProcessorFactory->configureProcessor($oDefaultContentProcessor);
                }
            }

            // Sets dependency injections
            $oDefaultContentProcessor->processBeforeEndPoint();

            return $oApplication;

        }

        /**
         * @param IAmContext $oContext
         * @param IAmDispatchManifest $oDispatchManifest
         * @param IProcessContent $oContentProcessor
         * @return Application
         */
        private function newApplication(
            IAmContext $oContext,
            IAmDispatchManifest $oDispatchManifest,
            IProcessContent $oContentProcessor
        ) {

            $oDispatcher = new Dispatcher($oDispatchManifest);

            return new Application(
                $oDispatcher,
                $oContext->getEnvironment(),
                $oContentProcessor
            );

        }

        /**
         * @param IAmContext $oContext
         * @param $aDependencySet
         * @return EndPointDependencyContainer
         */
        private function factoryEndPointDependencyContainer(IAmContext $oContext, $aDependencySet)
        {
            $oEnvironment = $oContext->getEnvironment();
            $oDependency = new Dependency();
            $oDependency->addDependency($aDependencySet);

            $oThisEndPointUrlProvider = new UrlProvider(
                $this->oDispatchManifest,
                $oEnvironment->getRequest()->getAllParams()
            );

            // EndPoint
            /** @noinspection PhpInternalEntityUsedInspection */
            $oEndPointDependencyContainer = new EndPointDependencyContainer(
                $oEnvironment,
                $this->oConfig,
                $this->oDispatchManifest,
                $oThisEndPointUrlProvider);

            // All methods
            $oEndPointDependencyContainer
                ->forAnyClass()// EndPoints do not have defined class they extend so DI is for all types
                ->merge($oDependency);

            return $oEndPointDependencyContainer;
        }

    }

}
