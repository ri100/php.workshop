<?php
namespace AtomPie\System\Dispatch {

    use AtomPie\I18n;
    use AtomPie\Web\Boundary\IAmRequest;
    use AtomPie\Web\Boundary\IHaveHeaders;
    use AtomPie\Web\Boundary\IHaveHttpMethod;
    use \AtomPie\Web\Connection\Http;
    use AtomPie\Web\Connection\Http\Header\Status;
    use AtomPie\Web\Connection\Http\Request;

    class ClientValidator
    {

        /**
         * @var ClientConstrain
         */
        private $oClient;

        /**
         * @var DispatchException
         */
        private $oError;

        public function __construct(ClientConstrain $oClient)
        {
            $this->oClient = $oClient;
        }

        /**
         * @param IAmRequest $oRequest
         * @return bool
         * @throws DispatchException
         */
        public function validate(IAmRequest $oRequest)
        {
            return
                $this->validateAccept($oRequest) &&
                $this->validateMethod($oRequest) &&
                $this->validateContentType($oRequest) &&
                $this->validateType($oRequest);
        }

        /**
         * @param IAmRequest $oRequest
         * @return bool
         * @throws DispatchException
         */
        public function validateContentType($oRequest)
        {
            $this->oError = null;
            if ($this->oClient->getContentType() !== null) {

                if ($oRequest->getMethod() == Request\Method::GET) {
                    throw new DispatchException(
                        I18n\Label(
                            'Can not check content-type on request send with GET method.'
                        )
                    );
                }

                if (!$oRequest->hasHeader(Http\Header::CONTENT_TYPE)) {
                    throw new DispatchException(
                            I18n\Label(
                                'Could not read content-type header from request. Available headers [%s]',
                                implode(',', array_keys($oRequest->getHeaders()))
                        ),
                        Status::UNSUPPORTED_MEDIA_TYPE
                    );
                }

                $sContentType = $oRequest->getHeader(Http\Header::CONTENT_TYPE)->getValue();

                if (0 != strcasecmp($sContentType, $this->oClient->getContentType())) {
                    $this->oError = new DispatchException(
                        I18n\Label('Incorrect request content-type. Expected [%s], given [%s]',
                            $this->oClient->getContentType(),
                            $sContentType
                        ),
                        Http\Header\Status::UNSUPPORTED_MEDIA_TYPE
                    );

                    return false;
                }

            }
            return true;
        }

        /**
         * @param IAmRequest $oRequest
         * @return bool
         * @throws DispatchException
         */
        public function validateType($oRequest)
        {
            $this->oError = null;
            if (null !== $this->oClient->getType()) {
                $bIsCorrect = true;

                if (strtolower($this->oClient->getType()) == 'ajax') {
                    $bIsCorrect = $oRequest->isAjax();
                } else {
                    if (strtolower($this->oClient->getType()) == 'cli') {
                        $bIsCorrect = $this->isCli();
                    } else {
                        if (strtolower($this->oClient->getType()) == 'webrequest') {
                            $bIsCorrect = !$this->isCli();
                        }
                    }
                }

                if (!$bIsCorrect) {

                    $this->oError = new DispatchException(
                        I18n\Label('Incorrect client type. Expected [%s].',
                            $this->oClient->getType()
                        ),
                        Http\Header\Status::FORBIDDEN
                    );

                    return false;

                }
            }
            return true;
        }

        /**
         * @param IHaveHttpMethod $oRequest
         * @return bool
         * @throws DispatchException
         */
        public function validateMethod($oRequest)
        {
            $this->oError = null;
            if (null !== $this->oClient->getMethod()) {
                $aMethods = explode(',', strtoupper($this->oClient->getMethod()));
                $sRequestMethod = $oRequest->getMethod();
                if (!in_array(strtoupper($sRequestMethod), $aMethods)) {
                    $this->oError = new DispatchException(
                        I18n\Label('Incorrect method. Expected [%s], given [%s].',
                            $this->oClient->getMethod(),
                            $sRequestMethod
                        ),
                        Status::METHOD_NOT_ALLOWED
                    );
                    return false;
                }
            }
            return true;
        }

        /**
         * @param IHaveHeaders $oRequest
         * @return bool
         * @throws DispatchException
         */
        public function validateAccept($oRequest)
        {
            $this->oError = null;
            if ($oRequest->hasHeader(Http\Header::ACCEPT)) {

                /** @var Http\Header\Accept $oAcceptHeader */
                $oAcceptHeader = $oRequest->getHeader(Http\Header::ACCEPT);

                if ($this->oClient->getAccept() !== null) {

                    if (!$oAcceptHeader->willYouAcceptMediaType($this->oClient->getAccept(), false)) {

                        $this->oError = new DispatchException(
                            I18n\Label('EndPoint requires that client accepts %s media-type. Client accepts %s.',
                                $this->oClient->getAccept(),
                                $oAcceptHeader->getValue()
                            ),
                            Status::NOT_ACCEPTABLE
                        );
                        return false;

                    }

                }
            }

            return true;
        }

        /**
         * @return bool
         */
        private function isCli()
        {
            return (php_sapi_name() === 'cli' || defined('STDIN'));
        }

        /**
         * @return DispatchException
         */
        public function getError()
        {
            return $this->oError;
        }
    }

}
