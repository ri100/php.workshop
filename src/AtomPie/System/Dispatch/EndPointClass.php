<?php
namespace AtomPie\System\Dispatch {

    use AtomPie\Boundary\System\IAmEndPointClass;

    class EndPointClass implements IAmEndPointClass
    {

        /**
         * @var string
         */
        private $sClassName;

        public function __construct($sClassName)
        {
            $this->sClassName = $sClassName;
        }

        /**
         * @return string
         */
        public function getClassName()
        {
            return $this->sClassName;
        }
    }

}
