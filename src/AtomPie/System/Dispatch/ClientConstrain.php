<?php
namespace AtomPie\System\Dispatch {

    final class ClientConstrain
    {

        /**
         * @var string
         */
        private $Accept;

        /**
         * @var string
         */
        private $Type;

        /**
         * @Enum({"GET","POST","PUT","DELETE","HEAD","OPTIONS"})
         */
        private $Method;

        /**
         * @var string
         */
        private $ContentType;

        /**
         * @return string
         */
        public function getAccept()
        {
            return $this->Accept;
        }

        /**
         * @return string
         */
        public function getContentType()
        {
            return $this->ContentType;
        }

        /**
         * @return mixed
         */
        public function getMethod()
        {
            return $this->Method;
        }

        /**
         * @return string
         */
        public function getType()
        {
            return $this->Type;
        }

        /**
         * @param string $Accept
         * @return ClientConstrain
         */
        public function accept($Accept)
        {
            $this->Accept = $Accept;
            return $this;
        }

        /**
         * @param string $Type
         * @return ClientConstrain
         */
        public function hasType($Type)
        {
            $this->Type = $Type;
            return $this;
        }

        /**
         * @param mixed $Method
         * @return ClientConstrain
         */
        public function hasMethod($Method)
        {
            $this->Method = $Method;
            return $this;
        }

        /**
         * @param string $ContentType
         * @return ClientConstrain
         */
        public function hasContentType($ContentType)
        {
            $this->ContentType = $ContentType;
            return $this;
        }

    }

}
