<?php
namespace AtomPie\System\Dispatch {

    use AtomPie\Boundary\System\IAmRequestValidator;
    use AtomPie\I18n;
    use AtomPie\Web\Boundary\IAmRequest;
    use AtomPie\Web\Connection\Http;
    use AtomPie\Web\Connection\Http\Header\Status;
    use AtomPie\Web\Connection\Http\Request;

    class RequestValidator implements IAmRequestValidator
    {

        /**
         * @var IAmRequest
         */
        private $oRequest;

        public function __construct($oRequest)
        {
            $this->oRequest = $oRequest;
        }

        /**
         * @param $sContentType
         * @return $this
         * @throws DispatchException
         * @throws I18n\Exception
         */
        public function isContentType($sContentType)
        {

            if (!empty($sContentType)) {

                if ($this->oRequest->getMethod() == Request\Method::GET) {
                    throw new DispatchException(
                        I18n\Label(
                            'Can not check content-type on request send with GET method.'
                        )
                    );
                }

                if (!$this->oRequest->hasHeader(Http\Header::CONTENT_TYPE)) {
                    throw new DispatchException(
                        I18n\Label(
                            'Could not read content-type header from request. Available headers [%s]',
                            implode(',', array_keys($this->oRequest->getHeaders()))
                        ),
                        Status::UNSUPPORTED_MEDIA_TYPE
                    );
                }

                $sRequestContentType = $this->oRequest->getHeader(Http\Header::CONTENT_TYPE)->getValue();

                if (0 != strcasecmp($sRequestContentType, $sContentType)) {
                    throw new DispatchException(
                        I18n\Label('Incorrect request content-type. Expected [%s], given [%s]',
                            $sContentType,
                            $sRequestContentType
                        ),
                        Http\Header\Status::UNSUPPORTED_MEDIA_TYPE
                    );
                }

            }

            return $this;
        }

        /**
         * @return $this
         * @throws DispatchException
         * @throws I18n\Exception
         */
        public function isAjax()
        {
            if (!$this->oRequest->isAjax()) {
                throw new DispatchException(
                    I18n\Label('Incorrect client type. Ajax request expected.'),
                    Http\Header\Status::FORBIDDEN
                );
            }

            return $this;
        }

        /**
         * @param $sMethod
         * @return $this
         * @throws DispatchException
         * @throws I18n\Exception
         */
        public function isMethod($sMethod)
        {
            if (!empty($sMethod)) {
                $aMethods = explode(',', strtoupper($sMethod));
                $sRequestMethod = $this->oRequest->getMethod();
                if (!in_array(strtoupper($sRequestMethod), $aMethods)) {
                    throw new DispatchException(
                        I18n\Label('Incorrect method. Expected [%s], given [%s].',
                            $sMethod,
                            $sRequestMethod
                        ),
                        Status::METHOD_NOT_ALLOWED
                    );
                }
            }

            return $this;
        }

        /**
         * @param $sAccept
         * @return $this
         * @throws DispatchException
         * @throws I18n\Exception
         */
        public function accepts($sAccept)
        {
            if ($this->oRequest->hasHeader(Http\Header::ACCEPT)) {

                /** @var Http\Header\Accept $oAcceptHeader */
                $oAcceptHeader = $this->oRequest->getHeader(Http\Header::ACCEPT);

                if (isset($sAccept)) {

                    if (!$oAcceptHeader->willYouAcceptMediaType($sAccept, false)) {

                        throw new DispatchException(
                            I18n\Label('EndPoint requires that client accepts %s media-type. Client accepts %s.',
                                $sAccept,
                                $oAcceptHeader->getValue()
                            ),
                            Status::NOT_ACCEPTABLE
                        );

                    }

                }
            }

            return $this;

        }

        /**
         * @return IAmRequest
         */
        public function getRequest()
        {
            return $this->oRequest;
        }

    }

}
