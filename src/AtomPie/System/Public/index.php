<?php
use AtomPie\Core\FrameworkConfig;
use AtomPie\System\Kernel;

$sVendorDir = __DIR__ . '/../../../../../../';
/** @noinspection PhpIncludeInspection */
$oLoader = require $sVendorDir . 'autoload.php';

$oConfig = new FrameworkConfig(
    realpath(__DIR__ . '/../../../../../../../.atompie/EndPoint'),
    "Main"
);

// Kernel

$oKernel = new Kernel($oLoader);
$oResponse = $oKernel->boot($oConfig);
$oResponse->send();
