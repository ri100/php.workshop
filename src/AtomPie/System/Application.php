<?php
namespace AtomPie\System {

    use AtomPie\Boundary\System\IAmContext;
    use AtomPie\EventBus\EventHandler;
    use AtomPie\DependencyInjection\DependencyContainer;
    use AtomPie\Boundary\Core\EventBus\IHandleEvents;
    use AtomPie\Boundary\Core\IAmFrameworkConfig;
    use AtomPie\Boundary\Core\IProcessContent;
    use AtomPie\Boundary\System\IAmDispatcher;
    use AtomPie\System\Dispatch\DispatchException;
    use AtomPie\Web\Boundary\IAmContent;
    use AtomPie\Web\Boundary\IAmContentType;
    use AtomPie\Web\Boundary\IAmEnvironment;
    use AtomPie\Web\Boundary\IAmResponse;
    use AtomPie\Web\Boundary\IChangeResponse;
    use AtomPie\I18n;
    use AtomPie\Web\Connection\Http\Header;

    class Application implements IHandleEvents
    {

        const EVENT_BEFORE_PROCESSING = '@BeforeProcessing';
        const EVENT_BEFORE_RENDERING = '@BeforeRendering';
        const EVENT_BEFORE_RESPONDING = '@BeforeResponding';
        const EVENT_BEFORE_DISPATCH = '@BeforeDispatch';
        const EVENT_AFTER_END_POINT_INVOKE = '@AfterEndPointInvoke';

        use EventHandler;

        /**
         * @var DependencyContainer
         */
        private $oDependencyContainer;

        /**
         * @var IAmDispatcher
         */
        private $oDispatcher;

        /**
         * @var IAmEnvironment
         */
        private $oEnvironment;

        /**
         * @var IProcessContent
         */
        private $oContentProcessor;

        public function __construct(
            IAmDispatcher $oDispatcher,
            IAmEnvironment $oEnvironment,
            IProcessContent $oContentProcessor = null
        ) {

            $this->oEnvironment = $oEnvironment;
            $this->oDispatcher = $oDispatcher;
            $this->oContentProcessor = $oContentProcessor;

        }

        /**
         * @return DependencyContainer
         */
        public function getGlobalDependencyContainer()
        {
            if (isset($this->oDependencyContainer)) {
                return $this->oDependencyContainer;
            } else {
                /** @noinspection PhpInternalEntityUsedInspection */
                return new DependencyContainer();
            }
        }

        public function injectDependency(DependencyContainer $oDependencyContainer)
        {
            $this->oDependencyContainer = $oDependencyContainer;
        }

        /**
         * @param IAmFrameworkConfig $oConfig
         * @param IAmContext $oContext
         * @return IChangeResponse
         * @throws DispatchException
         * @throws Exception
         * @throws \AtomPie\EventBus\Exception
         */
        public function run(IAmFrameworkConfig $oConfig, IAmContext $oContext)
        {

            $oDispatcherManifest =  $this->oDispatcher->getDispatchManifest();

            $oEndPointSpec = $oDispatcherManifest->getEndPoint();

            //////////////////////////////////
            // Constructing end-point object

            $oClassToInvoke = $oContext->getEndPointClass();
            $oEnvironment = $oContext->getEnvironment();

            if($oClassToInvoke->getClassName() ==  NoClass::class) {
                return $oEnvironment->getResponse();
            }

            $sFullEndPointClassName = $oClassToInvoke->getClassName();
            
            $oGlobalDependencyContainer = $this->getGlobalDependencyContainer();

            ///////////////////////////////////////////
            // Throw exception if class does not exist

            if (!class_exists($sFullEndPointClassName)) {

                throw new DispatchException(
                    I18n\Label('Access to EndPoints denied. Please declare EndPoint in [%s].', $oConfig->getEndPointDir()),
                    Header\Status::INTERNAL_SERVER_ERROR
                );

            }

            ////////////////////////////////////////
            // Contract fillers

            $oGlobalContractFillers = $oConfig->getContractsFillers();
            $oLocalContractFillers = $oContext->getContractFillers();

            // Overrides global with local definition

            $oContractFillers = $oGlobalContractFillers->append($oLocalContractFillers);

            //////////////////////////////////////////////////////////////
            // Returns EndPoint object if EndPoint method is not static

            $mEndPointObject = $this->oDispatcher->getEndPointObject(
                $sFullEndPointClassName,
                $oContractFillers,
                $oEndPointSpec,
                $oGlobalDependencyContainer);

            $this->triggerEvent(self::EVENT_BEFORE_DISPATCH, $mEndPointObject);

            // Dispatch
            
            $oRequest = $oEnvironment->getRequest();
            $mContent = $this->oDispatcher->dispatch(
                $oContractFillers,
                $oRequest,
                $mEndPointObject,
                $this->oContentProcessor,
                $oGlobalDependencyContainer
            );

            $this->triggerEvent(self::EVENT_AFTER_END_POINT_INVOKE, $mContent);

            // Check on return

            $oOnReturn = $oContext->getOnReturn();
            if($oOnReturn instanceof \Closure) {
                $mContent = $oOnReturn($mContent);
            }

            // Set response from context

            $oResponse = $oEnvironment->getResponse();
            $this->checkResponseConsistency($oResponse);

            $oContent = $oResponse->getContent();
            $oContentType = $oContent->getContentType();

            $this->setContent($oContent, $oContentType, $mContent);

            return $oResponse;
        }

        /**
         * @return IAmDispatcher
         */
        public function getDispatcher()
        {
            return $this->oDispatcher;
        }

        /**
         * @param IAmContent $oContent
         * @param IAmContentType $oContentType
         * @param $mContent
         * @throws \AtomPie\EventBus\Exception
         */
        private function setContent($oContent, $oContentType, $mContent)
        {

            // Set process and content

            $this->triggerEvent(self::EVENT_BEFORE_PROCESSING, $mContent);

            $mContent = $this->oContentProcessor->processAfterEndPoint($mContent);

            $this->triggerEvent(self::EVENT_BEFORE_RENDERING, $mContent);

            $mContent = $this->oContentProcessor->processFinally($mContent, $oContentType);

            $this->triggerEvent(self::EVENT_BEFORE_RESPONDING, $mContent);

            $oContent->setContent($mContent);
        }

        /**
         * @param IAmResponse $oResponse
         * @throws Exception
         * @throws I18n\Exception
         */
        private function checkResponseConsistency($oResponse)
        {
            if ($oResponse->getContent()->getContentType()->getValue() != $oResponse->getHeader(Header::CONTENT_TYPE)->getValue()) {
                throw new Exception(I18n\Label('Response Content-Type and Content-Type header are not equal.'));
            }
        }

    }

}
