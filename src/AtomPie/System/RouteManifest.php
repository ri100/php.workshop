<?php
namespace AtomPie\System {

    use AtomPie\Boundary\System\IAddRoutes;

    class RouteManifest {

        private $sUrl;
        private $aMethods;
        private $sDispatchManifest;
        /**
         * @var IAddRoutes
         */
        private $oRouter;

        public function __construct(
            IAddRoutes $oRouter,
            array $aMethods = null,
            $sUrl = null,
            $sDispatchManifest = null)
        {
            $this->sUrl = $sUrl;
            $this->aMethods = $aMethods;
            $this->sDispatchManifest = $sDispatchManifest;
            $this->oRouter = $oRouter;
        }

        /**
         * @return string
         */
        public function getUrl()
        {
            return $this->sUrl;
        }

        /**
         * @return string
         */
        public function getMethods()
        {
            return $this->aMethods;
        }

        /**
         * @return string
         */
        public function getDispatchManifest()
        {
            return $this->sDispatchManifest;
        }

        /**
         * @param $sDispatchManifest
         * @return $this
         */
        public function routeTo($sDispatchManifest)
        {
            $this->sDispatchManifest = $sDispatchManifest;
            $this->isValid();
            $this->oRouter->addRoute(
                $this->aMethods,
                $this->sUrl,
                $this->sDispatchManifest
            );
            return $this;
        }

        private function isValid() {
            if(empty($this->aMethods)) {
                throw new Exception('Route method is not defined.');
            }

            if(!isset($this->sDispatchManifest)) {
                throw new Exception('Route dispatch manifest is not defined.');
            }
        }

        /**
         * @param array $aMethods
         * @return $this
         */
        public function asMethods(array $aMethods)
        {
            $this->aMethods = $aMethods;
            return $this;
        }
    }

}
