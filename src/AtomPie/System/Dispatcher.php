<?php
namespace AtomPie\System {

    use AtomPie\Core\Dispatch\EndPointImmutable;
    use AtomPie\DependencyInjection\Boundary\IFillContracts;
    use AtomPie\DependencyInjection\ContractFillers;
    use AtomPie\DependencyInjection\DependencyInjector;
    use AtomPie\DependencyInjection\Boundary\IConstructInjection;
    use AtomPie\Boundary\Core\Dispatch\IAmEndPointValue;
    use AtomPie\Boundary\Core\Dispatch\IAmDispatchManifest;
    use AtomPie\Boundary\Core\IProcessContent;
    use AtomPie\Core\Service\AuthorizeAnnotationService;
    use AtomPie\DependencyInjection\Exception;
    use AtomPie\System\Dispatch\DispatchException;
    use AtomPie\Boundary\System\IAmDispatcher;
    use AtomPie\Boundary\System\IControlAccess;
    use Generi\Boundary\IType;
    use AtomPie\Web\Boundary\IChangeRequest;
    use AtomPie\I18n;
    use AtomPie\Web\Connection\Http\Header\Status;

    /**
     * MVC controller. Dispatch service is responsible for dispatching to classes.method controller.
     *
     * Dependencies IAccessController
     *
     * @version $Id$
     * @author Risto Kowaczewski
     * @internal
     */
    class Dispatcher implements IAmDispatcher
    {

        const ON_AFTER_ENDPOINT_CALL = '@OnAfterEndPointCall';
        const ON_BEFORE_RESPONSE_FILL = '@OnBeforeResponseFill';

        /**
         * @var Dispatcher
         */
        protected static $oInstance;

        /**
         * @var IAmDispatchManifest
         */
        private $oDispatchManifest;

        /**
         * @param IAmDispatchManifest $oDispatchManifest
         */
        public function __construct(
            IAmDispatchManifest $oDispatchManifest
        ) {
            $this->oDispatchManifest = $oDispatchManifest;
        }

        /**
         * Checks whether there is an request event pending. May throw an Exception if
         * Kernel is not booted.
         *
         * @param IType $oComponent
         * @param $sComponentName
         * @param $sEvent
         * @return bool
         */
        public function hasRequestEvent(IType $oComponent, $sComponentName, $sEvent)
        {
            $oDispatcherManifest = $this->getDispatchManifest();
            if ($oDispatcherManifest->hasEventSpec()) {
                $oEventSpec = $oDispatcherManifest->getEventSpec();
                if ($oEventSpec->hasEvent()) {
                    return
                        strtolower($oEventSpec->getEvent()) == strtolower($sEvent)
                        && $oComponent->getFullName() == $oEventSpec->getComponentType()
                        && $sComponentName == $oEventSpec->getComponentName();
                }
            }
            return false;
        }

        /////////////////////
        // Getters / Setters

        /**
         * @return IAmDispatchManifest
         */
        public function getDispatchManifest()
        {
            return $this->oDispatchManifest;
        }

        ///////////////////////////////

        /**
         * Dispatch. Runs controller and afterward action.
         * @param IFillContracts $oContractFillers
         * @param IChangeRequest $oRequest
         * @param $oEndPointObject
         * @param IProcessContent $oContentProcessor
         * @param IConstructInjection $oGlobalDependencyContainer
         * @return mixed|object
         * @throws DispatchException
         * @throws \Generi\Exception
         */
        public function dispatch(
            IFillContracts $oContractFillers,
            IChangeRequest $oRequest,
            $oEndPointObject, // object or class name
            IProcessContent $oContentProcessor,
            IConstructInjection $oGlobalDependencyContainer
        ) {

            $oEndPointSpec = $this->getDispatchManifest()->getEndPoint();

            if (is_object($oEndPointObject)) {
                $sFullEndPointClassName = get_class($oEndPointObject);
            } else {
                $sFullEndPointClassName = $oEndPointObject;
            }

            ///////////////////////////////////////////////////
            // EndPoint annotations Authorize
            // On class level

            $oAuthorizeAnnotationHandler = new AuthorizeAnnotationService();
            // Can END dispatch if not authorized

            $oAuthorizeAnnotationHandler->checkAuthorizeAnnotation(
                $oEndPointObject
            );   // From class

            ////////////////////////////////////////
            // @EndPoint annotation on class level
            
            if ($oEndPointSpec->isDefaultMethod() && !method_exists($oEndPointObject,
                    EndPointImmutable::DEFAULT_METHOD)
            ) {

//                $this->validateClient(
//                    $oRequest, $oAnnotationHandler->getClientAnnotation($sFullEndPointClassName)
//                );

                if (is_object($oEndPointObject)) {
                    return $oEndPointObject;
                }

                return $this->newEndPointInstance(
                    $oEndPointObject
                    , $oGlobalDependencyContainer
                    , $oContractFillers
                );

            }

            //////////////////////////////////////
            // Get EndPoint Method

            $sEndPointMethodName = $this->getEndPointMethodName(
                $sFullEndPointClassName,
                $oEndPointSpec
            );

            /////////////////////////////////
            // Validate client expectations
            // @Client

//            $this->validateClient(
//                $oRequest, $oAnnotationHandler->getClientAnnotation($sFullEndPointClassName, $sEndPointMethodName)
//            );

            ///////////////////////////////////////////////////
            // EndPoint annotations Authorize
            // On method level

            $oAuthorizeAnnotationHandler->checkAuthorizeAnnotation(
                $oEndPointObject,
                $sEndPointMethodName
            ); // From method


            ////////////////////////////
            // Invoke

            return $this->invokeEndPoint(
                $oGlobalDependencyContainer,
                $oEndPointObject,
                $sFullEndPointClassName,
                $sEndPointMethodName,
                $oContractFillers
            );

        }

        /**
         * @param $sNameSpacedEndPointClass
         * @param IConstructInjection $oGlobalDependencyContainer
         * @param IFillContracts $oContractFillers
         * @return object
         */
        private function factorEndPointObject(
            $sNameSpacedEndPointClass
            , IConstructInjection $oGlobalDependencyContainer
            , IFillContracts $oContractFillers)
        {

            $oEndPointClass = $this->newEndPointInstance(
                $sNameSpacedEndPointClass
                , $oGlobalDependencyContainer
                , $oContractFillers);

            if ($oEndPointClass instanceof IControlAccess) {
                if (true !== $oEndPointClass->authorize()) {
                    $oEndPointClass->invokeNotAuthorized();
                }
            }

            return $oEndPointClass;
        }

        /**
         * @param IConstructInjection $oEndPointDependencyContainer
         * @param object $oEndPointClass
         * @param $sFullEndPointClassName
         * @param $sEndPointMethodName
         * @param ContractFillers $oContactFillers
         * @return mixed $ReturnContent
         * @throws Exception
         */
        private function invokeEndPoint(
            $oEndPointDependencyContainer,
            $oEndPointClass = null,
            $sFullEndPointClassName,
            $sEndPointMethodName,
            ContractFillers $oContactFillers = null
        ) {

            /////////////////////////////////////
            // Invoke EndPoint action
            // and return content
            
            $oDependencyInjector = new DependencyInjector($oEndPointDependencyContainer, $oContactFillers);
            $mContentReturnedByEndPoint = $oDependencyInjector->invokeMethod(
                is_object($oEndPointClass)
                    ? $oEndPointClass           // object method
                    : $sFullEndPointClassName,  // static method
                $sEndPointMethodName
            );

            return $mContentReturnedByEndPoint;

        }

        /**
         * @param $sFullEndPointClassName
         * @param IAmEndPointValue $oEndPointSpec
         * @return string
         * @throws DispatchException
         */
        private function getEndPointMethodName(
            $sFullEndPointClassName,
            IAmEndPointValue $oEndPointSpec
        ) {

            if (!$oEndPointSpec->hasMethod()) {
                throw new DispatchException(
                        I18n\Label('Empty EndPoint name for class [%s]. Can not invoke endpoint.',
                        $oEndPointSpec->getEndPointString()
                    ),
                    Status::NOT_FOUND
                );
            }

            /////////////////////////////////////////////////
            // At this point method must existsExactly in object

            $sEndPointMethodString = $oEndPointSpec->getMethodString();

            if (!method_exists($sFullEndPointClassName, $sEndPointMethodString)) {
                throw new DispatchException(
                    I18n\Label('Can not invoke endpoint [%s.%s]. Method may not exist.',
                        $sFullEndPointClassName,
                        $sEndPointMethodString
                    ),
                    Status::NOT_FOUND
                );
            }

            return $sEndPointMethodString;

        }

        /**
         * Returns EndPoint object it EndPoint method is not static
         *
         * @param $sEndPointFullClassName
         * @param IFillContracts $oContractFillers
         * @param IAmEndPointValue $oEndPointSpec
         * @param IConstructInjection $oGlobalDependencyContainer
         * @return object|string
         */
        public function getEndPointObject(
            $sEndPointFullClassName,
            IFillContracts $oContractFillers,
            IAmEndPointValue $oEndPointSpec,
            IConstructInjection $oGlobalDependencyContainer
        ) {

            $sMethodString = $oEndPointSpec->getMethodString();

            if (method_exists($sEndPointFullClassName, $sMethodString)) {

                $oReflectionMethod = new \ReflectionMethod($sEndPointFullClassName, $sMethodString);

                // Static
                if ($oReflectionMethod->isStatic()) {
                    return $sEndPointFullClassName;
                }

                return $this->factorEndPointObject(
                    $sEndPointFullClassName
                    , $oGlobalDependencyContainer
                    , $oContractFillers);
            }
            return $sEndPointFullClassName;

        }

        /**
         * @param $sNameSpacedEndPointClass
         * @param IConstructInjection $oGlobalDependencyContainer
         * @param IFillContracts $oContractFillers
         * @return object
         */
        private function newEndPointInstance(
            $sNameSpacedEndPointClass
            , IConstructInjection $oGlobalDependencyContainer
            , IFillContracts $oContractFillers)
        {
            if (!method_exists($sNameSpacedEndPointClass, '__construct')) {
                return new $sNameSpacedEndPointClass();
            } else {
                $oDependencyInjector = new DependencyInjector($oGlobalDependencyContainer, $oContractFillers);
                return $oDependencyInjector->newInstance($sNameSpacedEndPointClass);
            }
        }

    }

}