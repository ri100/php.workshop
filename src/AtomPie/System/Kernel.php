<?php
namespace AtomPie\System {

    use AtomPie\Boundary\System\IAmContext;
    use AtomPie\Boundary\System\IRoute;
    use AtomPie\Core\Dispatch\DispatchManifest;
    use AtomPie\Core\Dispatch\EndPointImmutable;
    use AtomPie\Core\Dispatch\EventSpecImmutable;
    use AtomPie\Web\Connection\Http\Response;
    use Composer\Autoload\ClassLoader;
    use AtomPie\EventBus\EventHandler;
    use AtomPie\Boundary\Core\IAmFrameworkConfig;
    use AtomPie\System\Error\DefaultErrorHandler;
    use AtomPie\Boundary\System\IHandleException;
    use AtomPie\Boundary\System\IRunAfterMiddleware;
    use AtomPie\Boundary\System\IRunBeforeMiddleware;
    use AtomPie\Web\Boundary\IChangeResponse;
    use AtomPie\Web\Connection\Http\Header\Status;

    /**
     * Responsible for:
     *  - running apps within middleware
     *  - handling errors
     *
     * Class Kernel
     * @package Workshop
     */
    class Kernel
    {

        use EventHandler;

        const EVENT_APPLICATION_BOOT = '@ApplicationBoot';
        const EVENT_KERNEL_BOOT = '@KernelBoot';

        /**
         * @var array
         */
        private $aMiddleWares;

        /**
         * @var IAmFrameworkConfig
         */
        private $oConfig;

        /**
         * @var ClassLoader
         */
        private $oClassLoader;

        /**
         * @var Bootstrap
         */
        private $oBootstrap;

        /**
         * @var IRoute
         */
        private $oRouter;

        public function __construct(ClassLoader $oClassLoader = null) {
            $this->oClassLoader = $oClassLoader;
        }

        /**
         * @param IRoute $oRouter
         */
        public function setRouter(IRoute $oRouter) {
            $this->oRouter = $oRouter;
        }
        
        /**
         * @param IAmFrameworkConfig $oConfig
         * @param IAmContext $oContext
         * @return IChangeResponse|Response
         */
        public function boot(IAmFrameworkConfig $oConfig, IAmContext $oContext = null) {

            try {

                if(isset($this->oRouter)) {
                    // TODO remove
                    $_SERVER['REQUEST_URI'] = str_replace('/vagrant/atompie', '', $_SERVER['REQUEST_URI']);

                    $aNewRequest = $this->oRouter->rewrite($_SERVER);
                    if($aNewRequest !== null) {
                        $_REQUEST = $aNewRequest;
                    }
                }
                
                $oDispatchManifest = DispatchManifest::factory($oConfig, $_REQUEST);

                // Context validates client constrains
                if($oContext == null) {
                    // Caution it mutates Environment
                    // Environment is decapsulated
                    // DO NOT USE Environment before this point
                    $oContext = new Context($oDispatchManifest->getEndPoint(), $oConfig);
                }

                $oConfig->lock();

                $this->oConfig = $oConfig;

                $oEnvironment = $oContext->getEnvironment();
                $oRequest = $oEnvironment->getRequest();
                $oResponse = $oEnvironment->getResponse();

                ///////////////////////////////////////
                // Remove dispatch params from request

                $oRequest->removeParam(EndPointImmutable::END_POINT_QUERY);
                $oRequest->removeParam(EventSpecImmutable::EVENT_QUERY);

                $this->aMiddleWares = $oConfig->getMiddleware();
                $this->oBootstrap = new Bootstrap($oConfig, $oContext);

                $this->triggerEvent(self::EVENT_KERNEL_BOOT, $this);

                $this->runBefore(
                    $oRequest,
                    $oResponse
                );

                $oCustomDependencyContainer = null;
                $oApplication = $this->oBootstrap->initApplication(
                    $oDispatchManifest,
                    $oCustomDependencyContainer,
                    $oConfig->getContentProcessors()
                );

                $this->triggerEvent(self::EVENT_APPLICATION_BOOT, $oApplication);

                $oResponse = $oApplication->run($oConfig, $oContext);
                
            } catch (\Exception $oException) {

                // Response content-type may be modified by construction of Context
                // If context is constructed correctly then content-typ may change.
                $oResponse = Environment::getInstance()->getResponse();

                $oResponse = $this->getResponseForException($oConfig->getErrorHandler(), $oException, $oResponse);

            } finally {

                if(isset($oResponse)) {
                    $this->runAfter($oResponse);
                }
            }

            return $oResponse;
        }

        /**
         * @param $oRequest
         * @param $oResponse
         * @throws Exception
         */
        private function runBefore($oRequest, $oResponse)
        {

            if (null !== $this->aMiddleWares && !empty($this->aMiddleWares)) {

                foreach ($this->aMiddleWares as $oMiddleWare) {
                    if ($oMiddleWare instanceof IRunBeforeMiddleware) {
                        $oMiddleWare->before($oRequest, $oResponse);
                    }
                }

            }
        }

        /**
         * @param $oResponse
         * @throws Exception
         */
        private function runAfter($oResponse)
        {
            if (null !== $this->aMiddleWares && !empty($this->aMiddleWares)) {
                foreach ($this->aMiddleWares as $oMiddleWare) {
                    if ($oMiddleWare instanceof IRunAfterMiddleware) {
                        $oMiddleWare->after($oResponse);
                    }
                }
            }
        }

        /**
         * @param IHandleException $oErrorRenderer
         * @param $oException
         * @param $oResponse
         * @return IChangeResponse
         */
        private function getResponseForException(IHandleException $oErrorRenderer = null, $oException, $oResponse)
        {
            if ($oErrorRenderer == null) {
                // Default value
                $oErrorRenderer = new DefaultErrorHandler(true);
            }

            $oResponse = $this->handleError(
                $oException,
                $oErrorRenderer,
                $oResponse
            );

            return $oResponse;
        }

        /**
         * @param \Exception $oException
         * @param \AtomPie\Boundary\System\IHandleException $oErrorRenderer
         * @param Response $oResponse
         * @return IChangeResponse
         */
        private function handleError(\Exception $oException, IHandleException $oErrorRenderer, Response $oResponse)
        {

            $iStatusCode = $oException->getCode();

            if ($iStatusCode == Status::UNAUTHORIZED) {
                $oResponse->addHeader('WWW-Authenticate', 'Basic realm="Secured Area"');
                $oResponse->setStatus(new Status(Status::UNAUTHORIZED));
            } else {
                if ($iStatusCode !== 0 && Status::isValidStatusCode($iStatusCode)) {
                    $oResponse->setStatus(new Status($iStatusCode));
                } else {
                    $oResponse->setStatus(new Status(Status::INTERNAL_SERVER_ERROR));
                }
            }

            $oResponse
                ->getContent()
                ->setContent(
                    $oErrorRenderer->handleException(
                        $oException,
                        $oResponse->getContent()->getContentType()
                    )
                );

            return $oResponse;
        }

    }

}
