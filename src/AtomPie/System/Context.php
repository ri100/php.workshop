<?php
namespace AtomPie\System {

    use AtomPie\Boundary\Core\Dispatch\IAmEndPointValue;
    use AtomPie\Boundary\Core\IAmFrameworkConfig;
    use AtomPie\Boundary\System\IAmContext;
    use AtomPie\DependencyInjection\ContractFillers;
    use AtomPie\Service\Object\DeEncapsulator;
    use AtomPie\System\Dispatch\ClientConstrain;
    use AtomPie\System\Dispatch\ClientValidator;
    use AtomPie\System\Dispatch\DispatchException;
    use AtomPie\System\Dispatch\EndPointClass;
    use AtomPie\Web\Boundary\IAmEnvironment;
    use AtomPie\Web\Connection\Http\Header;
    use AtomPie\I18n;

    class Context implements IAmContext
    {
        /**
         * @var EndPoint
         */
        private $oEndPoint;

        /**
         * @var EndPointClass
         */
        private $sEndPointClass;

        /**
         * @var array
         */
        private $aContractFillers = [];
        
        /**
         * @var IAmEnvironment
         */
        private $oEnvironment;

        /**
         * @var ClientConstrain
         */
        private $oClientConstrain;

        /**
         * @var \Closure
         */
        private $oOnReturnClosure;

        /**
         * Context constructor.
         * @param IAmEndPointValue $oEndPointSpec
         * @param IAmFrameworkConfig $oConfig
         * @throws DispatchException
         */
        public function __construct(IAmEndPointValue $oEndPointSpec, IAmFrameworkConfig $oConfig)
        {
            $this->oClientConstrain = new ClientConstrain();
            $this->oEndPoint = new EndPoint();
            $this->oEnvironment = $this->init(
                $oEndPointSpec,
                [$oConfig->getEndPointDir()]
            );;
        }

        private function getFile($aPaths, $sEndPointName, $sEndPointMethod) {

            $aEndPointFiles = [];

            $sGlobalEndPointDeclaration = '__global_context__.php';

            // First path is checked for global endpoint.
            $sGlobalPath = current($aPaths);
            $sGlobalEndPointDeclaration = $sGlobalPath.DIRECTORY_SEPARATOR.$sGlobalEndPointDeclaration;
            if(is_file($sGlobalEndPointDeclaration)) {
                $aEndPointFiles[] = $sGlobalEndPointDeclaration;
            }

            foreach ($aPaths as $sFilePath) {

                $sEndPointFilePath =  $sFilePath.DIRECTORY_SEPARATOR.str_replace('\\',DIRECTORY_SEPARATOR,$sEndPointName).'.php';
                $sEndPointMethodFilePath =  $sFilePath.DIRECTORY_SEPARATOR.str_replace('\\',DIRECTORY_SEPARATOR,$sEndPointName).'.'.$sEndPointMethod.'.php';

                $bHasEndPointFilePath = is_file($sEndPointFilePath);
                $bHasEndPointMethodFilePath = is_file($sEndPointMethodFilePath);

                // Order matters

                if($bHasEndPointFilePath) {
                    $aEndPointFiles[] = $sEndPointFilePath;
                }

                if($bHasEndPointMethodFilePath) {
                    $aEndPointFiles[] = $sEndPointMethodFilePath;
                }
            }
            
            return empty($aEndPointFiles)
                ? null
                : $aEndPointFiles;
        }

        /**
         * @param IAmEndPointValue $oEndPointSpec
         * @param array $aPaths
         * @return IAmEnvironment
         * @throws DispatchException
         * @throws I18n\Exception
         */
        private function init(IAmEndPointValue $oEndPointSpec, array $aPaths = [])
        {

            $sEndPointName = $oEndPointSpec->getEndPointString();
            $sEndPointMethod = $oEndPointSpec->getMethodString();

            $aFilesToLoad = $this->getFile($aPaths, $sEndPointName, $sEndPointMethod);

            if(null === $aFilesToLoad) {

                throw new DispatchException(
                    I18n\Label(
                        'Could not load %s.php or %s.%s.php EndPoint declaration from [%s] folders. Method [%s] in endpoint [%s] can not be invoked. Please check if endpoint [%s.php] is declared in [%s]',
                        $sEndPointName,
                        $sEndPointName,
                        $sEndPointMethod,
                        implode(',',$aPaths),
                        $oEndPointSpec->getMethodString(),
                        $oEndPointSpec->getEndPointString(),
                        $sEndPointName,
                        implode(',',$aPaths)
                    ),
                    Header\Status::NOT_FOUND
                );

            }

            foreach ($aFilesToLoad as $sEndPointFilePath) {

                /** @noinspection PhpIncludeInspection */
                $aEndPointContext = require $sEndPointFilePath;

                if (!$aEndPointContext instanceof \Closure) {
                    throw new DispatchException(\AtomPie\I18n\Label('Endpoint declaration must return closure.'));
                }

                $aEndPointContext($this->oEndPoint, $this->oClientConstrain);

            }

            $pClosure = ($this->oEndPoint);
            $oEnvironment = Environment::getInstance();
            $oOpenEnvironment = new DeEncapsulator($oEnvironment);

            list(
                $this->sEndPointClass,
                $oOpenEnvironment->oRequest,
                $oOpenEnvironment->oResponse,
                $this->aContractFillers,
                $oOpenEnvironment->oSession,
                $oOpenEnvironment->oServer,
                $oOpenEnvironment->oEnvVariables,
                $this->oOnReturnClosure
                ) = $pClosure();

            if(empty($this->sEndPointClass)) {
                throw new DispatchException(\AtomPie\I18n\Label('Please define EndPoint class as $endpoint->setClassName(CLASS_NAME).'));
            }

            ////////////////////////////////
            // Validate client constrains

            $oClientValidator = new ClientValidator($this->oClientConstrain);
            if(!$oClientValidator->validate($oEnvironment->getRequest())) {
                throw $oClientValidator->getError();
            }

            return $oEnvironment;

        }

        /**
         * @return ContractFillers
         */
        public function getContractFillers()
        {
            return new ContractFillers($this->aContractFillers);
        }

        /**
         * @return EndPointClass
         */
        public function getEndPointClass()
        {
            return $this->sEndPointClass;
        }

        /**
         * @return IAmEnvironment
         */
        public function getEnvironment()
        {
            return $this->oEnvironment;
        }

        /**
         * @return \Closure
         */
        public function getOnReturn()
        {
            return $this->oOnReturnClosure;
        }

    }

}
