<?php
namespace AtomPie\System;

class ClassShortcuts
{
    /**
     * @var Namespaces
     */
    private $aEventNamespaces;
    /**
     * @var Namespaces
     */
    private $aEventClasses;

    public function __construct(Namespaces $aEventNamespaces = null, Namespaces $aEventClasses = null)
    {
        $this->aEventNamespaces = ($aEventNamespaces !== null) ? $aEventNamespaces : new Namespaces();
        $this->aEventClasses = ($aEventClasses !==null) ? $aEventClasses : new Namespaces();
    }

    /**
     * @return Namespaces
     */
    public function getNamespaces()
    {
        return $this->aEventNamespaces;
    }

    /**
     * @return Namespaces
     */
    public function getClasses()
    {
        return $this->aEventClasses;
    }
}