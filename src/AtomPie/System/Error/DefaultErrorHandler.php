<?php
namespace AtomPie\System\Error {

    use AtomPie\Boundary\System\IHandleException;
    use AtomPie\Web\Boundary\IChangeResponse;
    use AtomPie\Web\Boundary\IRecognizeMediaType;
    use AtomPie\Web\Connection\Http\Header\Status;
    use AtomPie\Web\Connection\Http\Response;

    class DefaultErrorHandler implements IHandleException
    {

        /**
         * @var bool
         */
        private $bWithStackTrace;

        public function __construct($bWithStackTrace = true)
        {
            $this->bWithStackTrace = $bWithStackTrace;
        }

        public function handleException(\Exception $oException, IRecognizeMediaType $oContentType)
        {
            $oExceptionMessage = new DefaultErrorRenderer($oContentType);
            $oExceptionMessage->ErrorMessage = $oException->getMessage();

            $oExceptionMessage->Line = $oException->getLine();
            $oExceptionMessage->File = $oException->getFile();
            $oExceptionMessage->Code = $oException->getCode();

            if ($this->bWithStackTrace) {
                $oExceptionMessage->StackTrace = explode("\n", $oException->getTraceAsString());
                $oExceptionMessage->ExceptionType = get_class($oException);
                $oInternalException = $oException->getPrevious();
                $oExceptionMessage->InternalException = isset($oInternalException)
                    ? $oInternalException
                    : 'none';
            } else {
                $oExceptionMessage->StackTrace = '';
                $oExceptionMessage->ExceptionType = get_class($oException);
                $oExceptionMessage->InternalException = '';
            }

            return $oExceptionMessage->__toString();
        }

        /**
         * @param IHandleException $oErrorRenderer
         * @param $oException
         * @param $oResponse
         * @return IChangeResponse
         */
        public static function getResponseForException(IHandleException $oErrorRenderer = null, $oException, $oResponse)
        {
            if ($oErrorRenderer == null) {
                // Default value
                $oErrorRenderer = new DefaultErrorHandler(true);
            }

            $oResponse = self::handleError(
                $oException,
                $oErrorRenderer,
                $oResponse
            );

            return $oResponse;
        }

        /**
         * @param \Exception $oException
         * @param \AtomPie\Boundary\System\IHandleException $oErrorRenderer
         * @param Response $oResponse
         * @return IChangeResponse
         */
        private static function handleError(\Exception $oException, IHandleException $oErrorRenderer, Response $oResponse)
        {

            $iStatusCode = $oException->getCode();

            if ($iStatusCode == Status::UNAUTHORIZED) {
                $oResponse->addHeader('WWW-Authenticate', 'Basic realm="Secured Area"');
                $oResponse->setStatus(new Status(Status::UNAUTHORIZED));
            } else {
                if ($iStatusCode !== 0 && Status::isValidStatusCode($iStatusCode)) {
                    $oResponse->setStatus(new Status($iStatusCode));
                } else {
                    $oResponse->setStatus(new Status(Status::INTERNAL_SERVER_ERROR));
                }
            }

            $oResponse
                ->getContent()
                ->setContent(
                    $oErrorRenderer->handleException(
                        $oException,
                        $oResponse->getContent()->getContentType()
                    )
                );

            return $oResponse;
        }

    }

}
