<?php
namespace AtomPie\System;

use AtomPie\Web\Boundary\IAmEnvironment;
use AtomPie\Web\Connection\Http\Header;
use AtomPie\Web\Connection\Http\Header\Accept;
use AtomPie\Web\Connection\Http\Request;
use AtomPie\Web\Connection\Http\Response;
use AtomPie\Web\EnvVariable;
use AtomPie\Web\Server;
use AtomPie\Web\Session;

class Environment implements IAmEnvironment
{
    /**
     * @var $this
     */
    private static $oInstance;
    /**
     * @var Server
     */
    protected $oServer;

    /**
     * @var Request
     */
    protected $oRequest;

    /**
     * @var Session
     */
    protected $oSession;

    /**
     * @var Response
     */
    protected $oResponse;

    /**
     * @var \AtomPie\Web\EnvVariable
     */
    protected $oEnvVariables;


    private function __construct()
    {
    }

    /**
     * @return Environment
     */
    public static function getInstance() {
        if(!isset(self::$oInstance)) {
            self::$oInstance = new self();
        }
        return self::$oInstance;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        if(!isset($this->oRequest)) {
            $this->oRequest = new Request();
            $this->oRequest->load();
        }
        return $this->oRequest;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        if (!isset($this->oResponse)) {
            $this->oResponse = new Response();
        }

        // Auto set content type based on Accept header
        $oRequest = $this->getRequest();
        
        // Change if there is default content-type header
        if ($this->hasDefaultContentTypeHeader() && $oRequest->hasHeader(Header::ACCEPT)) {
            $oAcceptHeader = $oRequest->getHeader(Header::ACCEPT);
            if ($oAcceptHeader instanceof Accept) {
                $this->oResponse->setContentTypeFromAcceptHeader($oAcceptHeader);
            }
        }

        return $this->oResponse;
    }

    protected function hasDefaultContentTypeHeader()
    {
        return !$this->oResponse->hasHeader(Header::CONTENT_TYPE) ||
        $this->oResponse->getHeader(Header::CONTENT_TYPE) instanceof Header\DefaultContentType;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        if(!isset($this->oSession)) {
            $this->oSession = Session::getInstance();
        }
        return $this->oSession;
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        if(!isset($this->oServer)) {
            $this->oServer = Server::getInstance();
        }
        return $this->oServer;
    }

    /**
     * @return \AtomPie\Web\EnvVariable
     */
    public function getEnv()
    {
        if(!isset($this->oEnvVariables)) {
            $this->oEnvVariables = EnvVariable::getInstance();
        }
        return $this->oEnvVariables;
    }
}