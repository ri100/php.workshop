<?php
namespace AtomPie\System;

class NoClass
{
    public function __call($name, $arguments) {
        return '';
    }
}