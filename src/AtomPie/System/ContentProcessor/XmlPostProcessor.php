<?php
namespace AtomPie\System\ContentProcessor;

class XmlPostProcessor
{
    /**
     * Returns NULL if could not convert.
     * 
     * @param $mContent
     * @return null|string
     */
    public function convertToXml($mContent)
    {
        if (is_string($mContent)) {
            $mContent = sprintf(
            /** @lang XML */
                ' <Root><![CDATA[%s]]></Root>', 
                $mContent);
        } else if (is_bool($mContent)) {
            $mContent = sprintf(/** @lang XML */
                '<Root>%s</Root>', (string)$mContent);
        } else if (is_scalar($mContent)) {
            $mContent = sprintf(/** @lang XML */
                '<Root>%s</Root>', $mContent);
        } else if (is_array($mContent)) {
            /** @noinspection CheckEmptyScriptTag */
            $mContent = $this->convertArrayToXml($mContent, new \SimpleXMLElement('<Root />'));
        } else if (is_object($mContent)) {
            /** @noinspection CheckEmptyScriptTag */
            $mContent = $this->convertObjectToXml($mContent,  new \SimpleXMLElement('<Root />'));
        } else {
            return null;
        }
        
        return $mContent;
    }

    private function convertArrayToXml($aArray, \SimpleXMLElement $oXml)
    {
        foreach ($aArray as $sName => $sValue) {
            if (is_numeric($sName) || is_numeric(substr($sName, 0, 1))) {
                $sName = (string)$sName;
                $sName = '_x' . sprintf("%04d", dechex(ord(substr($sName, 0, 1)))) . '_' . substr($sName, 1);
            }
            if (is_array($sValue)) {
                $this->convertArrayToXml($sValue, $oXml->addChild($sName));
            } else {
                if (is_object($sValue)) {
                    $this->convertObjectToXml($sValue, $oXml->addChild($sName));
                } else {
                    $oXml->addChild($sName, htmlspecialchars($sValue));
                }
            }
        }
        return $oXml->asXML();
    }

    private function convertObjectToXml($array, \SimpleXMLElement $oXml)
    {
        $aProperties = get_object_vars($array);
        foreach ($aProperties as $sName => $sValue) {
            if (is_object($sValue)) {
                $this->convertObjectToXml($sValue, $oXml->addChild($sName));
            } else {
                $oXml->addChild($sName, $sValue);
            }
        }
        return $oXml->asXML();
    }
}