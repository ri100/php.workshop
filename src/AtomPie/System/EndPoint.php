<?php
namespace AtomPie\System {

    use AtomPie\Boundary\System\IAmRequestValidator;
    use AtomPie\DependencyInjection\Contract;
    use AtomPie\System\Dispatch\EndPointClass;
    use AtomPie\System\Dispatch\RequestValidator;
    use AtomPie\Web\EnvVariable;
    use AtomPie\Web\Boundary\ISetUpResponse;
    use AtomPie\Web\Connection\Http\Header;
    use AtomPie\Web\Connection\Http\Request;
    use AtomPie\Web\Connection\Http\Response;
    use AtomPie\Web\Exception;
    use AtomPie\Web\Server;
    use AtomPie\Web\Session;

    class EndPoint
    {
        private static $lock = false;

        /**
         * @var Request
         */
        private $oRequest;

        /**
         * @var Response
         */
        private $oResponse;

        /**
         * @var EndPointClass
         */
        private $oEndPointClass;

        /**
         * @var array
         */
        private $aContracts = [];

        /**
         * @var Session
         */
        private $oSession;

        /**
         * @var \AtomPie\Web\EnvVariable
         */
        private $oEnvVariables;

        /**
         * @var Server
         */
        private $oServer;

        /**
         * @var \Closure
         */
        private $oOnReturn;

        /**
         * EndPoint constructor.
         */
        public function __construct()
        {
            $this->oRequest = new Request();
            $this->oRequest->load();
            $this->oResponse = new Response();
            $this->oSession = Session::getInstance();
            $this->oEnvVariables = EnvVariable::getInstance();
            $this->oServer = Server::getInstance();
        }

        /**
         * @return Request
         */
        public function getRequest() {
            return $this->oRequest;
        }
        
        /**
         * @param $sClassName
         */
        public function setClassName($sClassName) {
            $this->oEndPointClass = new EndPointClass($sClassName);
        }

        private function hasDefaultContentTypeHeader() {
            return !$this->oResponse->hasHeader(Header::CONTENT_TYPE) ||
            $this->oResponse->getHeader(Header::CONTENT_TYPE) instanceof Header\DefaultContentType;
        }
        
        public function __invoke() {
            
            if($this->hasDefaultContentTypeHeader() && $this->oRequest->hasHeader(Header::ACCEPT)) {
                $oAcceptHeader = $this->oRequest->getHeader(Header::ACCEPT);
                if ($oAcceptHeader instanceof Header\Accept) {
                    try {
                        // Client can sent incorrect accept header
                        $this->oResponse->setContentTypeFromAcceptHeader($oAcceptHeader);
                    } catch (Exception $e) {
                        // Then fallback to default text/html
                        $this->oResponse->setContentType('text/html');
                    }
                }
            }
            
            return [
                $this->oEndPointClass,
                $this->oRequest,
                $this->oResponse,
                $this->aContracts,
                $this->oSession,
                $this->oServer,
                $this->oEnvVariables,
                $this->oOnReturn
            ];
        }

        /**
         * @return ISetUpResponse
         */
        public function onResponse()
        {
            return $this->oResponse;
        }

        /**
         * @return IAmRequestValidator
         */
        public function onRequest()
        {
            return new RequestValidator($this->oRequest);
        }

        /**
         * @param $sContractClass
         * @return Contract
         */
        public function onContract($sContractClass)
        {
            $oContract = new Contract($sContractClass);
            $this->aContracts[] = $oContract;
            return $oContract;
        }

        public function onReturn(\Closure $oOnReturn) {
            $this->oOnReturn = $oOnReturn;
        }

        public function lock()
        {
            self::$lock = true;
        }

        /**
         * @return Session
         */
        public function onSession()
        {
            return $this->oSession;
        }

    }

}
