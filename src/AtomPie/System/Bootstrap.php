<?php
namespace AtomPie\System {

    use AtomPie\Boundary\Core\Dispatch\IAmDispatchManifest;
    use AtomPie\Boundary\Core\IAmFrameworkConfig;
    use AtomPie\Boundary\Core\ISetUpContentProcessor;
    use AtomPie\Boundary\System\IAmContext;
    use AtomPie\DependencyInjection\DependencyContainer;
    use AtomPie\EventBus\Exception;

    /**
     * Responsible for:
     *  - init of app
     */
    class Bootstrap
    {

        /**
         * @var IAmContext
         */
        private $oContext;

        /**
         * @var IAmFrameworkConfig
         */
        private $oConfig;

        public function __construct(
            IAmFrameworkConfig $oConfig,
            IAmContext $oContext
        ) {
            $this->oContext = $oContext;
            $this->oConfig = $oConfig;
        }

        /**
         * Init application. During application init Exception may be thrown.
         *
         * Pass custom dependency injection container.
         * Standard dependency containers are already defined.
         *
         *
         * @param IAmDispatchManifest $oDispatchManifest
         * @param DependencyContainer $oCustomDependencyContainer
         * @param ISetUpContentProcessor[] $aContentProcessors
         * @return Application
         * @throws Exception
         */
        public function initApplication(
            IAmDispatchManifest $oDispatchManifest,
            DependencyContainer $oCustomDependencyContainer = null,
            array $aContentProcessors = []
        ) {

            $oSystemInit = new Init();

            ///////////////////////////////////
            // Sets dependency injections, etc

            $oApplication = $oSystemInit->initApplication(
                $this->oContext,
                $this->oConfig,
                $oDispatchManifest,
                $aContentProcessors);

            if (isset($oCustomDependencyContainer)) {
                $oApplication
                    ->getGlobalDependencyContainer()
                    ->merge($oCustomDependencyContainer);
            }

            return $oApplication;
        }

    }

}
