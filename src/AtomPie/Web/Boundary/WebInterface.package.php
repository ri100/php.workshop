<?php
namespace AtomPie\Web\Boundary;

use AtomPie\Web\Connection\Http\UploadFile;
use AtomPie\Web\Connection\ProxyCredentials;
use AtomPie\Web\Connection\Http\Url;
use AtomPie\Web\Exception;
use AtomPie\Web\CookieJar;
use AtomPie\Web\Cookie;
use Generi\Boundary\IAmKeyValueStore;

interface IAmHttpCommunication
{

    /**
     * @return IChangeRequest
     */
    public function getRequest();

    /**
     * @return IChangeResponse
     */
    public function getResponse();
}

interface IAmEnvironment
{

    /**
     * @return IChangeRequest | IAmRequest
     */
    public function getRequest();

    /**
     * @return IChangeResponse | IAmResponse
     */
    public function getResponse();

    /**
     * @return IAmEnvVariable
     */
    public function getEnv();

    /**
     * @return IAmSession
     */
    public function getSession();

    /**
     * @return IAmServer
     */
    public function getServer();

}

interface IRecognizeMediaType
{

    /**
     * Returns true if Content-Type header indicates that
     * it has Json encoded content.
     *
     * Also check if content can be json_decoded, as Content-Type
     * header is only information on content type and can be
     * wrong if not implemented correctly.
     *
     * @return boolean
     */
    public function isJson();

    /**
     * Returns true if Content-Type header indicates that
     * it has Xml content.
     *
     * @return boolean
     */
    public function isXml();

    /**
     * Returns true if Content-Type header indicates that
     * it has Text content.
     *
     * @return boolean
     */
    public function isText();

    /**
     * Returns true if Content-Type header indicates that
     * it has Javascript content.
     *
     * @return bool
     */
    public function isJavascript();

    /**
     * Returns true if Content-Type header indicates that
     * it has Html content.
     *
     * @return boolean
     */
    public function isHtml();

    /**
     * Returns true if Content-Type header indicates that
     * it has Xml content.
     *
     * @return boolean
     */
    public function isUrlEncoded();

}

interface IAmHeader
{

    /**
     * Returns a name of a header, e.g. Location, Date, Accept, etc.
     * See http://en.wikipedia.org/wiki/List_of_HTTP_header_fields for header filed names.
     *
     * @return string $sName
     */
    public function getName();

    /**
     * Returns value of a header.
     *
     * @return string $sValue
     */
    public function getValue();

    /**
     * Returns header as in request or response.
     *
     * @return string
     */
    public function __toString();

    /**
     * Send a raw HTTP header
     * @link http://www.php.net/manual/en/function.header.php
     * @param string string <p>
     * The header string.
     * </p>
     * <p>
     * There are two special-case header calls. The first is a header
     * that starts with the string "HTTP/" (case is not
     * significant), which will be used to figure out the HTTP status
     * code to send. For example, if you have configured Apache to
     * use a PHP script to handle requests for missing files (using
     * the ErrorDocument directive), you may want to
     * make sure that your script generates the proper status code.
     * </p>
     * <p>
     * ]]>
     * </p>
     * <p>
     * The second special case is the "Location:" header. Not only does
     * it send this header back to the browser, but it also returns a
     * REDIRECT (302) status code to the browser
     * unless the 201 or
     * a 3xx status code has already been set.
     * </p>
     * <p>
     * ]]>
     * </p>
     * @param $bReplaceCurrent bool[optional] <p>
     * The optional replace parameter indicates
     * whether the header should replace a previous similar header, or
     * add a second header of the same type. By default it will replace,
     * but if you pass in false as the second argument you can force
     * multiple headers of the same type. For example:
     * </p>
     * <p>
     * ]]>
     * </p>
     * @param $iStatus int[optional] <p>
     * Forces the HTTP response code to the specified value.
     * </p>
     * @return void
     */
    public function send($bReplaceCurrent = null, $iStatus = null);

}

interface IAmStatusHeader extends IAmHeader
{

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return string
     */
    public function getVersion();

    /**
     * @param $iStatusCode
     * @return bool
     */
    public function is($iStatusCode);

    /**
     * @return bool
     */
    public function isServerError();

    /**
     * @return bool
     */
    public function isResourceError();

    /**
     * @return bool
     */
    public function isRedirect();

    /**
     * @return bool
     */
    public function isOk();
}

interface IAmHttpHeader extends IAmHeader
{
    /**
     * Sets value of a header.
     *
     * @param $sValue
     */
    public function setValue($sValue);

}

interface IAmContentType extends
    IRecognizeMediaType
    , IAmHttpHeader
{
    /**
     * @return string
     */
    public function getMediaType();

    public function getParam($sParamName);
}

interface IAmContent
{

    /**
     * @return bool
     */
    public function isFile();

    /**
     * Returns content as a string.
     *
     * @return string
     */
    public function __toString();

    /**
     * Content length.
     *
     * @return int $iContentLength
     */
    public function getContentLength();

    /**
     * Content type.
     *
     * @return IAmContentType
     */
    public function getContentType();

    /**
     * @return bool
     */
    public function hasContentType();

    /**
     * Return content
     *
     * @return string
     */
    public function get();

    /**
     * Returns true if content is not empty and false if empty.
     *
     * @return boolean
     */
    public function notEmpty();

    /**
     * Sets content.
     *
     * @param string $sContent
     * @param IAmContentType $oContentType
     */
    public function set($sContent, IAmContentType $oContentType = null);

    /**
     * @param $sContent
     */
    public function setContent($sContent);

    /**
     * @param IAmContentType $oContentType
     */
    public function setContentType(IAmContentType $oContentType);

    /**
     * Decodes json content.
     *
     * @param bool $bAssoc [optional]
     * When true, returned objects will be converted into
     * associative arrays.
     *
     * @return mixed the value encoded in json in appropriate
     * PHP type. Values true, false and
     * null (case-insensitive) are returned as true, false
     * and NULL respectively. NULL is returned if the
     * json cannot be decoded or if the encoded
     * data is deeper than the recursion limit.
     */
    public function decodeAsJson($bAssoc = null);

    /**
     * Returns the JSON representation of a value
     * @link http://www.php.net/manual/en/function.json-encode.php
     *
     * @param options int[optional] <p>
     * Bitmask consisting of JSON_HEX_QUOT,
     * JSON_HEX_TAG,
     * JSON_HEX_AMP,
     * JSON_HEX_APOS,
     * JSON_FORCE_OBJECT.
     * </p>
     * @return string a JSON encoded string on success.
     */
    public function encodeAsJson($iOptions = null);

    /**
     * @return \SimpleXMLElement
     */
    public function getAsSimpleXml();
}

interface IChangeParams extends IHaveParams
{
    /**
     * @param $sKey
     */
    public function removeParam($sKey);

    /**
     * @param string $sKey
     * @param string $sValue
     * @param null $sMethod
     */
    public function setParam($sKey, $sValue, $sMethod = null);

}

interface IChangeHeaders
{

    /**
     * @return $this
     */
    public function resetHeaders();

    /**
     * Adds header to request.
     *
     * @param string $sName
     * @param string | IAmHttpHeader $sValue
     * @return $this
     */
    public function addHeader($sName, $sValue);

    /**
     * @param string $sName
     * @return $this
     */
    public function removeHeader($sName);

}

interface IChangeTimeOut extends IHaveTimeOut
{
    /**
     * Sets request time out.
     *
     * @param int $iTimeOut
     */
    public function setTimeOut($iTimeOut);

}

interface IChangeUrl extends IHaveUrl
{
    /**
     * Sets \Web\Connection\Http\Url.
     *
     * @param Url $oHttpUrl
     */
    public function setUrl(Url $oHttpUrl);
}

interface IChangeHttpMethod extends IHaveHttpMethod
{

    /**
     * Sets method. use \Web\Connection\Http\Request::GET,
     *                    \Web\Connection\Http\Request::POST,
     *                    \Web\Connection\Http\Request::PUT,
     *                    \Web\Connection\Http\Request::DELETE
     *
     * @param string $sMethod
     */
    public function setMethod($sMethod);

}

interface IHaveHeaders
{

    /**
     * Returns TRUE if has header $sName.
     *
     * @param string $sName
     * @return bool
     */
    public function hasHeader($sName);


    /**
     * Returns $sName header.
     * Remember to populate header values with load method.
     *
     * @param string $sName
     * @return IAmHttpHeader | boolean
     */
    public function getHeader($sName);

    /**
     * Returns array of headers (Web\Connection\Http\Header class).
     *
     * @return IAmHttpHeader[]
     */
    public function getHeaders();

    /**
     * Returns TRUE if header collection is not empty.
     *
     * @return bool
     */
    public function hasHeaders();
}


interface IHaveUrl
{
    /**
     * Returns TRUE if request has set URL, FALSE if oposit.
     *
     * @return bool
     */
    public function hasUrl();

    /**
     * Returns \Web\Connection\Http\Url.
     *
     * @return Url $oHttpUrl
     */
    public function getUrl();
}

interface IHaveHttpMethod
{

    /**
     * Returns method.
     *
     * @return string
     */
    public function getMethod();

    /**
     * Returns TRUE if method is set, false if oposit.
     *
     * @param string $sMethod use
     *                    \Web\Connection\Http\Request::GET,
     *                    \Web\Connection\Http\Request::POST,
     *                    \Web\Connection\Http\Request::PUT,
     *                    \Web\Connection\Http\Request::DELETE
     *                    instead of string
     *
     * @return boolean
     */
    public function isMethod($sMethod);

}

interface IHaveTimeOut
{
    /**
     * Returns time-out.
     *
     * @return int
     */
    public function getTimeOut();
}

interface IHaveParams
{

    /**
     * @param null $sMethod
     * @return array | null
     */
    public function getAllParams($sMethod = null);

    /**
     * Returns request parameter.
     *
     * @param string $sName
     * @param null $sMethod
     * @return string | null
     */
    public function getParam($sName, $sMethod = null);

    /**
     * @param string $sName
     * @param string $sMethod
     * @return bool
     */
    public function hasParam($sName, $sMethod = null);

}

interface IHaveContent
{

    /**
     * Returns content.
     *
     * @return IAmContent
     */
    public function getContent();

    /**
     * Returns TRUE if HTTP communication has content.
     *
     * @return bool
     */
    public function hasContent();

}


interface IChangeContent extends IHaveContent
{
    /**
     * Sets content.
     *
     * @param IAmContent $oContent
     */
    public function setContent(IAmContent $oContent);

    public function setContentType($sContentType);

}

interface IAmRequest extends
    IHaveHttpMethod,
    IHaveUrl,
    IHaveTimeOut,
    IHaveParams,
    IHaveHeaders,
    IHaveContent,
    IHaveCookies
{

    /**
     * Returns true if request is ajax or false if it is not.
     *
     * @return bool
     */
    public function isAjax();

    /**
     * Loads data from received request.
     *
     * @return $this
     */
    public function load();

    /**
     * @param $sVariableName
     * @return array|mixed|null
     * @throws \AtomPie\Web\Exception
     */
    public function getParamWithFallbackToBody($sVariableName);
}

interface IChangeRequest extends
    IChangeHttpMethod,
    IChangeUrl,
    IChangeHeaders,
    IChangeCookies,
    IHaveHeaders,
    IChangeTimeOut,
    IChangeParams,
    IAmRequest
{

    ////////////////////////
    // I Handle referrer

    /**
     * @return string
     */
    public function getReferrerUrl();

    /**
     * @param string $sReferrerUrl
     */
    public function setReferrerUrl($sReferrerUrl);

    /**
     * @return bool
     */
    public function hasReferrerUrl();

    ////////////////////////
    // I Handle proxy

    public function removeProxy();

    /**
     * @return bool
     */
    public function hasProxy();

    /**
     * @return bool
     */
    public function hasProxyCredentials();

    /**
     * @return ProxyCredentials
     */
    public function getProxyCredentials();

    /**
     * @return string
     */
    public function getProxy();

    /**
     * @param string $sProxy
     * @param ProxyCredentials $oCredentials
     * @return $this
     */
    public function setProxy($sProxy, ProxyCredentials $oCredentials = null);

    ////////////////////////
    // I Handle files

    /**
     * @param $sParamName
     * @return bool
     */
    public function hasUploadedFile($sParamName);

    /**
     * @param $sParamName
     * @return UploadFile
     */
    public function getFile($sParamName);

    ////////////////////////
    //

    /**
     * Loads data from received request.
     *
     * @return $this
     */
    public function load();

    public function send($sUrl = null, $aParams = null);

}

interface IAmSession extends IAmKeyValueStore, \Countable
{

    // TODO add constructor to interface

    public function mergeKeyValue($sKey, $mValue);

    /**
     * @return bool
     */
    public function isEmpty();

    /**
     * @return array
     */
    public function getAll();

}

interface IAmServer
{

    public function getServerUri();

    public function getServerUrl($sUrl = null, $bWithPort = true);

    public function getServer($bWithPort = true);

    public function getServerName();

    public function getServerPhpFolder();

    /**
     * @return string | null
     */
    public function getSelfPhpFile();

    /**
     * @return string
     */
    public function getSelfPhpFolder();

    public function getRequestUri();

    public function getDocumentRoot();

    public function getContextDocumentRoot();

    public function getScriptName();

    public function getScriptFileName();

    public function getServerAdmin();

    public function getRequestScheme();

    public function getProtocol();

    public function getRemotePort();

    public function getRequestMethod();

    public function getQueryString();

    public function getIp();

    public function isLocalHost();

    /**
     * @return bool
     */
    public function isHttps();

    /**
     * @return mixed
     */
    public function getHost();

    /**
     * @return mixed
     */
    public function getPort();
}

interface IUploadFile
{
    /**
     * Checks whether the uploaded file is valid.
     *
     * @param null $aValidFileFormats
     * @param null $iMaxFileSize
     * @throws Exception
     */
    public function isValid($aValidFileFormats = null, $iMaxFileSize = null);

    /**
     * Moves file to destination.
     *
     * @param $sDestinationFileName
     * @throws Exception
     */
    public function move($sDestinationFileName);

    public function getMime();

    public function getParamName();

    public function getTempName();

    public function getFileSize();
}

interface IAmRequestParam
{

    public function __construct($sName, $sValue = null);

    /**
     * @return string
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @return bool
     */
    public function isArray();

    /**
     * @return bool
     */
    public function isNull();

    /**
     * @return bool
     */
    public function isEmpty();
}

interface IPersistParamState
{

    /**
     * Loads state from session.
     *
     * @param $sParamName
     * @param string $sContext
     * @return mixed|null
     */
    public function loadState($sParamName, $sContext = 'global-context');

    /**
     * Saves state to session.
     *
     * @param IAmRequestParam $oParam
     * @param null $sAs
     * @param string $sContext
     * @return
     */
    public function saveState(IAmRequestParam $oParam, $sAs = null, $sContext = 'global-context');

    /**
     * Removes param value from session.
     *
     * If you set $bRemoveAllValuesRegardlessContext to TRUE
     * all param values will be removed regardless the context.
     *
     * So if there is param A with 10 values for different components
     * all values will be removed.
     *
     * @param $sParamName
     * @param bool $bRemoveAllValuesRegardlessContext
     * @param string $sContext
     */
    public function removeState($sParamName, $bRemoveAllValuesRegardlessContext = false, $sContext = 'global-context');

    /**
     * @param $sParamName
     * @return bool
     */
    public function hasState($sParamName);
}

interface IHaveStatusHeader
{
    /**
     * Returns status code.
     *
     * @return IAmStatusHeader
     */
    public function getStatus();

}

interface IChangeStatusHeader
{
    /**
     * Sets status code.
     *
     * @param IAmStatusHeader $oStatus
     * @return $this
     */
    public function setStatus(IAmStatusHeader $oStatus);
}

interface IHaveCookies
{

    /**
     * @return CookieJar
     */
    public function getCookies();

    /**
     * @return bool
     */
    public function hasCookies();


}

interface IChangeCookies
{
    /**
     * @param Cookie $oCookie
     * @return $this
     */
    public function addCookie(Cookie $oCookie);

    /**
     * @param \AtomPie\Web\CookieJar $oMyCookieJar
     * @return $this
     */
    public function appendCookieJar(CookieJar $oMyCookieJar);
}

interface ISetUpResponse extends
    IChangeCookies,
    IChangeHeaders,
    IChangeStatusHeader {
}

interface IChangeResponse extends
    IChangeStatusHeader,
    IHaveStatusHeader,
    IChangeCookies,
    IHaveCookies,
    IChangeHeaders,
    IHaveHeaders,
    IChangeContent
{

    /**
     * Sends response.
     */
    public function send();

}

interface IAmResponse extends IHaveContent, IHaveStatusHeader, IHaveHeaders
{

}

interface IAmImmutableHttpData
    extends IHaveContent,
    IHaveCookies,
    IHaveHeaders
{

    /**
     * @return string $sRequestString
     */
    public function getRequestString();

}


