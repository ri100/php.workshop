<?php
namespace AtomPie\Web\Connection\Http {

    use AtomPie\Web\Exception;

    class TimeOutException extends Exception
    {
    }
}