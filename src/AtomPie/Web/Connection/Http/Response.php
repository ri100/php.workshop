<?php
namespace AtomPie\Web\Connection\Http {

    use AtomPie\Web\Boundary\IAmContent;
    use AtomPie\Web\Boundary\IAmHeader;
    use AtomPie\Web\Boundary\IAmStatusHeader;
    use AtomPie\Web\Boundary\IChangeResponse;
    use AtomPie\Web\Connection\Http\Header\ContentType;
    use AtomPie\Web\Cookie;
    use AtomPie\Web\CookieJar;

    /**
     * Response class can be used to send response to the client request or
     * receive response if the the server send request to other system.
     *
     *
     * Example 1: Send request and receive response.
     *
     *  <pre>
     *  // Define url
     *  $oUrl = new \Web\Connection\Http\Url();
     *  $oUrl->setUrl('http://localhost/cont/action');
     *
     *  // Create request
     *    $request = new \Web\Connection\Http\Request();
     *  // Set method if not default GET
     *    $request->hasMethod('post');
     *  // Set url
     *  $request->setUrl($oUrl);
     *  // Add additional headers
     *  $request->addHeader('Accept',\Web\Connection\Http\ContentType::JSON);
     *  // Add content (in this example JSON encoded content)
     *  $request->setJsonContent('{id:1}');
     *
     *  $request->addHeader('Warning', '001');
     *  // Api key
     *  $request->addHeader('Content-MD5', 'qwerty');
     *
     *  // Send - returns response object.
     *  $response = $request->send();
     *
     *  // Prints response status
     *  echo $response->getStatus();
     *  </pre>
     *
     * Example 2: Receive request and send response.
     *
     * <pre>
     *  // Receive request
     *  $request =  new \Web\Connection\Http\Request();
     *  $request->load();
     *  echo $request->getHeader('Accept');
     *
     *  // Process
     *
     *  // Send response
     *  $response =  new \Web\Connection\Http\Response(\Web\Connection\Http\Header\Status::OK);
     *  $response->setContent(new \Web\Connection\Http\Content('My response', new \Web\Connection\Http\ContentType('text/plain')));
     *  $response->addHeader('Custom', 'MyHeader');
     *  $response->send();
     *  </pre>
     */
    class Response extends ImmutableResponse implements IChangeResponse
    {

        /**
         * Sets status code.
         *
         * @param IAmStatusHeader $oStatus
         * @return $this
         */
        public final function setStatus(IAmStatusHeader $oStatus)
        {
            $this->oStatus = $oStatus;
            return $this;
        }

        /**
         * @param $sContentType
         * @return $this
         */
        public function setContentType($sContentType)
        {
            $this->getContent()->setContentType(new ContentType($sContentType));
            $this->__addHeader(Header::CONTENT_TYPE, $sContentType);
            return $this;
        }

        /**
         * @param Header\Accept $oAcceptHeader
         * @return Response
         */
        public function setContentTypeFromAcceptHeader(Header\Accept $oAcceptHeader)
        {
            $aISendContentTypesByPriority = array(
                Header\ContentType::HTML,
                Header\ContentType::HTML . ';charset=utf-8',
                Header\ContentType::JSON,
                Header\ContentType::JSON . ';charset=utf-8',
                Header\ContentType::XML,
                Header\ContentType::XML . ';charset=utf-8'
            );

            $sContentType = $this->getAcceptableContentType($oAcceptHeader, $aISendContentTypesByPriority);
            if ($sContentType !== null && $sContentType !== '*/*') {
                // addHeader sets also ContentType.
                $this->addHeader(Header::CONTENT_TYPE, $sContentType);
            }

            return $this;
        }

        /**
         * @param Header\Accept $oAcceptHeader
         * @param $aAvailableResponseContentTypesByPriority
         * @return null
         */
        private function getAcceptableContentType($oAcceptHeader, $aAvailableResponseContentTypesByPriority)
        {
            $aMimeTypes = $oAcceptHeader->getMediaTypesByPriority();
            foreach ($aMimeTypes as $oMimeType) {
                foreach ($aAvailableResponseContentTypesByPriority as $sContentType) {
                    if ($oMimeType->willYouAccept($sContentType)) {
                        return $oMimeType->getMedia();
                    }
                }
            }
            return null;
        }

        ////////////////////////
        // Cookie

        /**
         * @param \AtomPie\Web\CookieJar $oMyCookieJar
         * @return $this
         */
        public function appendCookieJar(CookieJar $oMyCookieJar)
        {
            $oCookieJar = $this->getCookies();
            foreach ($oMyCookieJar->getAll() as $oCookie) {
                /* @var $oCookie Cookie */
                $oCookieJar->add($oCookie);
            }
            return $this;
        }

        /**
         * @param Cookie $oCookie
         * @return $this
         */
        public function addCookie(Cookie $oCookie)
        {
            CookieJar::getInstance()->add($oCookie);
            return $this;
        }

        /**
         * Adds header to request.
         *
         * @param string $sName
         * @param string | Header $sValue
         * @return $this
         */
        public final function addHeader($sName, $sValue)
        {
            if(strcasecmp($sName, Header::CONTENT_TYPE) == 0) {
                if ($sValue instanceof IAmHeader) {
                    $sValue = $sValue->getValue();
                }
                $this->setContentType($sValue);
            }
            $this->__addHeader($sName, $sValue);
            return $this;
        }

        /**
         * @return $this
         */
        public final function resetHeaders()
        {
            $this->aHeaders = array();
            return $this;
        }

        /**
         * @param string $sName
         * @return $this
         */
        public final function removeHeader($sName)
        {
            $this->__removeHeader($sName);
            return $this;
        }

        /**
         * Sets content.
         *
         * @param IAmContent $oContent
         * @return $this
         */
        public function setContent(IAmContent $oContent)
        {
            $this->oContent = $oContent;
            if ($oContent->hasContentType()) {
                $this->addHeader(Header::CONTENT_TYPE, $oContent->getContentType());
            }
            return $this;
        }

    }
}