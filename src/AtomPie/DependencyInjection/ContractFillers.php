<?php
namespace AtomPie\DependencyInjection;

use AtomPie\DependencyInjection\Boundary\IFillContracts;
use AtomPie\I18n;

class ContractFillers implements IFillContracts
{

    /**
     * @var \AtomPie\DependencyInjection\Boundary\IAmContractFiller[]
     */
    private $aContractFillers;

    public function __construct(array $aContractFillers)
    {
        $this->aContractFillers = $aContractFillers;
    }

    /**
     * @param IFillContracts $oContractFillers
     * @return ContractFillers
     */
    public function append(IFillContracts $oContractFillers) {
        return new self(array_merge($this->get(),$oContractFillers->get()));
    }

    /**
     * @return \AtomPie\DependencyInjection\Boundary\IAmContractFiller[]
     */
    public function get() {
        return $this->aContractFillers; 
    }

    /**
     * Returns null if did not found any contract fillers.
     *
     * @param $sInterfaceName
     * @param $sClassName
     * @param $sMethodName
     * @return null|string
     * @throws Exception
     */
    public function getContractFillerFor($sInterfaceName, $sClassName, $sMethodName)
    {
        $sGlobalContractFiller = null;
        $sClassContractFiller = null;
        $sClassAndMethodContractFiller = null;
        foreach ($this->aContractFillers as $oContract) {
            
            if(!$oContract instanceof Contract) {
                throw new Exception(I18n\Label('Contracts fillers should be defined by onContract()->fillBy() function.'));
            }
            
            if($oContract->isForContract($sInterfaceName)) {

                if($oContract->isConstrainedToClassAndMethod($sClassName, $sMethodName)) {
                    $sClassAndMethodContractFiller = $oContract->getContractFiller();
                    break;
                } else if($oContract->isConstrainedToClass($sClassName)) {
                    $sClassContractFiller = $oContract->getContractFiller();
                } else if($oContract->isGlobal()) {
                    $sGlobalContractFiller = $oContract->getContractFiller();
                }

            }
        }

        if(isset($sClassAndMethodContractFiller)) {
            return $sClassAndMethodContractFiller;
        }

        if(isset($sClassContractFiller)) {
            return $sClassContractFiller;
        }

        if(isset($sGlobalContractFiller)) {
            return $sGlobalContractFiller;
        }

        return null;
    }

}
