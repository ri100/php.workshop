<?php
namespace AtomPie\DependencyInjection\Boundary;

use AtomPie\DependencyInjection\ContractFillers;
use AtomPie\DependencyInjection\Exception;

interface IFillContracts
{
    /**
     * @param IFillContracts $oContractFillers
     * @return ContractFillers
     */
    public function append(IFillContracts $oContractFillers);

    /**
     * @return IAmContractFiller[]
     */
    public function get();

    /**
     * Returns null if did not found any contract fillers.
     *
     * @param $sInterfaceName
     * @param $sClassName
     * @param $sMethodName
     * @return null|string
     * @throws Exception
     */
    public function getContractFillerFor($sInterfaceName, $sClassName, $sMethodName);
}