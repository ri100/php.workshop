<?php
namespace AtomPie\I18n;

function Number($sNumber) {
    return $sNumber;
}

function Date($sData) {
    return $sData;
}

function Label() {
    
    $iParamNumber = func_num_args();

    if ($iParamNumber == 0) {
        throw new Exception('Function i18nLabel must have at least one parameter!');
    }

    $sString = func_get_arg(0);
    
    if (is_object($sString)) {
        if(!method_exists($sString, '__toString')) {
            throw new Exception('Function i18nLabel parameter must implement __toString if object passed as first parameter!');
        }
        $sString = $sString->__toString();
    }

    if (!is_string($sString)) {
        throw new Exception('Variable passed to i18nLabel object is not string.');
    }

    if ($iParamNumber > 1) {
        $aParameters = func_get_args();
        array_shift($aParameters);
        $sString = vsprintf($sString, $aParameters);
    }

    return $sString;
    
}